// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>, Sebastian Niedner
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.examples;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;

/**
 * solve nqueens problem by using CBDDs.
 *
 * @since ?.?
 */
public final class NQueensCBDD {

  private NQueensCBDD() {}

  public static void main(String[] args) {

    if (args.length == 0) {
      args = new String[] {"4", "5", "6", "7"};
    }

    for (String str : args) {
      final int n = Integer.parseInt(str);
      Creator creator =
          Builders.cbddBuilder()
              .setTableType(Builders.TableType.CASArray)
              .setVarCount(n * n)
              .setTableSize(100000)
              .setThreads(6)
              .setCacheSize(10000)
              .setParallelizationType(Builders.ParallelizationType.FORK_JOIN)
              .build();

      NQueens queens = new NQueens(n, creator);

      long start = System.currentTimeMillis();

      queens.build();
      BigInteger count = queens.solve();
      long end = System.currentTimeMillis();
      System.out.println(
          "CAS"
              + " "
              + "N="
              + n
              + ":\n SatCount: "
              + count
              + "\n Duration: "
              + (end - start)
              + "ms");

      queens.queen = null;
      queens.board = null;
      queens.close();
      System.gc();
    }
  }
}
