// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.examples;

import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;

public final class BitVectorExQuantExample {

  private BitVectorExQuantExample() {}

  public static void main(String[] args) {
    int vecN = 32;

    int varN = 10;
    bitvectorExquant(vecN, varN);
  }

  private static void bitvectorExquant(int vecN, int varN) {

    varN = (varN % 2 == 0) ? varN : varN + (varN % 2);

    DD[][] varsi = new DD[varN][vecN];
    Creator creator =
        Builders.intBuilder().setTableSize(10).setThreads(1).setVarCount(vecN * varN).build();

    int var = 0;
    for (int j = 0; j < varN; j++) {
      for (int i = 0; i < vecN; i++) {
        varsi[j][i] = creator.makeIthVar(var++);
      }
    }

    DD[] chains = new DD[varN];
    for (int j = 0; j < varN; j++) {
      chains[j] = creator.makeTrue();
      for (int i = 0; i < vecN; i++) {
        chains[j] = creator.makeAnd(chains[j], varsi[j][i]);
      }
    }

    DD chain = creator.makeTrue();
    for (int i = 0; i < chains.length; ) {
      chain = creator.makeAnd(chain, creator.makeEqual(chains[i++], chains[i++]));
    }
    long start = System.currentTimeMillis();
    creator.makeExists(chain, chains[chains.length / 2]);
    System.out.println("Computation time: " + (System.currentTimeMillis() - start));
  }
}
