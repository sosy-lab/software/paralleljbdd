// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.examples;

import java.math.BigInteger;
import java.util.Arrays;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.api.ZDDCreator;

/**
 * Implementation of the n queens problem as {@link Example} which can be solved with bdd. *
 *
 * @since 1.0
 */
public class NQueensZDD implements Example {
  /** The boolean_operations used for bdd operations. */
  private final ZDDCreator creator;

  /** number of queens akka problem size. */
  private final int n;

  /** {@link DD} matrix, representing chessboard. */
  private DD[] board;

  /** representing whole problem bdd. */
  private DD queen;

  /**
   * Creates new {@link NQueensZDD} instances with given parameter.
   *
   * @param n - the problem size
   * @param creator - the boolean_operations used for bdd operations
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public NQueensZDD(int n, ZDDCreator creator) {
    this.creator = creator;
    this.n = n;
  }

  private DD get(int i, int j) {
    return board[i + j * n];
  }

  /** {@inheritDoc} */
  @Override
  public void build() {
    queen = creator.empty();
    board = new DD[n * n];
    DD one = creator.base();
    DD zero = creator.empty();

    boolean[] mark = new boolean[n * n];

    for (int i = 0; i < n * n; i++) {
      board[i] = creator.makeNode(zero, one, i);
    }
    DD firstRow = zero;
    for (int i = 0; i < n; i++) {
      firstRow = creator.union(firstRow, get(0, i));
    }

    DD lastRow = firstRow;
    for (int i = 1; i < n; i++) {
      DD thisRow = zero;
      for (int j = 0; j < n; ++j) {
        DD build = build(i, j, lastRow, mark);
        thisRow = creator.union(thisRow, build);
      }
      lastRow = thisRow;
    }
    queen = lastRow;
  }

  /**
   * Build all rules for one field and append them to {@link #queen}.
   *
   * @param i - row index
   * @param j - column index
   */
  private DD build(int i, int j, DD row, boolean[] mark) {
    Arrays.fill(mark, false);

    for (int k = 0; k < i; k++) {
      mark[k + n * j] = true;
    }
    for (int k = 1; k <= i; k++) {
      int a = j - k;
      int b = i - k;
      if (valid(b, a)) {
        mark[b + n * a] = true;
      }
      a = j + k;
      if (valid(b, a)) {
        mark[b + n * a] = true;
      }
    }

    for (int k = 0; k < n * n; k++) {
      if (mark[k]) {
        int a = k / n;
        int b = k % n;
        row = creator.subSet0(row, get(b, a));
      }
    }
    return creator.product(row, get(i, j));
  }

  private boolean valid(int a, int b) {
    return (a >= 0 && a < n) && (b >= 0 && b < n);
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger solve() {
    return creator.satCount(queen);
  }

  /** {@inheritDoc} */
  @Override
  public DD solution() {
    return queen;
  }

  @Override
  public void close() {
    queen = null;
    board = null;
  }

  public static void main(String... args) {
    if (args.length == 0) {
      args = new String[] {"8", "9", "10", "11", "12"};
    }

    for (String str : args) {
      final int n = Integer.parseInt(str);
      NQueensZDD queens =
          new NQueensZDD(
              n,
              Builders.zddBuilder()
                  .setVarCount(n * n)
                  .setTableSize(100000)
                  .setSelectedCacheSize(10000)
                  .build());

      long start = System.currentTimeMillis();

      queens.build();
      BigInteger count = queens.solve();
      long end = System.currentTimeMillis();
      System.out.println(
          "n="
              + n
              + ":\n Sat Count: "
              + count.intValueExact()
              + "\n Duration: "
              + (end - start)
              + "ms");
      queens.queen = null;
      queens.board = null;

      queens.creator.shutdown();
      System.gc();
    }
  }
}
