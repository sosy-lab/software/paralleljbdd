// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.examples;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Implementation of the theN queens problem as {@link Example} which can be solved with bdd.
 *
 * @author Stephan Holzner
 * @see Example
 * @since 1.0
 */
public class NQueens implements Example {
  /** The boolean_operations used for bdd operations. */
  protected final Creator creator;

  /** number of queens akka problem size. */
  protected final int theN;

  /** {@link DD} matrix, representing chessboard. */
  protected List<DD> board;

  /** representing whole problem bdd. */
  protected DD queen;

  /**
   * Creates new {@link NQueens} instances with given parameter.
   *
   * @param n - the problem size
   * @param creator - the boolean_operations used for bdd operations
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public NQueens(int n, Creator creator) {
    this.creator = creator;
    this.theN = n;
  }

  /** {@inheritDoc} */
  @Override
  public void build() {
    queen = creator.makeTrue();
    board = new ArrayList<>(theN * theN);
    DD zero = creator.makeFalse();

    for (int i = 0; i < theN; ++i) {
      for (int j = 0; j < theN; ++j) {
        set(creator.makeIthVar(i * theN + j));
      }
    }
    for (int i = 0; i < theN; ++i) {
      DD f1 = zero;

      for (int j = 0; j < theN; ++j) {
        f1 = creator.makeOr(f1, get(i, j));
      }
      queen = creator.makeAnd(queen, f1);
    }

    for (int i = 0; i < theN; ++i) {
      for (int j = 0; j < theN; ++j) {
        build(i, j);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger solve() {
    return creator.satCount(queen);
  }

  /** {@inheritDoc} */
  @Override
  public DD solution() {
    return queen;
  }

  public static void main(String[] args) {

    if (args.length == 0) {
      args = new String[] {"4", "5", "6", "7"};
    }

    for (String str : args) {
      final int n = Integer.parseInt(str);

      for (Builders.TableType tableType : new Builders.TableType[] {Builders.TableType.CASArray}) {

        for (int i = 0; i < 2; i++) {

          Creator creator =
              Builders.intBuilder()
                  .setTableType(tableType)
                  .setVarCount(12 * 12)
                  .setTableSize(100000)
                  .setThreads(6)
                  .setCacheSize(10000)
                  .build();

          NQueens queens = new NQueens(n, creator);

          long start = System.currentTimeMillis();

          queens.build();
          BigInteger count = queens.solve();
          long end = System.currentTimeMillis();
          System.out.println(
              tableType
                  + " "
                  + "N="
                  + n
                  + ":\n SatCount: "
                  + count
                  + "\n Duration: "
                  + (end - start)
                  + "ms");

          queens.queen = null;
          queens.board = null;
          queens.close();
          System.gc();
        }
      }
    }
  }

  /**
   * Build all rules for one field and append them to {@link #queen}.
   *
   * @param i - row index
   * @param j - column index
   */
  protected void build(int i, int j) {

    DD a = creator.makeTrue();
    DD b = creator.makeTrue();
    DD c = creator.makeTrue();
    DD d = creator.makeTrue();

    int k;
    for (k = 0; k < theN; ++k) {
      if (k != j) {
        DD f5 = creator.makeNand(get(i, k), get(i, j));
        a = creator.makeAnd(a, f5);
      }
    }

    for (k = 0; k < theN; ++k) {
      if (k != i) {
        DD f5 = creator.makeNand(get(i, j), get(k, j));
        b = creator.makeAnd(b, f5);
      }
    }

    int n;
    for (k = 0; k < this.theN; ++k) {
      n = k - i + j;
      if (n >= 0 && n < this.theN && k != i) {
        DD f6 = creator.makeNand(get(i, j), get(k, n));
        c = creator.makeAnd(c, f6);
      }
    }

    for (k = 0; k < this.theN; ++k) {
      n = i + j - k;
      if (n >= 0 && n < this.theN && k != i) {
        DD f6 = creator.makeNand(get(i, j), get(k, n));
        d = creator.makeAnd(d, f6);
      }
    }

    c = creator.makeAnd(c, d);
    b = creator.makeAnd(b, c);
    a = creator.makeAnd(a, b);

    queen = creator.makeAnd(queen, a);
  }

  private DD get(int row, int column) {
    return board.get(row * theN + column);
  }

  private void set(DD value) {
    board.add(value);
  }

  @Override
  public void close() {
    queen = null;
    board = null;
    creator.shutDown();
  }
}
