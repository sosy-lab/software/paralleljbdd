// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.examples;

import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.api.ZDDCreator;
import org.sosy_lab.pjbdd.util.parser.DotExporter;

/** Encodes simple function: f = x OR (y AND NOT z). */
public final class SimpleFunctionExample {

  private SimpleFunctionExample() {}

  public static void main(String[] args) {
    String logMsg =
        "Construct Boolean formula f = x | (y & -z)\n"
            + "As BDD .dot: \n"
            + new DotExporter().bddToString(buildBDDEncoding())
            + "\n"
            + "As ZDD .dot: \n"
            + new DotExporter().bddToString(buildZDDEncoding());
    System.out.println(logMsg);
  }

  private static DD buildBDDEncoding() {
    Creator c = Builders.bddBuilder().setVarCount(3).build();
    DD x = c.makeIthVar(0);
    DD y = c.makeIthVar(1);
    DD z = c.makeIthVar(2);
    // build function
    return c.makeOr(x, c.makeAnd(y, c.makeNot(z)));
  }

  private static DD buildZDDEncoding() {
    ZDDCreator c = Builders.zddBuilder().setVarCount(3).build();

    // Encode zdd variable representations to be used in functions

    // encode x
    DD dontcarz0 = c.makeNode(c.empty(), c.empty(), 2);
    DD dontcarz1 = c.makeNode(c.base(), c.base(), 2);
    DD dontcary0 = c.makeNode(dontcarz0, dontcarz0, 1);
    DD dontcary1 = c.makeNode(dontcarz1, dontcarz1, 1);
    DD bddx = c.makeNode(dontcary0, dontcary1, 0);

    // encode y
    DD y = c.makeNode(dontcarz0, dontcarz1, 1);
    DD bddy = c.makeNode(y, y, 0);

    // encode z
    DD z = c.makeNode(c.empty(), c.base(), 2);
    DD yz = c.makeNode(z, z, 1);
    DD bddz = c.makeNode(yz, yz, 0);

    // build function (difference (x, y) = and(y, not(y)))
    return c.union(bddx, c.difference(bddy, bddz));
  }
}
