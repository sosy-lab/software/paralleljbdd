// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class MakeVarBeforeTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void checkCreatedDDsWithNewVarAreOnTop() {
    skipIfChained();

    DD var10 = creator.makeIthVar(10);
    DD varBefore10 = creator.makeVariableBefore(var10);

    DD and = creator.makeAnd(var10, varBefore10);

    assertThat(and.getVariable()).isEqualTo(varBefore10.getVariable());

    DD var5 = creator.makeIthVar(5);
    DD varBefore5 = creator.makeVariableBefore(var5);

    DD or = creator.makeAnd(var5, varBefore5);

    assertThat(or.getVariable()).isEqualTo(varBefore5.getVariable());
  }

  @Test
  public void checkRemainingOrderLeftUnchanged() {
    int[] oldOrdering = creator.getVariableOrdering();
    DD var10 = creator.makeIthVar(10);
    DD varBefore10 = creator.makeVariableBefore(var10);
    int[] newOrdering = creator.getVariableOrdering();
    int iOld = 0;
    int iNew = 0;

    for (; iOld < oldOrdering.length; iNew++, iOld++) {
      if (newOrdering[iNew] == varBefore10.getVariable()) {
        iNew++;
      }
      assertThat(newOrdering[iNew]).isEqualTo(oldOrdering[iOld]);
    }

    int[] oldOrdering2 = creator.getVariableOrdering();
    DD var0 = creator.makeIthVar(0);
    DD varBefore0 = creator.makeVariableBefore(var0);
    int[] newOrdering2 = creator.getVariableOrdering();
    int iOld2 = 0;
    int iNew2 = 0;

    for (; iOld2 < oldOrdering2.length; iNew2++, iOld2++) {
      if (newOrdering2[iNew2] == varBefore0.getVariable()) {
        iNew2++;
      }
      assertThat(newOrdering2[iNew2]).isEqualTo(oldOrdering2[iOld2]);
    }
  }

  @Test
  public void checkVarIsInsertedCorrectly() {
    DD var10 = creator.makeIthVar(10);
    DD varBefore10 = creator.makeVariableBefore(var10);

    int[] ordering = creator.getVariableOrdering();
    boolean insertedCorrectVar10 = false;
    for (int i = 0; i < ordering.length; i++) {
      if (ordering[i] == var10.getVariable()) {
        break;
      }
      if (ordering[i] == varBefore10.getVariable()) {
        insertedCorrectVar10 = ordering[i + 1] == var10.getVariable();
        break;
      }
    }

    assertThat(insertedCorrectVar10).isTrue();

    DD var5 = creator.makeIthVar(5);
    DD varBefore5 = creator.makeVariableBefore(var5);

    int[] ordering2 = creator.getVariableOrdering();
    boolean insertedCorrectVar5 = false;
    for (int i = 0; i < ordering2.length; i++) {
      if (ordering2[i] == var5.getVariable()) {
        break;
      }
      if (ordering2[i] == varBefore5.getVariable()) {
        insertedCorrectVar5 = ordering2[i + 1] == var5.getVariable();
        break;
      }
    }

    assertThat(insertedCorrectVar5).isTrue();

    DD var0 = creator.makeIthVar(0);
    DD varBefore0 = creator.makeVariableBefore(var0);

    int[] ordering3 = creator.getVariableOrdering();
    boolean insertedCorrectVar0 = false;
    for (int i = 0; i < ordering3.length; i++) {
      if (ordering3[i] == var0.getVariable()) {
        break;
      }
      if (ordering3[i] == varBefore0.getVariable()) {
        insertedCorrectVar0 = ordering3[i + 1] == var0.getVariable();
        break;
      }
    }

    assertThat(insertedCorrectVar0).isTrue();
  }

  @Test
  public void checkCallMakeVarBeforeMultipleTimes() {
    int[] oldOrdering = creator.getVariableOrdering();
    DD var10 = creator.makeIthVar(10);
    DD var1Before10 = creator.makeVariableBefore(var10);
    DD var2Before10 = creator.makeVariableBefore(var10);
    DD var3Before10 = creator.makeVariableBefore(var10);
    DD var4Before10 = creator.makeVariableBefore(var10);

    int[] newOrdering = creator.getVariableOrdering();
    int iOld = 0;
    int iNew = 0;

    for (; iOld < oldOrdering.length; iNew++, iOld++) {
      if (newOrdering[iNew] == var1Before10.getVariable()) {
        assertThat(var2Before10.getVariable()).isEqualTo(newOrdering[++iNew]);
        assertThat(var3Before10.getVariable()).isEqualTo(newOrdering[++iNew]);
        assertThat(var4Before10.getVariable()).isEqualTo(newOrdering[++iNew]);
        assertThat(var10.getVariable()).isEqualTo(newOrdering[++iNew]);
      }
      assertThat(newOrdering[iNew]).isEqualTo(oldOrdering[iOld]);
    }
  }
}
