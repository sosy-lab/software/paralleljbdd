// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.CreatorBuilder;
import org.sosy_lab.pjbdd.api.IntCreatorBuilder;

/**
 * Base class to set up all known creators.
 *
 * @author Stephan Holzner (modified by Daniel Baier)
 * @since 1.0
 */
public class CreatorCombinatorTest {

  protected Creator creator;

  /** List with creator instances used to perform test for each instance. */
  // @Parameters(name = "creator= {0}, ct={2}, table={1}")
  public static Collection<Object[]> data2() {
    List<Object[]> creators = new ArrayList<>();
    for (CreatorType ct : CreatorType.values()) {
      if (ct == CreatorType.ConcurrentInt || ct == CreatorType.SerialInt) {
        if (ct == CreatorType.ConcurrentInt) {
          creators.add(
              new Object[] {
                resolveCreator(ct, UniqueTableType.Array, false), UniqueTableType.Array, ct,
              });
          creators.add(
              new Object[] {
                resolveCreator(ct, UniqueTableType.Array, true), UniqueTableType.Array, ct,
              });
        }
        creators.add(
            new Object[] {
              resolveCreator(ct, UniqueTableType.CASArray, false), UniqueTableType.CASArray, ct,
            });
        creators.add(
            new Object[] {
              resolveCreator(ct, UniqueTableType.CASArray, true), UniqueTableType.CASArray, ct,
            });
      } else {
        for (UniqueTableType ut : UniqueTableType.values()) {
          if (ut == UniqueTableType.Array
              && (ct != CreatorType.SerialApply && ct != CreatorType.CBDD)) {
            continue;
          }
          creators.add(
              new Object[] {
                resolveCreator(ct, ut, false), ut, ct,
              });
        }
      }
    }
    creators.add(
        new Object[] {
          resolveCreator(CreatorType.ConcurrentCBDD, UniqueTableType.CASArray, true),
          UniqueTableType.CASArray,
          CreatorType.ConcurrentCBDD,
        });
    creators.add(
        new Object[] {
          resolveCreator(CreatorType.ConcurrentInt, UniqueTableType.CASArray, true),
          UniqueTableType.CASArray,
          CreatorType.ConcurrentInt,
        });
    creators.add(
        new Object[] {
          resolveCreator(CreatorType.ForkJoin, UniqueTableType.CASArray, true),
          UniqueTableType.CASArray,
          CreatorType.ForkJoin,
        });

    return creators;
  }

  /** Perform class specific test. */
  @Test
  public void defaultTest() {}

  /** List with creator instances used to perform test for each instance. */
  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> data() {
    List<Object[]> creators = new ArrayList<>();
    for (CreatorType ct : CreatorType.values()) {
      if (ct == CreatorType.ConcurrentInt || ct == CreatorType.SerialInt) {
        if (ct == CreatorType.ConcurrentInt) {
          creators.add(
              new Object[] {
                false, UniqueTableType.Array, ct,
              });
        }
        creators.add(
            new Object[] {
              false, UniqueTableType.CASArray, ct,
            });
      } else {
        for (UniqueTableType ut : UniqueTableType.values()) {
          if (ut == UniqueTableType.Array
              && (ct != CreatorType.SerialApply && ct != CreatorType.CBDD)) {
            continue;
          }
          creators.add(
              new Object[] {
                false, ut, ct,
              });
        }
      }
    }
    creators.add(
        new Object[] {
          true, UniqueTableType.CASArray, CreatorType.ConcurrentCBDD,
        });
    creators.add(
        new Object[] {
          true, UniqueTableType.CASArray, CreatorType.ConcurrentInt,
        });
    creators.add(
        new Object[] {
          true, UniqueTableType.CASArray, CreatorType.ForkJoin,
        });

    return creators;
  }

  protected void skipIfChained() {
    org.junit.Assume.assumeTrue(
        creatorTypeToUse() != CreatorType.CBDD && creatorTypeToUse() != CreatorType.ConcurrentCBDD);
  }

  // Some default
  protected Creator creatorToUse() {
    return resolveCreator(creatorTypeToUse(), uniqueTableTypeToUse(), synchronizeReorderingToUse());
  }

  // Some default
  protected boolean synchronizeReorderingToUse() {
    return true;
  }

  // Some default
  protected UniqueTableType uniqueTableTypeToUse() {
    return UniqueTableType.Array;
  }

  // Some default
  protected CreatorType creatorTypeToUse() {
    return CreatorType.SerialInt;
  }

  @Before
  public void createCreator() {
    creator = creatorToUse();
  }

  @After
  public void cleanUp() {
    creator.cleanUnusedNodes();
    creator.shutDown();
  }

  /**
   * Initialize scenario for specific combination of queens N CreatorType and UniqueTableType.
   *
   * @param ct - the UniqueTableType
   * @param ut - the CreatorType
   * @return the resolved creator uniquetable combination
   */
  private static Creator resolveCreator(
      CreatorType ct, UniqueTableType ut, boolean synchronizeReordering) {

    Creator creator;
    if (ct == CreatorType.ConcurrentInt) {
      CreatorBuilder creatorBuilder;
      creatorBuilder = Builders.intBuilder();
      creatorBuilder.setThreads(4).build();
      if (synchronizeReordering) {
        creatorBuilder.disableThreadSafety();
        creatorBuilder = ((IntCreatorBuilder) creatorBuilder).disableGC();
      }
      creator = creatorBuilder.build();
    } else if (ct == CreatorType.SerialInt) {
      CreatorBuilder creatorBuilder;
      creatorBuilder = Builders.intBuilder();
      creatorBuilder.setThreads(1).build();
      if (synchronizeReordering) {
        creatorBuilder.disableThreadSafety();
      }
      creator = creatorBuilder.build();
    } else {
      CreatorBuilder creatorBuilder;
      if (ct == CreatorType.CBDD || ct == CreatorType.ConcurrentCBDD) {
        creatorBuilder = Builders.cbddBuilder();
      } else {
        creatorBuilder = Builders.bddBuilder();
      }
      switch (ut) {
        case HashSet:
          creatorBuilder.setTableType(Builders.TableType.ConcurrentHashMap);
          break;
        case HashBucket:
          creatorBuilder.setTableType(Builders.TableType.ConcurrentHashBucket);
          break;
        case CASArray:
          creatorBuilder.setTableType(Builders.TableType.CASArray);
          break;
        case Array:
          creatorBuilder.disableThreadSafety();
          break;
        default:
          throw new IllegalArgumentException("Unknown Uniquetable type");
      }

      switch (ct) {
        case ForkJoin:
          creatorBuilder
              .setUseApply(false)
              .setParallelizationType(Builders.ParallelizationType.FORK_JOIN);
          break;
        case ApplyOp:
          creatorBuilder
              .setUseApply(true)
              .setParallelizationType(Builders.ParallelizationType.FORK_JOIN);
          break;
        case SerialApply:
          creatorBuilder
              .setUseApply(true)
              .setParallelizationType(Builders.ParallelizationType.NONE);
          break;
        case CompletableApply:
          creatorBuilder
              .setUseApply(true)
              .setParallelizationType(Builders.ParallelizationType.COMPLETABLE_FUTURE);
          break;
        case CompletableFuture:
          creatorBuilder
              .setUseApply(false)
              .setParallelizationType(Builders.ParallelizationType.COMPLETABLE_FUTURE);
          break;
        case Future:
          creatorBuilder
              .setUseApply(false)
              .setParallelizationType(Builders.ParallelizationType.FUTURE);
          break;
        case Stream:
          creatorBuilder
              .setUseApply(false)
              .setParallelizationType(Builders.ParallelizationType.STREAM);
          break;
        case Serial:
          creatorBuilder
              .setUseApply(false)
              .setParallelizationType(Builders.ParallelizationType.NONE);
          break;
        case Guava:
          creatorBuilder
              .setUseApply(false)
              .setParallelizationType(Builders.ParallelizationType.GUAVA_FUTURE);
          break;
        case CBDD:
          creatorBuilder.setParallelizationType(Builders.ParallelizationType.NONE);
          break;
        case ConcurrentCBDD:
          creatorBuilder.setParallelizationType(Builders.ParallelizationType.FORK_JOIN);
          break;
        default:
          throw new IllegalArgumentException("Unknown Creator type");
      }
      if (synchronizeReordering) {
        creatorBuilder.synchronizeReorderingOperations();
      }
      creator = creatorBuilder.build();
    }
    return creator;
  }

  /** enum to resolve creator types. */
  protected enum CreatorType {
    Future,
    Guava,
    Serial,
    Stream,
    CompletableFuture,
    ForkJoin,
    ApplyOp,
    SerialApply,
    CompletableApply,
    ConcurrentInt,
    SerialInt,
    CBDD,
    ConcurrentCBDD
  }

  /** enum to resolve unique table types. */
  public enum UniqueTableType {
    HashBucket,
    HashSet,
    CASArray,
    Array
  }
}
