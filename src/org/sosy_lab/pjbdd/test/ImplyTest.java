// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'IMPLY' test class uses {@link CreatorCombinatorTest} as base class to perform make 'IMPLY'
 * test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class ImplyTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeImply(var1, creator.makeTrue());
    assertThat(res1).isEqualTo(creator.makeTrue());

    DD res2 = creator.makeImply(creator.makeFalse(), var2);
    assertThat(res2).isEqualTo(creator.makeTrue());

    DD res3 = creator.makeImply(creator.makeTrue(), var2);
    assertThat(var2).isEqualTo(res3);
  }
}
