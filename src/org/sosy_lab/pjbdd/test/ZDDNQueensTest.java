// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.sosy_lab.pjbdd.examples.Example;
import org.sosy_lab.pjbdd.examples.NQueensZDD;
import org.sosy_lab.pjbdd.examples.NQueensZDDWithUnateSetAlgebra;

public class ZDDNQueensTest extends ZDDCombinatorTest {
  /** Size(N) to expected satCount Map. */
  private final Map<Integer, Integer> scenarios = new LinkedHashMap<>();

  @Before
  public void init() {
    scenarios.put(4, 2);
    scenarios.put(5, 10);
    scenarios.put(6, 4);
    scenarios.put(7, 40);
  }

  @Test
  public void test() {
    scenarios.forEach(
        (key, value) -> {
          Example example = new NQueensZDD(key, creator);
          example.build();
          int satCount = example.solve().intValue();

          assertThat(satCount).isEqualTo(value);
        });
    scenarios.forEach(
        (key, value) -> {
          Example example = new NQueensZDDWithUnateSetAlgebra(key, creator);
          example.build();
          int satCount = example.solve().intValue();

          assertThat(satCount).isEqualTo(value);
        });
  }
}
