// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'EXISTENTIAL QUANTIFICATION' test class uses {@link CreatorCombinatorTest} as base class to
 * perform make 'EXISTENTIAL QUANTIFICATION' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class ExistsTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    skipIfChained();
    DD bdd0 = creator.makeVariable();
    DD bdd1 = creator.makeVariable();
    DD bdd2 = creator.makeVariable();

    DD conjunction = bdd2;
    conjunction = creator.makeAnd(conjunction, bdd1);
    conjunction = creator.makeAnd(conjunction, bdd0);

    DD exquant = creator.makeExists(conjunction, new DD[] {bdd2, bdd1});
    assertThat(bdd0).isEqualTo(exquant);

    DD exquant2 = creator.makeExists(creator.makeTrue(), creator.makeVariable());
    assertThat(exquant2).isEqualTo(creator.makeTrue());

    DD bdd3 = creator.makeVariable();
    DD bdd4 = creator.makeVariable();

    DD dis = creator.makeOr(bdd0, creator.makeOr(bdd1, creator.makeOr(bdd2, bdd4)));

    DD exquant3 = creator.makeExists(dis, bdd3);
    assertThat(exquant3).isEqualTo(dis);
  }
}
