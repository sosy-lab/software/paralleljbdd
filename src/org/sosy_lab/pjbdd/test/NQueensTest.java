// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.core.uniquetable.UniqueTable;
import org.sosy_lab.pjbdd.examples.Example;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.intBDD.IntUniqueTable;

/**
 * Test class which solves the {@link NQueens} example with various n for all known{@link Creator}
 * and {@link UniqueTable} / {@link IntUniqueTable} combinations.
 *
 * @author Stephan Holzner
 * @see NQueens
 * @see Creator
 * @see IntUniqueTable
 * @see UniqueTable
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class NQueensTest extends CreatorCombinatorTest {
  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    /* Size(N) to expected satCount Map. */
    Map<Integer, Integer> scenarios = new LinkedHashMap<>();
    scenarios.put(4, 2);
    scenarios.put(5, 10);
    scenarios.put(6, 4);
    scenarios.put(7, 40);

    scenarios.forEach(
        (key, value) -> {
          Example example = new NQueens(key, creator);
          example.build();

          assertThat(example.solve().intValue()).isEqualTo(value);
        });
  }
}
