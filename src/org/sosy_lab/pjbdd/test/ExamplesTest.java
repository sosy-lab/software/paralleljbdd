// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.function.Predicate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sosy_lab.pjbdd.examples.BitVectorExQuantExample;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.examples.NQueensCBDD;
import org.sosy_lab.pjbdd.examples.NQueensZDDWithUnateSetAlgebra;
import org.sosy_lab.pjbdd.examples.SimpleFunctionExample;
import org.sosy_lab.pjbdd.tbdd.TBDDNQueens;

public class ExamplesTest {
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final PrintStream originalOut = System.out;

  @Before
  @SuppressWarnings("checkstyle:IllegalInstantiation")
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent, true, StandardCharsets.UTF_8));
  }

  @After
  public void restoreStreams() {
    System.setOut(originalOut);
  }

  @Test
  public void test() {
    Predicate<String> predicate =
        a ->
            a.contains("N=4")
                && a.contains("SatCount: 2")
                && a.contains("N=5")
                && a.contains("SatCount: 10")
                && a.contains("N=6")
                && a.contains("SatCount: 4")
                && a.contains("N=7")
                && a.contains("SatCount: 40");

    BitVectorExQuantExample.main(new String[0]);
    assertThat(outContent.toString(StandardCharsets.UTF_8).contains("Computation time: ")).isTrue();

    NQueens.main(new String[0]);
    assertThat(predicate.test(outContent.toString(StandardCharsets.UTF_8))).isTrue();

    NQueensCBDD.main(new String[0]);
    assertThat(predicate.test(outContent.toString(StandardCharsets.UTF_8))).isTrue();

    SimpleFunctionExample.main(new String[0]);
    assertThat(
            outContent
                .toString(StandardCharsets.UTF_8)
                .contains("Construct Boolean formula f = x | (y & -z)"))
        .isTrue();

    NQueensZDDWithUnateSetAlgebra.main(new String[0]);
    assertThat(predicate.test(outContent.toString(StandardCharsets.UTF_8))).isTrue();

    TBDDNQueens.main(new String[0]);
    assertThat(predicate.test(outContent.toString(StandardCharsets.UTF_8))).isTrue();
  }
}
