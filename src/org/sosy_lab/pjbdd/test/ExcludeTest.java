// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.sosy_lab.pjbdd.api.DD;

@RunWith(Parameterized.class)
public class ExcludeTest extends ZDDCombinatorTest {

  @Test
  public void test() {
    DD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    DD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    DD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    DD bdd0 = creator.union(var2, var1);
    DD bdd1 = creator.union(bdd0, var0);

    DD exclude0 = creator.exclude(creator.empty(), bdd1);
    assertThat(exclude0).isEqualTo(creator.empty());

    DD exclude1 = creator.exclude(bdd1, creator.base());
    assertThat(exclude1).isEqualTo(creator.empty());

    DD exclude2 = creator.exclude(bdd1, bdd1);
    assertThat(exclude2).isEqualTo(creator.empty());

    DD exclude3 = creator.exclude(creator.base(), bdd1);
    assertThat(exclude3).isEqualTo(creator.base());

    DD exclude4 = creator.exclude(bdd1, creator.empty());
    assertThat(exclude4).isEqualTo(bdd1);
  }
}
