// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.ZDDBuilder;
import org.sosy_lab.pjbdd.api.ZDDCreator;

@RunWith(Parameterized.class)
public class ZDDCombinatorTest {

  /** List with creator instances used to perform test for each instance. */
  @Parameters(name = "creator={0}, table={1}")
  public static Collection<Object[]> data() {
    List<Object[]> creators = new ArrayList<>();
    for (CreatorType ct : CreatorType.values()) {
      for (UniqueTableType ut : UniqueTableType.values()) {
        creators.add(new Object[] {resolveCreator(ut, ct, false), ut});
        creators.add(new Object[] {resolveCreator(ut, ct, true), ut});
      }
    }
    return creators;
  }

  @Parameter(0)
  public ZDDCreator creator;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  /** Perform class specific test for specific {@link ZDDCreator} and {@link UniqueTableType}. */
  @Test
  public void defaultTest() {}

  /**
   * Initialize scenario for specific combination of CreatorType and UniqueTableType.
   *
   * @param ut - the UniqueTableType
   * @param ct - the CreatorType
   * @return the Creator combination
   */
  private static ZDDCreator resolveCreator(
      UniqueTableType ut, CreatorType ct, boolean sizeVariation) {

    ZDDBuilder creatorBuilder = Builders.zddBuilder().setVarCount(0).setIncreaseFactor(1);
    switch (ut) {
      case HashSet:
        creatorBuilder.setTableType(Builders.TableType.ConcurrentHashMap);
        break;
      case HashBucket:
        creatorBuilder.setTableType(Builders.TableType.ConcurrentHashBucket);
        break;
      case ResizingArray:
        creatorBuilder.setTableType(Builders.TableType.CASArray);
        break;
      default:
        throw new IllegalArgumentException("Unknown Uniquetable type: " + ut);
    }
    if (sizeVariation) {
      creatorBuilder.setParallelism(2000);
      creatorBuilder.setSelectedParallelism(2000);
      creatorBuilder.setTableSize(2000);
      creatorBuilder.setCacheSize(2000);
      creatorBuilder.setSelectedCacheSize(2000);
    }
    switch (ct) {
      case Serial:
        return creatorBuilder.setThreads(1).build();
      case Concurrent:
        return creatorBuilder.setThreads(4).build();
    }
    return creatorBuilder.setThreads(1).build();
  }

  /** enum to resolve unique table types. */
  enum UniqueTableType {
    HashBucket,
    HashSet,
    ResizingArray
  }

  /** enum to resolve creator types. */
  enum CreatorType {
    Serial,
    Concurrent
  }
}
