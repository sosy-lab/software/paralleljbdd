// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.api.Statistics;
import org.sosy_lab.pjbdd.examples.NQueens;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class SimpleStatsTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void testSimpleCreatorStats() {
    skipIfChained();

    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    Creator.Stats creatorStats = Statistics.of(creator);

    // default nr of variables = 10
    assertThat(creatorStats.getVariableCount()).isEqualTo(12);
    // pjbdd creates variable + it's negation + true&false
    assertThat(creatorStats.getNodeCount()).isEqualTo(26);

    DD unused = creator.makeAnd(var1, var2);
    Creator.Stats creatorStats2 = Statistics.of(creator);

    assertThat(creatorStats2.getNodeCount()).isEqualTo(27);
    assertThat(creatorStats2.getCacheNodeCount()).isEqualTo(1);
    assertThat(unused.isLeaf()).isFalse();
  }

  @Test
  public void testSimpleBDDStats() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    Statistics.DDStats ddStats = Statistics.of(var1);

    assertThat(ddStats.getNodeCount()).isEqualTo(3);
    assertThat(ddStats.getEdgeCount()).isEqualTo(2);

    DD res3 = creator.makeAnd(var1, creator.makeFalse());
    Statistics.DDStats ddStats2 = Statistics.of(res3);

    assertThat(ddStats2.getNodeCount()).isEqualTo(1);
    assertThat(ddStats2.getEdgeCount()).isEqualTo(0);

    DD res = creator.makeAnd(var1, var2);
    Statistics.DDStats ddStats3 = Statistics.of(res);

    assertThat(ddStats3.getNodeCount()).isEqualTo(4);
    assertThat(ddStats3.getEdgeCount()).isEqualTo(4);
  }

  @Test
  public void testNQueensStats() {
    skipIfChained();
    NQueens nQueens = new NQueens(4, creator);
    nQueens.build();
    Statistics.DDStats ddStats1 = Statistics.of(nQueens.solution());
    assertThat(ddStats1.getNodeCount()).isEqualTo(31);
    assertThat(ddStats1.getEdgeCount()).isEqualTo(58);
  }
}
