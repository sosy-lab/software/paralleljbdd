// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class NodeManagerTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void initialVarCountCanBeIncreasedWithIthVarDuringRuntime() {
    int initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount + 1; i++) {
      creator.makeIthVar(i);
    }

    assertThat(creator.getVariableCount()).isEqualTo(initialVarcount + 1);

    initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount * 2; i++) {
      creator.makeIthVar(i);
    }

    assertThat(creator.getVariableCount()).isEqualTo(initialVarcount * 2L);
  }

  @Test
  public void initialVarCountCanBeIncreasedWithMakeVarDuringRuntime() {
    int initialVarcount = creator.getVariableCount();

    creator.makeVariable();

    assertThat(creator.getVariableCount()).isEqualTo(initialVarcount + 1);

    initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount; i++) {
      creator.makeVariable();
    }

    assertThat(creator.getVariableCount()).isEqualTo(initialVarcount * 2L);
  }
}
