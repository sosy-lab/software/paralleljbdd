// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.math.BigInteger;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.examples.NQueens;

/**
 * Make 'SAT_COUNT' test class uses {@link CreatorCombinatorTest} as base class to perform make
 * 'SAT_COUNT' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class SatCountTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void testGeneral() {
    DD a = creator.makeVariable();
    DD b = creator.makeVariable();

    DD or = creator.makeOr(creator.makeVariable(), creator.makeVariable());
    for (int i = 0; i < 4; i++) {
      or = creator.makeOr(or, creator.makeVariable());
    }

    DD ite = creator.makeIte(a, b, or);

    assertThat(creator.satCount(ite)).isEqualTo(BigInteger.valueOf(190));
  }

  @Test
  public void andSATCount() {
    DD and = creator.makeAnd(creator.makeVariable(), creator.makeVariable());
    for (int i = 0; i < 9; i++) {
      and = creator.makeAnd(and, creator.makeVariable());
    }

    assertThat(creator.satCount(and)).isEqualTo(BigInteger.valueOf(1));
  }

  @Test
  public void orSATCount() {
    DD or = creator.makeOr(creator.makeVariable(), creator.makeVariable());
    for (int i = 0; i < 8; i++) {
      or = creator.makeOr(or, creator.makeVariable());
    }

    assertThat(creator.satCount(or)).isEqualTo(BigInteger.valueOf(1023));
  }

  @Test
  public void testSATCountQueens() {
    NQueens queens = new NQueens(4, creator);
    queens.build();
    assertThat(queens.solve()).isEqualTo(BigInteger.valueOf(2));

    NQueens queens2 = new NQueens(8, creator);
    queens2.build();
    assertThat(queens2.solve()).isEqualTo(BigInteger.valueOf(92));
  }

  @Test
  public void testSATCountSimple() {
    DD a = creator.makeVariable();
    DD b = creator.makeVariable();
    DD c = creator.makeVariable();
    DD d = creator.makeVariable();

    DD aAndB = creator.makeAnd(a, b);
    // Only the and has 1 sat assignment
    assertThat(creator.satCount(aAndB)).isEqualTo(BigInteger.valueOf(1));

    DD bAndCAndD = creator.makeAnd(b, creator.makeAnd(d, c));
    assertThat(creator.satCount(bAndCAndD)).isEqualTo(BigInteger.valueOf(1));
    DD or = creator.makeOr(aAndB, bAndCAndD);
    // a and b true is sat, c and d irrelevant -> 2^2 = 4
    // b, d, c = true is sat -> 2^1 = 2
    // but the assignment a,b,c,d true is shared between both so -1
    assertThat(creator.satCount(or)).isEqualTo(BigInteger.valueOf(5));

    DD ite = creator.makeIte(a, b, c);
    assertThat(creator.satCount(ite)).isEqualTo(BigInteger.valueOf(4));

    DD iteAnd = creator.makeAnd(creator.makeAnd(a, b), ite);
    // since a and b must be true, only one possible ite assignment
    assertThat(creator.satCount(iteAnd)).isEqualTo(BigInteger.valueOf(1));

    DD e = creator.makeVariable();
    DD f = creator.makeVariable();
    DD eAndF = creator.makeAnd(e, f);
    assertThat(creator.satCount(eAndF)).isEqualTo(BigInteger.valueOf(1));
    DD or2 = creator.makeOr(eAndF, or);
    // a and b true is sat, c, d, e, f irrelevant -> 2^4 = 16
    // b, d, c = true is sat -> 2^3 = 8
    // e, f = true -> 2^4 = 16
    // minus shared assignments (-9)
    assertThat(creator.satCount(or2)).isEqualTo(BigInteger.valueOf(31));
  }
}
