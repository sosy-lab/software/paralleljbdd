// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.collect.ImmutableSet;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'COMPOSE' test class uses {@link CreatorCombinatorTest} as base class to perform make
 * 'COMPOSE' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class ComposeTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    skipIfChained();

    DD a = creator.makeIthVar(1);
    DD b = creator.makeIthVar(2);
    DD c = creator.makeIthVar(3);
    DD d = creator.makeIthVar(4);
    DD e = creator.makeIthVar(5);

    DD bdd1 =
        creator.makeOr(
            creator.makeAnd(creator.makeNot(a), b),
            creator.makeAnd(a, creator.makeOr(b, creator.makeAnd(creator.makeNot(b), c))));
    DD bdd2 = creator.makeAnd(d, e);

    DD composition = creator.makeCompose(bdd1, c.getVariable(), bdd2);

    DD expected =
        creator.makeOr(
            creator.makeAnd(creator.makeNot(a), b),
            creator.makeAnd(a, creator.makeOr(b, creator.makeAnd(creator.makeNot(b), bdd2))));
    assertThat(composition).isEqualTo(expected);
  }

  /*
   * Regression test for: x1 NAND x2 -> not x1 OR not x2 then subs x1 with not x1 and (not x1) NAND
   * x2 -> x1 OR not x2
   *
   */
  @Test
  public void composeTest() {
    DD x1 = creator.makeIthVar(1);
    DD notx1 = creator.makeNot(x1);
    DD x2 = creator.makeIthVar(2);

    DD nand = creator.makeNand(x1, x2);

    DD comp1 = creator.makeCompose(nand, x1.getVariable(), notx1);
    DD comp2 = creator.makeCompose(comp1, x1.getVariable(), notx1);
    DD comp3 = creator.makeCompose(comp2, x1.getVariable(), notx1);

    for (DD originalNand : ImmutableSet.of(nand, comp2)) {
      // x1 NAND x2 -> not x1 OR not x2
      assertThat(creator.makeTrue()).isEqualTo(originalNand.getLow());
      assertThat(creator.makeFalse()).isEqualTo(originalNand.getHigh().getHigh());
      assertThat(creator.makeTrue()).isEqualTo(originalNand.getHigh().getLow());
    }

    for (DD compositition : ImmutableSet.of(comp1, comp3)) {
      // (not x1) NAND x2 -> x1 OR not x2
      if (compositition.getVariable() == x1.getVariable()) {
        assertThat(creator.makeTrue()).isEqualTo(compositition.getHigh());
        assertThat(creator.makeFalse()).isEqualTo(compositition.getLow().getHigh());
        assertThat(creator.makeTrue()).isEqualTo(compositition.getLow().getLow());
      } else {
        assertThat(creator.makeTrue()).isEqualTo(compositition.getLow());
        assertThat(creator.makeTrue()).isEqualTo(compositition.getHigh().getHigh());
        assertThat(creator.makeFalse()).isEqualTo(compositition.getHigh().getLow());
      }
    }
  }
}
