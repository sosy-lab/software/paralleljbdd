// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.IfThenElseData;

/**
 * Make 'ITE' test class uses {@link CreatorCombinatorTest} as base class to perform make 'ITE' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class IteTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void testGeneral() {
    DD i = creator.makeVariable();
    DD t = creator.makeVariable();
    DD e = creator.makeVariable();

    DD res1 = creator.makeIte(i, t, t);
    assertThat(t).isEqualTo(res1);

    DD res2 = creator.makeIte(creator.makeTrue(), t, e);
    assertThat(t).isEqualTo(res2);

    DD res3 = creator.makeIte(creator.makeFalse(), t, e);
    assertThat(e).isEqualTo(res3);

    DD res4 = creator.makeIte(i, creator.makeTrue(), creator.makeFalse());
    assertThat(i).isEqualTo(res4);

    DD notI = creator.makeNot(i);
    DD res5 = creator.makeIte(i, creator.makeFalse(), creator.makeTrue());
    assertThat(notI).isEqualTo(res5);

    IfThenElseData iteData1 = new IfThenElseData(i, creator.makeFalse(), creator.makeTrue());

    IfThenElseData iteData2 = new IfThenElseData(i, creator.makeFalse(), creator.makeTrue());
    assertThat(iteData2).isEqualTo(iteData1);

    DD iteD = creator.makeIte(iteData1);
    assertThat(res5).isEqualTo(iteD);
  }

  @Test
  public void testIteEncodingAsSATRegression() {

    DD x1 = creator.makeIthVar(1);
    DD x2 = creator.makeIthVar(2);
    DD bot = creator.makeFalse();

    // If a then b else c is equivalent to (a AND b) OR (!a AND c)
    DD ite = creator.makeIte(x1, x2, bot);
    DD iteAsFormula =
        creator.makeOr(creator.makeAnd(x1, x2), creator.makeAnd(creator.makeNot(x1), bot));

    assertThat(iteAsFormula).isEqualTo(ite);
  }

  @Test
  public void testIteEncodingAsSAT() {
    skipIfChained();

    boolean bx1;
    boolean bx2;
    boolean bx3;
    boolean res;

    boolean[][] permutations = {
      {false, false, false},
      {false, false, true},
      {false, true, false},
      {false, true, true},
      {true, false, false},
      {true, false, true},
      {true, true, false},
      {true, true, true},
    };

    DD bot = creator.makeFalse();
    DD top = creator.makeTrue();

    DD x1 = creator.makeIthVar(1);
    DD x2 = creator.makeIthVar(2);
    DD x3 = creator.makeIthVar(3);

    // If a then b else c is equivalent to (a AND b) OR (!a AND c)
    DD ite = creator.makeIte(x1, x2, x3);
    DD iteAsFormula =
        creator.makeOr(creator.makeAnd(x1, x2), creator.makeAnd(creator.makeNot(x1), x3));

    creator.anySat(ite);

    int iteSatCount = creator.satCount(ite).intValueExact();
    int iteAsFormulaSatCount = creator.satCount(iteAsFormula).intValueExact();

    // The number of possible assignments match for the general case
    assertThat(iteAsFormulaSatCount).isEqualTo(iteSatCount);
    assertThat(iteSatCount).isEqualTo(4);

    for (boolean[] permutation : permutations) {
      bx1 = permutation[0];
      bx2 = permutation[1];
      bx3 = permutation[2];

      res = bx1 ? bx2 : bx3;

      x1 = bx1 ? top : bot;
      x2 = bx2 ? top : bot;
      x3 = bx3 ? top : bot;

      DD iteFromBools = creator.makeIte(x1, x2, x3);
      DD iteAsFormulaFromBools =
          creator.makeOr(creator.makeAnd(x1, x2), creator.makeAnd(creator.makeNot(x1), x3));

      assertThat(iteAsFormulaFromBools).isEqualTo(iteFromBools);
      assertThat(creator.satCount(iteAsFormulaFromBools).intValueExact())
          .isEqualTo(creator.satCount(iteFromBools).intValueExact());
      if (res) {
        assertThat(creator.satCount(iteFromBools).intValueExact()).isGreaterThan(0);
      } else {
        assertThat(creator.satCount(iteFromBools).intValueExact()).isEqualTo(0);
      }
    }
  }
}
