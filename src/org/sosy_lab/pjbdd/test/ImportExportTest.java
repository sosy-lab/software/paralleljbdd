// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.util.parser.BDDStringExporter;
import org.sosy_lab.pjbdd.util.parser.BDDStringImporter;
import org.sosy_lab.pjbdd.util.parser.DotExporter;
import org.sosy_lab.pjbdd.util.parser.DotImporter;
import org.sosy_lab.pjbdd.util.parser.Exporter;
import org.sosy_lab.pjbdd.util.parser.Importer;

/**
 * Test export and import classes.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class ImportExportTest {

  /** Test dot export and dor import. */
  @Test
  public void testDotExport() {
    Creator c =
        Builders.bddBuilder()
            .setCacheSize(1000)
            .setIncreaseFactor(1)
            .setTableSize(10000)
            .setVarCount(100)
            .setParallelism(1000)
            .build();
    NQueens queens = new NQueens(4, c);
    queens.build();
    queens.solve();
    Exporter<DD> exporter = new DotExporter();
    String stringBDD = exporter.bddToString(queens.solution());
    // System.out.println(stringBDD);

    Importer<DD> importer = new DotImporter(c);
    DD bdd = importer.bddFromString(stringBDD);
    assertThat(queens.solution()).isEqualTo(bdd);
  }

  /** Test custom string representation export and dor import. */
  @Test
  public void testStringExport() {
    Creator c =
        Builders.bddBuilder()
            .setCacheSize(1000)
            .setIncreaseFactor(1)
            .setTableSize(10000)
            .setVarCount(100)
            .setParallelism(1000)
            .build();
    NQueens queens = new NQueens(4, c);
    queens.build();
    queens.solve();
    Exporter<DD> exporter = new BDDStringExporter(c);
    String stringBDD = exporter.bddToString(queens.solution());
    // System.out.println(stringBDD);

    Importer<DD> importer = new BDDStringImporter(c);
    DD bdd = importer.bddFromString(stringBDD);
    assertThat(queens.solution()).isEqualTo(bdd);
  }
}
