// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class AndTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeAnd(var1, var1);

    assertThat(var1).isEqualTo(res1);

    DD res2 = creator.makeAnd(var1, creator.makeTrue());
    assertThat(var1).isEqualTo(res2);

    DD res3 = creator.makeAnd(var1, creator.makeFalse());
    assertThat(creator.makeFalse()).isEqualTo(res3);

    DD res4 = creator.makeAnd(creator.makeFalse(), var1);
    assertThat(creator.makeFalse()).isEqualTo(res4);

    DD res5 = creator.makeAnd(creator.makeTrue(), var2);
    assertThat(var2).isEqualTo(res5);

    DD res = creator.makeAnd(var1, var2);
    assertThat(var1.getVariable()).isEqualTo(res.getVariable());
    assertThat(creator.makeFalse()).isEqualTo(res.getLow());
    assertThat(var2).isEqualTo(res.getHigh());
  }
}
