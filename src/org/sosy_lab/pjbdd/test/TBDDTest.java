// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.collect.ImmutableSet;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.api.Statistics;
import org.sosy_lab.pjbdd.examples.Example;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.tbdd.TBDDNQueens;
import org.sosy_lab.pjbdd.util.IfThenElseData;

public class TBDDTest {
  Creator creator;

  @Before
  public void init() {
    creator = Builders.newTBDDBuilder().setVarCount(5).setThreads(4).build();
  }

  @After
  public void shutdown() {
    if (creator != null) {
      creator.shutDown();
      creator = null;
    }
  }

  @Test
  public void andTest() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeAnd(var1, var1);
    assertThat(res1).isEqualTo(var1);

    DD res2 = creator.makeAnd(var1, creator.makeTrue());
    assertThat(res2).isEqualTo(var1);

    DD res3 = creator.makeAnd(var1, creator.makeFalse());
    assertThat(res3).isEqualTo(creator.makeFalse());

    DD res4 = creator.makeAnd(creator.makeFalse(), var1);
    assertThat(res4).isEqualTo(creator.makeFalse());

    DD res5 = creator.makeAnd(creator.makeTrue(), var2);
    assertThat(res5).isEqualTo(var2);

    DD res = creator.makeAnd(var1, var2);
    assertThat(res.getVariable()).isEqualTo(var1.getVariable());
    assertThat(res.getLow()).isEqualTo(creator.makeFalse());
    assertThat(res.getHigh()).isEqualTo(var2);
  }

  @Test
  public void nQueensTest() {
    Map<Integer, Integer> scenarios = new LinkedHashMap<>();

    scenarios.put(4, 2);
    scenarios.put(5, 10);
    scenarios.put(6, 4);
    scenarios.put(7, 40);

    scenarios.forEach(
        (key, value) -> {
          init();

          Example example = new TBDDNQueens(key, creator);
          example.build();
          int satCount = example.solve().intValue();
          assertThat(satCount).isEqualTo(value);

          shutdown();
        });
  }

  @Test
  public void composeTest1() {
    DD a = creator.makeIthVar(1);
    DD b = creator.makeIthVar(2);
    DD c = creator.makeIthVar(3);
    DD d = creator.makeIthVar(4);
    DD e = creator.makeIthVar(5);

    DD bdd1 =
        creator.makeOr(
            creator.makeAnd(creator.makeNot(a), b),
            creator.makeAnd(a, creator.makeOr(b, creator.makeAnd(creator.makeNot(b), c))));
    DD bdd2 = creator.makeAnd(d, e);

    DD composition = creator.makeCompose(bdd1, c.getVariable(), bdd2);

    DD expected =
        creator.makeOr(
            creator.makeAnd(creator.makeNot(a), b),
            creator.makeAnd(a, creator.makeOr(b, creator.makeAnd(creator.makeNot(b), bdd2))));

    assertThat(composition).isEqualTo(expected);
  }

  @Test
  public void composeTest2() {
    DD x1 = creator.makeIthVar(1);
    DD notx1 = creator.makeNot(x1);
    DD x2 = creator.makeIthVar(2);

    DD nand = creator.makeNand(x1, x2);

    DD comp1 = creator.makeCompose(nand, x1.getVariable(), notx1);
    DD comp2 = creator.makeCompose(comp1, x1.getVariable(), notx1);
    DD comp3 = creator.makeCompose(comp2, x1.getVariable(), notx1);

    for (DD originalNand : ImmutableSet.of(nand, comp2)) {
      // x1 NAND x2 -> not x1 OR not x2
      assertThat(originalNand.getLow()).isEqualTo(creator.makeTrue());
      assertThat(originalNand.getHigh().getHigh()).isEqualTo(creator.makeFalse());
      assertThat(originalNand.getHigh().getLow()).isEqualTo(creator.makeTrue());
    }

    for (DD compositition : ImmutableSet.of(comp1, comp3)) {
      // (not x1) NAND x2 -> x1 OR not x2
      if (compositition.getVariable() == x1.getVariable()) {
        assertThat(compositition.getHigh()).isEqualTo(creator.makeTrue());
        assertThat(compositition.getLow().getHigh()).isEqualTo(creator.makeFalse());
        assertThat(compositition.getLow().getLow()).isEqualTo(creator.makeTrue());
      } else {
        assertThat(compositition.getLow()).isEqualTo(creator.makeTrue());
        assertThat(compositition.getHigh().getHigh()).isEqualTo(creator.makeTrue());
        assertThat(compositition.getHigh().getLow()).isEqualTo(creator.makeFalse());
      }
    }
  }

  @Test
  public void creatorTest() {
    assertThat(creator.getVersion()).isEqualTo("1.2");
  }

  @Test
  public void existsTest() {
    DD bdd0 = creator.makeVariable();
    DD bdd1 = creator.makeVariable();
    DD bdd2 = creator.makeVariable();

    DD conjunction = bdd2;
    conjunction = creator.makeAnd(conjunction, bdd1);
    conjunction = creator.makeAnd(conjunction, bdd0);

    DD exquant = creator.makeExists(conjunction, new DD[] {bdd2, bdd1});
    assertThat(bdd0).isEqualTo(exquant);

    DD exquant2 = creator.makeExists(creator.makeTrue(), creator.makeVariable());
    assertThat(exquant2).isEqualTo(creator.makeTrue());

    DD bdd3 = creator.makeVariable();
    DD bdd4 = creator.makeVariable();

    DD dis = creator.makeOr(bdd0, creator.makeOr(bdd1, creator.makeOr(bdd2, bdd4)));

    DD exquant3 = creator.makeExists(dis, bdd3);

    assertThat(exquant3).isEqualTo(dis);
  }

  @Test
  public void implyTest() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeImply(var1, creator.makeTrue());
    assertThat(creator.makeTrue()).isEqualTo(res1);

    DD res2 = creator.makeImply(creator.makeFalse(), var2);
    assertThat(creator.makeTrue()).isEqualTo(res2);

    DD res3 = creator.makeImply(creator.makeTrue(), var2);
    assertThat(res3).isEqualTo(var2);
  }

  @Test
  public void iteTest() {
    DD i = creator.makeVariable();
    DD t = creator.makeVariable();
    DD e = creator.makeVariable();

    DD res1 = creator.makeIte(i, t, t);
    assertThat(res1).isEqualTo(t);

    DD res2 = creator.makeIte(creator.makeTrue(), t, e);
    assertThat(res2).isEqualTo(t);

    DD res3 = creator.makeIte(creator.makeFalse(), t, e);
    assertThat(res3).isEqualTo(e);

    DD res4 = creator.makeIte(i, creator.makeTrue(), creator.makeFalse());
    assertThat(res4).isEqualTo(i);

    DD notI = creator.makeNot(i);
    DD res5 = creator.makeIte(i, creator.makeFalse(), creator.makeTrue());
    assertThat(res5).isEqualTo(notI);

    IfThenElseData iteData1 = new IfThenElseData(i, creator.makeFalse(), creator.makeTrue());

    IfThenElseData iteData2 = new IfThenElseData(i, creator.makeFalse(), creator.makeTrue());
    assertThat(iteData2).isEqualTo(iteData1);

    DD iteD = creator.makeIte(iteData1);
    assertThat(res5).isEqualTo(iteD);
  }

  @Test
  public void iteTestEncodingAsSATRegression() {

    DD x1 = creator.makeIthVar(1);
    DD x2 = creator.makeIthVar(2);
    DD bot = creator.makeFalse();

    // If a then b else c is equivalent to (a AND b) OR (!a AND c)
    DD ite = creator.makeIte(x1, x2, bot);
    DD iteAsFormula =
        creator.makeOr(creator.makeAnd(x1, x2), creator.makeAnd(creator.makeNot(x1), bot));

    assertThat(iteAsFormula).isEqualTo(ite);
  }

  @Test
  public void iteTestEncodingAsSAT() {
    boolean bx1;
    boolean bx2;
    boolean bx3;
    boolean res;

    boolean[][] permutations = {
      {false, false, false},
      {false, false, true},
      {false, true, false},
      {false, true, true},
      {true, false, false},
      {true, false, true},
      {true, true, false},
      {true, true, true},
    };

    DD bot = creator.makeFalse();
    DD top = creator.makeTrue();

    DD x1 = creator.makeIthVar(1);
    DD x2 = creator.makeIthVar(2);
    DD x3 = creator.makeIthVar(3);

    // If a then b else c is equivalent to (a AND b) OR (!a AND c)
    DD ite = creator.makeIte(x1, x2, x3);
    DD iteAsFormula =
        creator.makeOr(creator.makeAnd(x1, x2), creator.makeAnd(creator.makeNot(x1), x3));

    creator.anySat(ite);

    int iteSatCount = creator.satCount(ite).intValueExact();
    int iteAsFormulaSatCount = creator.satCount(iteAsFormula).intValueExact();

    // The number of possible assignments match for the general case
    assertThat(iteAsFormulaSatCount).isEqualTo(iteSatCount);
    assertThat(iteSatCount).isEqualTo(4);

    for (boolean[] permutation : permutations) {
      bx1 = permutation[0];
      bx2 = permutation[1];
      bx3 = permutation[2];

      res = bx1 ? bx2 : bx3;

      x1 = bx1 ? top : bot;
      x2 = bx2 ? top : bot;
      x3 = bx3 ? top : bot;

      DD iteFromBools = creator.makeIte(x1, x2, x3);
      DD iteAsFormulaFromBools =
          creator.makeOr(creator.makeAnd(x1, x2), creator.makeAnd(creator.makeNot(x1), x3));

      assertThat(iteAsFormulaFromBools).isEqualTo(iteFromBools);
      assertThat(creator.satCount(iteAsFormulaFromBools).intValueExact())
          .isEqualTo(creator.satCount(iteFromBools).intValueExact());
      if (res) {
        assertThat(creator.satCount(iteFromBools).intValueExact()).isGreaterThan(0);
      } else {
        assertThat(creator.satCount(iteFromBools).intValueExact()).isEqualTo(0);
      }
    }
  }

  @Test
  public void nAndTest() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    DD res1 = creator.makeNand(var1, creator.makeFalse());
    assertThat(res1).isEqualTo(creator.makeTrue());

    DD res2 = creator.makeNand(creator.makeFalse(), var2);
    assertThat(res2).isEqualTo(creator.makeTrue());

    DD var3 = creator.makeVariable();
    DD var4 = creator.makeVariable();
    DD res3 = creator.makeNand(var3, var4);
    assertThat(res3).isNotEqualTo(creator.makeTrue());

    DD res5 = creator.makeNand(creator.makeFalse(), creator.makeFalse());
    assertThat(res5).isEqualTo(creator.makeTrue());

    DD var6 = creator.makeVariable();
    DD negVar6 = creator.makeNot(var6);
    DD res6 = creator.makeNand(var6, negVar6);
    assertThat(res6).isEqualTo(creator.makeTrue());

    DD var7 = creator.makeVariable();
    DD res7 = creator.makeNand(var7, creator.makeNot(var7));
    assertThat(res7).isEqualTo(creator.makeTrue());
  }

  @Test
  public void initialVarCountCanBeIncreasedWithIthVarDuringRuntime() {
    int initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount + 1; i++) {
      creator.makeIthVar(i);
    }

    assertThat(creator.getVariableCount()).isEqualTo(initialVarcount + 1);

    initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount * 2; i++) {
      creator.makeIthVar(i);
    }

    assertThat(creator.getVariableCount()).isEqualTo(initialVarcount * 2L);
  }

  @Test
  public void norTest() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeNor(var1, creator.makeTrue());
    assertThat(creator.makeFalse()).isEqualTo(res1);

    DD res2 = creator.makeNor(creator.makeTrue(), var2);
    assertThat(res2).isEqualTo(creator.makeFalse());
  }

  @Test
  public void notTest() {
    DD bdd = creator.makeVariable();
    DD not = creator.makeNot(bdd);

    assertThat(not.getLow()).isEqualTo(bdd.getHigh());
    assertThat(not.getHigh()).isEqualTo(bdd.getLow());

    DD notZero = creator.makeNot(creator.makeFalse());
    assertThat(notZero).isEqualTo(creator.makeTrue());

    DD notOne = creator.makeNot(creator.makeTrue());
    assertThat(notOne).isEqualTo(creator.makeFalse());
  }

  @Test
  public void orTest() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeOr(var1, var1);
    assertThat(res1).isEqualTo(var1);

    DD res2 = creator.makeOr(var1, creator.makeFalse());
    assertThat(res2).isEqualTo(var1);

    DD res3 = creator.makeOr(var1, creator.makeTrue());
    assertThat(res3).isEqualTo(creator.makeTrue());

    DD res4 = creator.makeOr(creator.makeTrue(), var2);
    assertThat(res4).isEqualTo(creator.makeTrue());

    DD res5 = creator.makeOr(creator.makeFalse(), var2);
    assertThat(res5).isEqualTo(var2);
  }

  @Test
  public void replaceTest() {
    DD bdd0 = creator.makeIthVar(0);
    DD bdd1 = creator.makeIthVar(1);
    DD bdd2 = creator.makeIthVar(2);
    DD bdd3 = creator.makeIthVar(3);
    DD conjunction = creator.makeAnd(bdd0, bdd1);
    conjunction = creator.makeAnd(conjunction, creator.makeNot(bdd2));

    DD replace = creator.makeReplace(conjunction, bdd1, bdd3);

    DD expected = creator.makeAnd(bdd0, creator.makeNot(bdd2));
    expected = creator.makeAnd(expected, bdd3);

    assertThat(replace).isEqualTo(expected);
  }

  @Test
  public void satCountTestGeneral() {
    DD a = creator.makeVariable();
    DD b = creator.makeVariable();

    DD or = creator.makeOr(creator.makeVariable(), creator.makeVariable());
    for (int i = 0; i < 4; i++) {
      or = creator.makeOr(or, creator.makeVariable());
    }

    DD ite = creator.makeIte(a, b, or);

    assertThat(creator.satCount(ite)).isEqualTo(BigInteger.valueOf(190));
  }

  @Test
  public void satCountAnd() {
    DD and = creator.makeAnd(creator.makeVariable(), creator.makeVariable());
    for (int i = 0; i < 9; i++) {
      and = creator.makeAnd(and, creator.makeVariable());
    }

    assertThat(creator.satCount(and)).isEqualTo(BigInteger.valueOf(1));
  }

  @Test
  public void satCountOr() {
    DD or = creator.makeOr(creator.makeVariable(), creator.makeVariable());
    for (int i = 0; i < 8; i++) {
      or = creator.makeOr(or, creator.makeVariable());
    }

    assertThat(creator.satCount(or)).isEqualTo(BigInteger.valueOf(1023));
  }

  @Test
  public void satCountTestQueens() {
    NQueens queens = new NQueens(4, creator);
    queens.build();
    assertThat(queens.solve()).isEqualTo(BigInteger.valueOf(2));

    NQueens queens2 = new NQueens(8, creator);
    queens2.build();
    assertThat(queens2.solve()).isEqualTo(BigInteger.valueOf(92));
  }

  @Test
  public void satCountTestSimple() {
    DD a = creator.makeVariable();
    DD b = creator.makeVariable();
    DD c = creator.makeVariable();
    DD d = creator.makeVariable();

    DD aAndB = creator.makeAnd(a, b);
    // Only the and has 1 sat assignment
    assertThat(creator.satCount(aAndB)).isEqualTo(BigInteger.valueOf(1));

    DD bAndCAndD = creator.makeAnd(b, creator.makeAnd(d, c));
    assertThat(creator.satCount(bAndCAndD)).isEqualTo(BigInteger.valueOf(1));
    DD or = creator.makeOr(aAndB, bAndCAndD);
    // a and b true is sat, c and d irrelevant -> 2^2 = 4
    // b, d, c = true is sat -> 2^1 = 2
    // but the assignment a,b,c,d true is shared between both so -1
    assertThat(creator.satCount(or)).isEqualTo(BigInteger.valueOf(5));

    DD ite = creator.makeIte(a, b, c);
    assertThat(creator.satCount(ite)).isEqualTo(BigInteger.valueOf(4));

    DD iteAnd = creator.makeAnd(creator.makeAnd(a, b), ite);
    // since a and b must be true, only one possible ite assignment
    assertThat(creator.satCount(iteAnd)).isEqualTo(BigInteger.valueOf(1));

    DD e = creator.makeVariable();
    DD f = creator.makeVariable();
    DD eAndF = creator.makeAnd(e, f);
    assertThat(creator.satCount(eAndF)).isEqualTo(BigInteger.valueOf(1));
    DD or2 = creator.makeOr(eAndF, or);
    // a and b true is sat, c, d, e, f irrelevant -> 2^4 = 16
    // b, d, c = true is sat -> 2^3 = 8
    // e, f = true -> 2^4 = 16
    // minus shared assignments (-9)
    assertThat(creator.satCount(or2)).isEqualTo(BigInteger.valueOf(31));
  }

  @Test
  public void testSimpleBDDStats() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    Statistics.DDStats ddStats = Statistics.of(var1);

    assertThat(ddStats.getNodeCount()).isEqualTo(3);
    assertThat(ddStats.getEdgeCount()).isEqualTo(2);

    DD res3 = creator.makeAnd(var1, creator.makeFalse());
    Statistics.DDStats ddStats2 = Statistics.of(res3);

    assertThat(ddStats2.getNodeCount()).isEqualTo(1);
    assertThat(ddStats2.getEdgeCount()).isEqualTo(0);

    DD res = creator.makeAnd(var1, var2);
    Statistics.DDStats ddStats3 = Statistics.of(res);

    assertThat(ddStats3.getNodeCount()).isEqualTo(4);
    assertThat(ddStats3.getEdgeCount()).isEqualTo(4);
  }

  @Test
  public void testNQueensStats() {
    NQueens nQueens = new NQueens(4, creator);
    nQueens.build();
    Statistics.DDStats ddStats1 = Statistics.of(nQueens.solution());
    assertThat(ddStats1.getNodeCount()).isEqualTo(31);
    assertThat(ddStats1.getEdgeCount()).isEqualTo(58);
  }

  @Test
  public void xOrTest() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeXor(var1, var1);
    assertThat(res1).isEqualTo(creator.makeFalse());

    DD res2 = creator.makeXor(creator.makeFalse(), var2);
    assertThat(res2).isEqualTo(var2);

    DD res3 = creator.makeXor(var1, creator.makeFalse());
    assertThat(res3).isEqualTo(var1);
  }
}
