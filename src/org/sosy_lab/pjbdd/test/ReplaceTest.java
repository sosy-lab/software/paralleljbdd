// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'EXISTENTIAL QUANTIFICATION' test class uses {@link CreatorCombinatorTest} as base class to
 * perform make 'EXISTENTIAL QUANTIFICATION' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class ReplaceTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    DD bdd0 = creator.makeIthVar(0);
    DD bdd1 = creator.makeIthVar(1);
    DD bdd2 = creator.makeIthVar(2);
    DD bdd3 = creator.makeIthVar(3);
    DD conjunction = creator.makeAnd(bdd0, bdd1);
    conjunction = creator.makeAnd(conjunction, creator.makeNot(bdd2));

    DD replace = creator.makeReplace(conjunction, bdd1, bdd3);

    DD expected = creator.makeAnd(bdd0, creator.makeNot(bdd2));
    expected = creator.makeAnd(expected, bdd3);

    assertThat(replace).isEqualTo(expected);
  }
}
