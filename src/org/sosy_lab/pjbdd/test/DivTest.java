// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.sosy_lab.pjbdd.api.DD;

public class DivTest extends ZDDCombinatorTest {

  @Test
  public void test() {
    DD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    DD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    DD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    DD bdd0 = creator.union(var2, var1);
    DD bdd1 = creator.union(bdd0, var0);

    DD div0 = creator.division(creator.empty(), bdd1);
    assertThat(div0).isEqualTo(creator.empty());

    DD div1 = creator.division(creator.base(), bdd1);
    assertThat(div1).isEqualTo(creator.empty());

    DD div2 = creator.division(bdd1, bdd1);
    assertThat(div2).isEqualTo(creator.base());

    DD div3 = creator.division(bdd1, creator.base());
    assertThat(div3).isEqualTo(bdd1);
  }
}
