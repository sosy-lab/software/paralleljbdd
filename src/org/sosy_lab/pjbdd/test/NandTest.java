// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'NAND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'NAND'
 * test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class NandTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    DD res1 = creator.makeNand(var1, creator.makeFalse());
    assertThat(creator.makeTrue()).isEqualTo(res1);

    DD res2 = creator.makeNand(creator.makeFalse(), var2);
    assertThat(creator.makeTrue()).isEqualTo(res2);

    DD var3 = creator.makeVariable();
    DD var4 = creator.makeVariable();
    DD res3 = creator.makeNand(var3, var4);
    assertThat(res3).isNotEqualTo(creator.makeTrue());

    DD var5 = creator.makeVariable();
    DD res4 = creator.makeNand(var5, creator.makeFalse());
    assertThat(res4).isEqualTo(creator.makeTrue());

    DD res5 = creator.makeNand(creator.makeFalse(), creator.makeFalse());
    assertThat(res5).isEqualTo(creator.makeTrue());

    DD var6 = creator.makeVariable();
    DD negVar6 = creator.makeNot(var6);
    DD res6 = creator.makeNand(var6, negVar6);
    assertThat(res6).isEqualTo(creator.makeTrue());

    DD var7 = creator.makeVariable();
    DD res7 = creator.makeNand(var7, creator.makeNot(var7));
    assertThat(res7).isEqualTo(creator.makeTrue());
  }
}
