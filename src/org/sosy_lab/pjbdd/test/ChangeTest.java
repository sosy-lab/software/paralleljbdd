// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.sosy_lab.pjbdd.api.DD;

public class ChangeTest extends ZDDCombinatorTest {

  @Test
  public void test() {
    DD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    DD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    DD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    DD bdd0 = creator.union(var2, var1);
    DD bdd1 = creator.union(bdd0, var0);

    DD change0 = creator.change(bdd0, var0);
    assertThat(change0.getVariable()).isEqualTo(var0.getVariable());
    assertThat(change0.getLow()).isEqualTo(creator.empty());
    assertThat(change0.getHigh()).isEqualTo(bdd0);
    DD change1 = creator.change(bdd1, var0);
    assertThat(change1.getVariable()).isEqualTo(var0.getVariable());
    assertThat(change1.getHigh()).isEqualTo(bdd1.getLow());
    assertThat(change1.getLow()).isEqualTo(bdd1.getHigh());
  }
}
