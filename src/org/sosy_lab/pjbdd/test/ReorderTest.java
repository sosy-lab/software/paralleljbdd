// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static com.google.common.truth.Truth.assertThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.examples.Example;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.util.parser.DotExporter;

@RunWith(Parameterized.class)
public class ReorderTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    skipIfChained();

    DD bdd1 = creator.makeNode(creator.makeFalse(), creator.makeTrue(), 0);
    DD bdd2 = creator.makeNode(creator.makeFalse(), creator.makeTrue(), 1);

    DD orBDD = creator.makeOr(bdd1, bdd2);
    Example example = new NQueens(4, creator);
    example.build();

    List<Integer> newOrder = new ArrayList<>();
    newOrder.add(1);
    newOrder.add(0);
    creator.setVarOrder(newOrder);

    assertThat(orBDD.getVariable()).isEqualTo(1);

    String reorderedNQueens = new DotExporter().bddToString(example.solution());
    Example example2 = new NQueens(4, creator);
    example2.build();

    String reorderedNQueens2 = new DotExporter().bddToString(example2.solution());
    assertThat(reorderedNQueens).isEqualTo(reorderedNQueens2);
  }
}
