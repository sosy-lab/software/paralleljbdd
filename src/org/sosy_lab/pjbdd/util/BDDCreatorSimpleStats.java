// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

import java.math.BigInteger;
import java.util.logging.Logger;
import org.sosy_lab.pjbdd.api.Creator;

public class BDDCreatorSimpleStats implements Creator.Stats {

  private static final String statsNotTrackedMSG = "Statistic not tracked.";

  private final int uniqueTableSize;
  private final int nodeCount;
  private final int cacheSize;
  private final int cacheNodeCount;
  private final int variableCount;

  public BDDCreatorSimpleStats(
      int pUniqueTableSize,
      int pUniqueTableNodeCount,
      int pCacheSize,
      int pCacheNodeCount,
      int pVariableCount) {
    uniqueTableSize = pUniqueTableSize;
    nodeCount = pUniqueTableNodeCount;
    cacheSize = pCacheSize;
    cacheNodeCount = pCacheNodeCount;
    variableCount = pVariableCount;
  }

  @Override
  public String prettyPrint() {
    return "Uniquetable : "
        + getNodeCount()
        + "/"
        + uniqueTableSize
        + "\n"
        + "Cache: "
        + cacheSize
        + "/"
        + getCacheNodeCount()
        + "\n"
        + "Variable count: "
        + variableCount;
  }

  @Override
  public int getNodeCount() {
    return nodeCount;
  }

  @Override
  public int getUniqueTableSize() {
    return uniqueTableSize;
  }

  @Override
  public int getVariableCount() {
    return variableCount;
  }

  @Override
  public int getCacheNodeCount() {
    return cacheNodeCount;
  }

  @Override
  public BigInteger getGarbageCollections() {
    Logger.getLogger(BDDCreatorSimpleStats.class.getName()).info(statsNotTrackedMSG);
    return null;
  }

  @Override
  public BigInteger getCacheHits() {
    Logger.getLogger(BDDCreatorSimpleStats.class.getName()).info(statsNotTrackedMSG);
    return null;
  }

  @Override
  public BigInteger resizeOperations() {
    Logger.getLogger(BDDCreatorSimpleStats.class.getName()).info(statsNotTrackedMSG);
    return null;
  }

  @Override
  public BigInteger resizeTime() {
    Logger.getLogger(BDDCreatorSimpleStats.class.getName()).info(statsNotTrackedMSG);
    return null;
  }

  @Override
  public BigInteger gcTime() {
    Logger.getLogger(BDDCreatorSimpleStats.class.getName()).info(statsNotTrackedMSG);
    return null;
  }

  @Override
  public BigInteger computationTime() {
    Logger.getLogger(BDDCreatorSimpleStats.class.getName()).info(statsNotTrackedMSG);
    return null;
  }
}
