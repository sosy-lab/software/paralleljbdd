// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;

/**
 * {@link Importer} implementation which imports {@link DD} from following custom String format:
 *
 * <p>Each node will be represented by a single line with 4 int values separated by ';' as follows:
 * index;variable;low_index;high_index there will be one header line as follows:
 * root_index;var_count;var_lvl1,var_lvl2,var_lvl3...
 *
 * @author Stephan Holzner
 * @see Importer
 * @since 1.0
 */
public class BDDStringImporter implements Importer<DD> {

  /** Enumeration cash, cashing already imported {@link DD}s to indices. */
  private final Map<Integer, DD> cash;

  /** the boolean_operations which imports the {@link DD}. */
  private final Creator nodeFactory;

  /**
   * Creates new {@link BDDStringImporter} instances.
   *
   * @param nodeFactory - the boolean_operations which imports the {@link DD}
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public BDDStringImporter(Creator nodeFactory) {
    this.nodeFactory = nodeFactory;
    this.cash = new HashMap<>();
  }

  /** {@inheritDoc} */
  @Override
  public DD importFromLinesStream(Stream<String> lines) {
    Iterator<String> nodeIterator = lines.iterator();
    int root = importHeader(nodeIterator.next());
    while (nodeIterator.hasNext()) {
      initBDDFromLine(nodeIterator.next());
    }
    lines.close();
    DD bdd = cash.get(root);
    cash.clear();
    return bdd;
  }

  /**
   * Import header and init {@link #nodeFactory} from line.
   *
   * @param line - header line
   */
  private int importHeader(String line) {
    if (line.isEmpty()) {
      throw new ImportExportException(ImportExportException.ErrorCodes.InvalidFileFormat);
    }
    StringTokenizer tokenizer = new StringTokenizer(line, ";");
    try {
      int root = Integer.parseInt(tokenizer.nextToken());
      int count = Integer.parseInt(tokenizer.nextToken());
      nodeFactory.setVariableCount(count);
      StringTokenizer varOrderTokenizer = new StringTokenizer(tokenizer.nextToken(), ",");
      List<Integer> varOrder = new ArrayList<>();
      while (varOrderTokenizer.hasMoreElements()) {
        varOrder.add(Integer.parseInt(varOrderTokenizer.nextToken()));
      }
      nodeFactory.setVarOrder(varOrder);
      return root;
    } catch (NoSuchElementException | NumberFormatException e) {
      throw new ImportExportException(ImportExportException.ErrorCodes.InvalidFileFormat);
    }
  }

  /**
   * Init a bdd from line and puts it into the {@link #cash}.
   *
   * @param line - the line
   */
  private void initBDDFromLine(String line) {

    StringTokenizer tokenizer = new StringTokenizer(line, ";");

    try {
      int index = Integer.parseInt(tokenizer.nextToken());
      if (index == 0) {
        cash.put(index, nodeFactory.makeFalse());
      } else if (index == 1) {
        cash.put(index, nodeFactory.makeTrue());
      } else {
        int var = Integer.parseInt(tokenizer.nextToken());
        int low = Integer.parseInt(tokenizer.nextToken());
        int high = Integer.parseInt(tokenizer.nextToken());

        assert cash.containsKey(low);
        assert cash.containsKey(high);
        cash.put(index, nodeFactory.makeNode(cash.get(low), cash.get(high), var));
      }
    } catch (NoSuchElementException | NumberFormatException e) {
      throw new ImportExportException(ImportExportException.ErrorCodes.InvalidFileFormat);
    }
  }
}
