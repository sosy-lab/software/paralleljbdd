// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;

/**
 * {@link Importer} implementation which imports {@link DD} from Dot format.
 *
 * @author Stephan Holzner
 * @see Importer
 * @since 1.0
 */
public class DotImporter implements Importer<DD> {

  /** Cash for already imported {@link DD} nodes. */
  private final Map<Integer, DD> cash;

  /** Index Cash for high childes. */
  private final Map<Integer, Integer> highCash;

  /** Index Cash for low childes. */
  private final Map<Integer, Integer> lowCash;

  /** Index Cash for variables. */
  private final Map<Integer, Integer> varCash;

  /** variable ordering. */
  private final List<Integer> variableOrder;

  /** the boolean_operations which imports the {@link DD}. */
  private final Creator nodeFactory;

  /**
   * Creates new {@link DotImporter} instances.
   *
   * @param nodeFactory - the boolean_operations which imports the {@link DD}
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public DotImporter(Creator nodeFactory) {
    this.nodeFactory = nodeFactory;
    this.cash = new HashMap<>();
    highCash = new HashMap<>();
    lowCash = new HashMap<>();
    varCash = new LinkedHashMap<>();
    variableOrder = new ArrayList<>();
  }

  /** {@inheritDoc} */
  @Override
  public DD importFromLinesStream(Stream<String> lines) {
    lines.forEach(this::initBDDFromLine);
    return buildBDD();
  }

  /**
   * Build the bdd after index caches are constructed from String.
   *
   * @return the imported bdd
   */
  private DD buildBDD() {
    DD result = nodeFactory.makeFalse();
    for (Integer index : variableOrder) {
      int var = varCash.get(index);
      if (var < 0) {
        result = (var == -1) ? nodeFactory.makeTrue() : nodeFactory.makeFalse();
        cash.put(index, result);
        continue;
      }
      int low = lowCash.get(index);
      int high = highCash.get(index);
      DD lowBDD = cash.get(low);
      DD highBDD = cash.get(high);
      result = nodeFactory.makeNode(lowBDD, highBDD, var);
      cash.put(index, result);
    }
    this.clean();
    return result;
  }

  /** Clean all cashes after imports. */
  private void clean() {
    cash.clear();
    highCash.clear();
    lowCash.clear();
    varCash.clear();
    variableOrder.clear();
  }

  /**
   * Read line and import the bdd in cashes.
   *
   * @param line - the line to be imported
   */
  private void initBDDFromLine(String line) {
    StringTokenizer tokenizer = new StringTokenizer(line.trim(), ";\n");

    String next;
    try {
      while (tokenizer.hasMoreTokens()) {
        next = tokenizer.nextToken();
        if (next.contains("->")) {
          importEdge(next);
        } else if (next.matches("(\\d+\\[label=x\\d+.*)")) {
          importNode(next);
        } else if (next.matches("(\\d+\\[label=\\d+.*)")) {
          importTrueOrFalse(next);
        }
      }
    } catch (NoSuchElementException | NumberFormatException e) {
      throw new ImportExportException(ImportExportException.ErrorCodes.InvalidFileFormat);
    }
  }

  /**
   * Import leaf from line.
   *
   * @param next - the line
   */
  private void importTrueOrFalse(String next) {
    StringTokenizer tokenizer =
        new StringTokenizer(next.replaceAll("\\[label", "").replaceAll("\\]", ""), "=");
    int node = Integer.parseInt(tokenizer.nextToken());
    int leaf = Integer.parseInt(tokenizer.nextToken());
    int var = (leaf == 0) ? -2 : -1;
    varCash.put(node, var);
  }

  /**
   * Import non terminal node from line.
   *
   * @param next - the line
   */
  private void importNode(String next) {
    StringTokenizer tokenizer =
        new StringTokenizer(next.replaceAll("\\[label=", "").replaceAll("\\]", ""), "x");
    int node = Integer.parseInt(tokenizer.nextToken());
    int var = Integer.parseInt(tokenizer.nextToken());
    varCash.put(node, var);
  }

  /**
   * Import edge from line.
   *
   * @param next - the line
   */
  private void importEdge(String next) {
    StringTokenizer tokenizer = new StringTokenizer(next, "->");
    int parent = Integer.parseInt(tokenizer.nextToken());
    int child = Integer.parseInt(tokenizer.nextToken().replaceAll("\\[style=dashed\\]", ""));
    checkOrder(parent, child);
    if (next.contains("[style=dashed]")) {
      lowCash.put(parent, child);
    } else {
      highCash.put(parent, child);
    }
  }

  /**
   * Helper to resolve current variable order. Therefore put parent and child's variables in correct
   * order.
   *
   * @param parent - parent's variable
   * @param child - child's variable
   */
  private void checkOrder(int parent, int child) {
    if (variableOrder.contains(parent) && variableOrder.contains(child)) {
      int indexParent = variableOrder.indexOf(parent);
      int indexChild = variableOrder.indexOf(child);
      if (indexChild > indexParent) {
        variableOrder.remove(indexChild);
        variableOrder.add(indexParent, child);
      }
    } else if (variableOrder.contains(parent) && !variableOrder.contains(child)) {
      variableOrder.add(variableOrder.indexOf(parent), child);
    } else {
      if (!variableOrder.contains(child)) {
        variableOrder.add(child);
      }
      if (!variableOrder.contains(parent)) {
        variableOrder.add(parent);
      }
    }
  }
}
