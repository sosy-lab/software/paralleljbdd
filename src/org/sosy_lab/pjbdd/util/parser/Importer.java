// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Main interface defining methods importing a {@link DD} from String representation or {@link
 * File}.
 *
 * @author Stephan Holzner
 * @see DD
 * @since 1.0
 */
public interface Importer<V extends DD> {

  /**
   * import bdd from String(line) stream.
   *
   * @param lines - lines Stream
   * @return imported bdd
   */
  V importFromLinesStream(Stream<String> lines);

  /**
   * import bdd from file.
   *
   * @param file - specified path
   * @return imported bdd
   */
  default V importBDDFromFile(Path file) {
    try (Stream<String> lines = Files.lines(file, StandardCharsets.UTF_8)) {
      return importFromLinesStream(lines);
    } catch (IOException e) {
      throw new ImportExportException(ImportExportException.ErrorCodes.FileOpen);
    }
  }

  /**
   * import bdd from path.
   *
   * @param path - specified path
   * @return imported bdd
   */
  default V importBDDFromPath(String path) {
    return importBDDFromFile(Path.of(path));
  }

  /**
   * import bdd from string.
   *
   * @param node - specified string
   * @return imported bdd
   */
  default V bddFromString(String node) {
    return importFromLinesStream(Stream.of(node.split("\n")));
  }
}
