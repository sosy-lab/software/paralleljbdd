// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.parser;

import java.util.HashMap;
import java.util.Map;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;

/**
 * {@link Exporter} implementation which exports {@link DD} in following custom String format:
 *
 * <p>Each node will be represented by a single line with 4 int values separated by ';' as follows:
 * index;variable;low_index;high_index there will be one header line as follows:
 * root_index;var_count;var_lvl1,var_lvl2,var_lvl3...
 *
 * @author Stephan Holzner
 * @see Exporter
 * @since 1.0
 */
public class BDDStringExporter implements Exporter<DD> {

  /** the {@link DD}s boolean_operations context. */
  private final Creator creator;

  /**
   * Creates new {@link BDDStringExporter} instances.
   *
   * @param creator - the boolean_operations context
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public BDDStringExporter(Creator creator) {
    this.creator = creator;
  }

  /**
   * Recursive fills {@link StringBuilder} with bdd nodes child String representations. Afterwards
   * nodes String representation will be appended.
   *
   * @param bdd - the node
   * @param cash - cashed used for enumerate bdd nodes
   * @param buffer - {@link StringBuilder} as buffer
   * @return the nodes String representation as {@link StringBuilder}
   */
  private StringBuilder toBDDString(DD bdd, Map<DD, Integer> cash, StringBuilder buffer) {
    if (!cash.containsKey(creator.getLow(bdd))) {
      toBDDString(creator.getLow(bdd), cash, buffer);
    }
    if (!cash.containsKey(creator.getHigh(bdd))) {
      toBDDString(creator.getHigh(bdd), cash, buffer);
    }
    cash.putIfAbsent(bdd, cash.size());
    return buffer
        .append(cash.get(bdd))
        .append(";")
        .append(bdd.getVariable())
        .append(";")
        .append(cash.get(creator.getLow(bdd)))
        .append(";")
        .append(cash.get(creator.getHigh(bdd)))
        .append("\n");
  }

  /** {@inheritDoc} */
  @Override
  public String extension() {
    return ".txt";
  }

  /** {@inheritDoc} */
  @Override
  public String bddToString(DD node) {
    Map<DD, Integer> cash = new HashMap<>();
    cash.put(creator.makeFalse(), 0);
    cash.put(creator.makeTrue(), 1);
    StringBuilder buffer = new StringBuilder(node.getVariable() * 4 * 3);
    toBDDString(node, cash, buffer.append("0\n").append("1\n"));
    StringBuilder result = makeHeaderLine(node, cash.size() - 1);
    return result.append(buffer).toString();
  }

  /**
   * Make header line for exported {@link DD} node with format:
   * root_index;var_count;var_lvl1,var_lvl2,var_lvl3...
   *
   * @param node - the {@link DD} node
   * @param index - the enumerated {@link DD} value
   * @return header line as {@link StringBuilder}
   */
  private StringBuilder makeHeaderLine(DD node, int index) {
    StringBuilder builder = new StringBuilder(node.getVariable() * 3);
    builder.append(index).append(";").append(creator.getVariableCount()).append(";");
    int[] varOrdering = creator.getVariableOrdering();
    for (int i : varOrdering) {
      builder.append(i).append(",");
    }
    return builder.append("\n");
  }
}
