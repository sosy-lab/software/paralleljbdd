// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Main interface defining methods exporting a {@link DD} to String or {@link File}.
 *
 * @author Stephan Holzner
 * @see DD
 * @since 1.0
 */
public interface Exporter<V extends DD> {

  /** default export directory. */
  String DEFAULT_PATH = "./res/";

  /**
   * Export a {@link DD} to exporters string format.
   *
   * @param node - node to be exported
   * @return the string representation
   */
  String bddToString(V node);

  /**
   * get exporters file extension.
   *
   * @return exporters file extension
   */
  String extension();

  /**
   * Export a {@link DD} to file with given path and name.
   *
   * @param graph - a {@link DD}
   * @param path - the filepath
   * @param name - the filename
   * @return created file
   */
  default File export(V graph, String path, String name) {
    Path file = Path.of(path + name + extension());
    write(file, bddToString(graph));
    return file.toFile();
  }

  /**
   * Export a {@link DD} to file with given path and default name.
   *
   * @param graph - a {@link DD}
   * @param path - the filepath
   * @return created file
   */
  default File export(V graph, String path) {
    return export(graph, path, generateFromTimeStamp());
  }

  /**
   * Export a {@link DD} to file with default path and default name.
   *
   * @param bdd - the node to be exported
   * @return created file
   */
  default File export(V bdd) {
    return export(bdd, DEFAULT_PATH);
  }

  /**
   * Generate default filename from time stamp.
   *
   * @return current time stamp as string representation
   */
  default String generateFromTimeStamp() {
    Instant instant = Instant.now();
    String s = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate().toString();
    s += "/" + LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime();
    return s;
  }

  /**
   * Write the bdd in string to a given file.
   *
   * @param file - target file's path
   * @param stringBDD - string bdd representation to be written
   */
  default void write(Path file, String stringBDD) {
    try {
      Files.createDirectories(file.getParent());
      Files.write(file, stringBDD.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      throw new ImportExportException(ImportExportException.ErrorCodes.FileOpen);
    }
  }
}
