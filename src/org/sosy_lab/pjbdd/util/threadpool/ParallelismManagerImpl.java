// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.threadpool;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Default {@link ParallelismManager} implementation. Internally uses a {@link ForkJoinPool} as
 * worker thread pool.
 *
 * <p>To help avoiding over threading {@link #canFork(int)} respects forking blocks and the number
 * of already submitted tasks in relation to number of worker threads.
 *
 * @author Stephan Holzner
 * @see ParallelismManager
 * @since 1.0
 */
public class ParallelismManagerImpl implements ParallelismManager {

  /** the internal worker thread pool. */
  private final ForkJoinPool threadPool;

  /** the number of submitted tasks. */
  private final AtomicInteger submissionCount = new AtomicInteger();

  /** the number of worker threads. */
  private final int parallelism;

  /**
   * Creates new {@link ParallelismManagerImpl} instances with: {@link #parallelism} = {@link
   * Runtime#availableProcessors()}.
   */
  public ParallelismManagerImpl() {
    this(Runtime.getRuntime().availableProcessors());
  }

  /**
   * Creates new {@link ParallelismManagerImpl} instances with.
   *
   * @param parallelism - number of worker threads
   */
  public ParallelismManagerImpl(int parallelism) {
    this.parallelism = parallelism;
    threadPool = new ForkJoinPool(parallelism);
  }

  /** {@inheritDoc} */
  @Override
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public ForkJoinPool getThreadPool() {
    return threadPool;
  }

  /** {@inheritDoc} */
  @Override
  public int getParallelism() {
    return parallelism;
  }

  /** {@inheritDoc} */
  @Override
  public boolean canFork(int topLevel) {
    return submissionCount.get() <= getParallelism();
  }

  /** {@inheritDoc} */
  @Override
  public void taskSupplied() {
    submissionCount.incrementAndGet();
  }

  /** {@inheritDoc} */
  @Override
  public void taskDone() {
    submissionCount.decrementAndGet();
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    threadPool.shutdown();
  }
}
