// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

import java.util.Arrays;
import java.util.Set;

/**
 * Helper class to perform some array operations.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public final class IntArrayUtils {

  /** empty constructor to avoid instantiation. */
  private IntArrayUtils() {}

  /**
   * Transform java Set of Integer to int[].
   *
   * @param set - the set to transform
   * @return the int[]
   */
  public static int[] toIntArray(Set<Integer> set) {
    int[] a = new int[set.size()];
    int i = 0;
    for (Integer val : set) {
      a[i++] = val;
    }
    return a;
  }

  /**
   * Get a subarray with given range.
   *
   * @param arr - the array
   * @param from - start index inclusive
   * @param to - end index exclusive
   * @return the subarray
   */
  public static int[] subArray(int[] arr, int from, int to) {
    return Arrays.copyOfRange(arr, from, to);
  }

  /**
   * Checks if two arrays have same content.
   *
   * @param levels - the first array
   * @param mValues - the second array
   * @return equality of array content.
   */
  public static boolean equals(int[] levels, int[] mValues) {
    if (levels == mValues) {
      return true;
    }
    if (levels.length != mValues.length) {
      return false;
    }
    for (int i = 0; i < levels.length; i++) {
      if (mValues[i] != levels[i]) {
        return false;
      }
    }
    return true;
  }
}
