// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

import java.math.BigInteger;

/**
 * Helper class to perform some prime operations. Uses {@link BigInteger#nextProbablePrime()} and
 * {@link BigInteger#isProbablePrime(int)}.
 *
 * @author Stephan Holzner
 * @see BigInteger
 * @since 1.0
 */
public final class PrimeUtils {

  /** empty constructor to avoid instantiation. */
  private PrimeUtils() {}

  /**
   * Check if input is even.
   *
   * @param i - input is even
   * @return <code>true</code> if input is even, <code>false</code> else
   */
  private static boolean isEven(int i) {
    return (i & 0x1) == 0;
  }

  /**
   * Get inputs greater next prime, or input if it's prime.
   *
   * @param src - the input
   * @return inputs greater next prime
   */
  public static int getGreaterNextPrime(int src) {

    if (isEven(src)) {
      ++src;
    }

    BigInteger bigInteger = BigInteger.valueOf(src);
    if (bigInteger.isProbablePrime(20)) {
      return src;
    }
    return bigInteger.nextProbablePrime().intValue();
  }

  /**
   * Get inputs lower next prime, or input if it's prime.
   *
   * @param src - the input
   * @return inputs lower next prime
   */
  public static int getLowerNextPrime(int src) {

    if (isEven(src)) {
      --src;
    }

    if (BigInteger.valueOf(src).isProbablePrime(20)) {
      return src;
    }
    return getLowerNextPrime(src - 2);
  }
}
