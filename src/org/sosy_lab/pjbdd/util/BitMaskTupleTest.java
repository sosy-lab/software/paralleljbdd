// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;

public class BitMaskTupleTest {

  @Test
  public void testBitMasking() {
    int tuple = BitMaskIntTupleUtils.createTuple(100, 122);

    int first = BitMaskIntTupleUtils.getFirst(tuple);
    int second = BitMaskIntTupleUtils.getSecond(tuple);

    assertThat(first).isEqualTo(100);
    assertThat(second).isEqualTo(122);

    tuple = BitMaskIntTupleUtils.createTuple(0, 32000);

    first = BitMaskIntTupleUtils.getFirst(tuple);
    second = BitMaskIntTupleUtils.getSecond(tuple);

    assertThat(first).isEqualTo(0);
    assertThat(second).isEqualTo(32000);

    tuple = BitMaskIntTupleUtils.createTuple(32001, 0);

    first = BitMaskIntTupleUtils.getFirst(tuple);
    second = BitMaskIntTupleUtils.getSecond(tuple);

    assertThat(first).isEqualTo(32001);
    assertThat(second).isEqualTo(0);

    tuple = BitMaskIntTupleUtils.createTuple(Short.MAX_VALUE - 1, Short.MAX_VALUE - 1);

    first = BitMaskIntTupleUtils.getFirst(tuple);
    second = BitMaskIntTupleUtils.getSecond(tuple);

    assertThat(first).isEqualTo(Short.MAX_VALUE - 1);
    assertThat(second).isEqualTo(Short.MAX_VALUE - 1);
  }
}
