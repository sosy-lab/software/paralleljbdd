// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

/**
 * Helper class to generate hash codes from various int inputs.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public final class HashCodeGenerator {

  /** empty constructor to avoid instantiation. */
  private HashCodeGenerator() {}

  /**
   * generates hashcode for four input ints.
   *
   * @param a - one
   * @param b - two
   * @param c - three
   * @param d - four
   * @return hashcode
   */
  public static int generateHashCode(int a, int b, int c, int d) {
    return hashPrime(a, b, c, d); // generateHashCode(c, generateHashCode(a, b)));
  }

  /**
   * generates hashcode for three input ints.
   *
   * @param a - getIf value
   * @param b - getThen value
   * @param c - getElse value
   * @return hashcode
   */
  public static int generateHashCode(int a, int b, int c) {
    return hashPrime(a, b, c); // generateHashCode(c, generateHashCode(a, b)));
  }

  /**
   * generates hashcode for two input ints.
   *
   * <p>Uses the Cantor pairing function to uniquely encode two natural numbers into a single
   * natural number, see <a href="https://en.wikipedia.org/wiki/Pairing_function">Cantor pairing
   * function</a>
   *
   * @param a - getIf value
   * @param b - getThen value
   * @return hashcode
   */
  public static int generateHashCode(int a, int b) {
    return ((a + b) * (a + b + 1) / 2 + a);
  }

  private static final int P1 = 12582917;
  private static final int P2 = 4256249;
  private static final int P3 = 741457;
  private static final int P4 = 90031;

  /**
   * prime-hash for three elements.
   *
   * @param a - getIf value
   * @param b - getThen value
   * @param c - getElse value
   * @return hashcode
   */
  public static int hashPrime(int a, int b, int c) {
    return (a * P1) + (b * P2) + (c * P3);
  }

  /**
   * prime-hash for three elements.
   *
   * @param a - one
   * @param b - two
   * @param c - three
   * @param d - four
   * @return hashcode
   */
  public static int hashPrime(int a, int b, int c, int d) {
    return (a * P1) + (b * P2) + (c * P3) + (d * P4);
  }
}
