// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

import com.google.common.base.Preconditions;

public final class BitMaskIntTupleUtils {

  private BitMaskIntTupleUtils() {}

  private static final int MASK = Short.MAX_VALUE;

  public static int getFirst(int tuple) {
    return tuple & MASK;
  }

  public static int getSecond(int tuple) {
    return tuple >> 16;
  }

  public static int createTuple(int first, int second) {
    int maxValue = Short.MAX_VALUE;
    Preconditions.checkArgument(
        first < maxValue, "Value: " + first + " is to big for BitmaskTuple implementation");
    Preconditions.checkArgument(
        second < maxValue, "Value: " + second + " is to big for BitmaskTuple implementation");
    return first + (second << 16);
  }
}
