// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.math.BigInteger;
import java.util.List;
import org.sosy_lab.pjbdd.bdd.BDDCreator;
import org.sosy_lab.pjbdd.util.IfThenElseData;

/**
 * Main {@link DD} boolean_operations interface defining the following {@link DD} operations.
 *
 * <ul>
 *   <li>{@link #makeVariable()}
 *   <li>{@link #restrict(DD, int, boolean)}
 *   <li>{@link #anySat(DD)}
 *   <li>{@link #satCount(DD)}
 *   <li>{@link #makeTrue()}
 *   <li>{@link #makeFalse()}
 *   <li>{@link #makeNot(DD)}
 *   <li>{@link #makeAnd(DD, DD)}
 *   <li>{@link #makeOr(DD, DD)}
 *   <li>{@link #makeXor(DD, DD)}
 *   <li>{@link #makeNor(DD, DD)}
 *   <li>{@link #makeXnor(DD, DD)}
 *   <li>{@link #makeNand(DD, DD)}
 *   <li>{@link #makeEqual(DD, DD)}
 *   <li>{@link #makeUnequal(DD, DD)}
 *   <li>{@link #makeIte(DD, DD, DD)}
 *   <li>{@link #makeImply(DD, DD)}
 *   <li>{@link #makeExists(DD, DD...)}
 *   <li>{@link #entails(DD, DD)}
 *   <li>{@link #makeNode(DD, DD, int)}
 * </ul>
 *
 * <p>Implementations are:
 *
 * <ul>
 *   <li>{@link BDDCreator}
 * </ul>
 *
 * @author Stephan Holzner
 * @see BDDCreator
 * @since 1.0
 */
public interface Creator {

  String VERSION = "1.2";

  /**
   * Creates a new variable and returns the predicate representing it.
   *
   * @return predicate representing the new variable
   */
  DD makeVariable();

  /**
   * Creates a new variable and inserts it before a given variable in the existing ordering.
   *
   * @param var - the successor variable.
   * @return new variable inserted before var
   */
  DD makeVariableBefore(DD var);

  /**
   * Get the BDD representation of variable i.
   *
   * @param i - the ith variable tobe returned
   * @return the BDD representation of variable i
   */
  default DD makeIthVar(int i) {
    return makeNode(makeFalse(), makeTrue(), i);
  }

  /**
   * Recursive restricts a nodes's specific variable.
   *
   * @param bdd - node to be restricted
   * @param var - variable to be restricted
   * @param restrictionVar - restricted to true or false branch
   * @return restricted bdd
   */
  DD restrict(DD bdd, int var, boolean restrictionVar);

  /**
   * Takes a {@link DD} and finds any satisfying variable assignment.
   *
   * @param f - the {@link DD} node
   * @return one satisfying variable assignment.
   */
  DD anySat(DD f);

  /**
   * Get {@link DD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param f - the {@link DD} node
   * @return the number of satisfying variable assignments.
   */
  BigInteger satCount(DD f);

  /**
   * Get a representation of logical truth.
   *
   * @return a representation of logical truth
   */
  DD makeTrue();

  /**
   * Get a representation of logical falseness.
   *
   * @return a representation of logical falseness
   */
  DD makeFalse();

  /**
   * Creates a {@link DD} representing a negation of the argument.
   *
   * @param f - the {@link DD} argument
   * @return (not f1)
   */
  DD makeNot(DD f);

  /**
   * Creates a {@link DD} representing an AND of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 and f2)
   */
  DD makeAnd(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing an OR of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 or f2)
   */
  DD makeOr(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing a Xor of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 xor f2)
   */
  DD makeXor(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing a Nor of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 nor f2)
   */
  DD makeNor(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing a Xnor of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 xnor f2)
   */
  DD makeXnor(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing a Nand of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 nand f2)
   */
  DD makeNand(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing an equality (bi-implication) of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 equal f2)
   */
  DD makeEqual(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing an inequality (XOR) of the two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 inequal f2)
   */
  DD makeUnequal(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing an if then else construct of the three arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param f3 - getElse {@link DD} argument
   * @return (if f1 then f2 else f3)
   */
  DD makeIte(DD f1, DD f2, DD f3);

  DD makeIte(IfThenElseData iteData);

  /**
   * Creates a {@link DD} representing an existential quantification of the getThen argument. If
   * there are more arguments, each of them is quantified.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - one or more {@link DD}
   * @return (exists f2... : f1)
   */
  DD makeExists(DD f1, DD[] f2);

  /**
   * Creates a {@link DD} representing an existential quantification of the getThen argument. If
   * there are more arguments, each of them is quantified.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - one {@link DD}
   * @return (exists f2 : f1)
   */
  DD makeExists(DD f1, DD f2);

  /**
   * Creates a {@link DD} representing a modification of the #f1 argument. All #oldVar occurrences
   * are replaced by #replaceVar without changing variable ordering.
   *
   * @param f1 - {@link DD} argument
   * @param oldVar - the variable to be replaced
   * @param replaceVar - the variable replacement
   * @return replace(f1, oldVar, replaceVar)
   */
  DD makeReplace(DD f1, DD oldVar, DD replaceVar);

  /**
   * Creates a {@link DD} representing a modification of the #f1 argument. Substitute #var
   * withfunction #f2
   *
   * @param f1 - {@link DD} argument
   * @param var - variable to be substituted
   * @param f2 - function substitute
   * @return compose(f1,var,f2)
   */
  DD makeCompose(DD f1, int var, DD f2);

  /**
   * Creates a {@link DD} representing an logical implication of the getThen argument.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (f1 imply f2)
   */
  DD makeImply(DD f1, DD f2);

  /**
   * checks whether the data {@link DD} represented by f1 is a subset of that represented by f2.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return true if (f1 entails f2), false otherwise
   */
  default boolean entails(DD f1, DD f2) {
    return makeImply(f1, f2).isTrue();
  }

  /**
   * A {@link DD} consists of the form if (predicate) then formula1 else formula2. This method
   * decomposes a {@link DD} into these three parts.
   *
   * @param f - a {@link DD}
   * @return a triple with the condition predicate and the formulas for the true branch and the else
   *     branch
   */
  IfThenElseData getIfThenElse(DD f);

  /**
   * Sets the bdd variable ordering and updates all to the new ordering.
   *
   * @param pOrder - the new order of the variables.
   */
  void setVarOrder(List<Integer> pOrder);

  /**
   * Returns existing {@link DD} node with given low, high branches and variable. Creates a new one
   * if there is no existing.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the bdd variable
   * @return new or existing bdd
   */
  DD makeNode(DD low, DD high, int var);

  /**
   * Get {@link DD} node's low branch child node.
   *
   * @param top - the node
   * @return the low branch child node
   */
  DD getLow(DD top);

  /**
   * Get {@link DD} node's high branch child node.
   *
   * @param top - the node
   * @return the high branch child node
   */
  DD getHigh(DD top);

  /** Shutdown boolean_operations and cleanup resources. */
  void shutDown();

  /**
   * Get number of existing variables.
   *
   * @return number of existing variables
   */
  int getVariableCount();

  /**
   * Get all existing variables in current ordering.
   *
   * @return all existing variables in current ordering
   */
  int[] getVariableOrdering();

  /**
   * Set number of existing variables.
   *
   * @param count - the new variable count
   */
  void setVariableCount(int count);

  /**
   * Get current statistics as {@link Stats} object.
   *
   * @return current statistics as {@link Stats} object
   */
  Stats getCreatorStats();

  /** Manually trigger unique table garbage collection of unused nodes. */
  void cleanUnusedNodes();

  /**
   * Get current framework version.
   *
   * @return current framework version
   */
  default String getVersion() {
    return VERSION;
  }

  /**
   * Stats interface defining getter methods for all all required parameters.
   *
   * @author Stephan Holzner
   * @since 1.0
   */
  interface Stats {

    /**
     * String format the objects properties.
     *
     * @return String formatted objects properties
     */
    String prettyPrint();

    /**
     * Get number of allocated nodes.
     *
     * @return number of allocated nodes
     */
    int getNodeCount();

    /**
     * Get size of unique table.
     *
     * @return size of unique table
     */
    int getUniqueTableSize();

    /**
     * Get number of created variables.
     *
     * @return number of created variables
     */
    int getVariableCount();

    /**
     * Get number of garbage collected elements.
     *
     * @return number of garbage collected elements
     */
    BigInteger getGarbageCollections();

    /**
     * Get number of elements in cache.
     *
     * @return number of cached elements.
     */
    int getCacheNodeCount();

    /**
     * Get cache hit count.
     *
     * @return number cache hits.
     */
    BigInteger getCacheHits();

    /**
     * Get number of performed resizes.
     *
     * @return number of performed resizes.
     */
    BigInteger resizeOperations();

    /**
     * Gt cumulated time spent for table resizes.
     *
     * @return cumulated time spent for table resizes.
     */
    BigInteger resizeTime();

    /**
     * Get cumulated time spent for gc.
     *
     * @return cumulated time spent for gc.
     */
    BigInteger gcTime();

    /**
     * Get cumulated time spent for bdd operations.
     *
     * @return cumulated time spent for bdd operations.
     */
    BigInteger computationTime();
  }
}
