// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.bdd.BDDCreator;
import org.sosy_lab.pjbdd.bdd.BDDReductionRule;
import org.sosy_lab.pjbdd.bdd.BDDSat;
import org.sosy_lab.pjbdd.bdd.algorithm.ApplyBDDAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.CompletableFutureApplyAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.CompletableFutureITEAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.ForkJoinApplyAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.ForkJoinITEAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.FutureITEAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.GuavaFutureITEAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.ITEBDDAlgorithm;
import org.sosy_lab.pjbdd.bdd.algorithm.StreamITEAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.ArrayCache;
import org.sosy_lab.pjbdd.core.cache.CASArrayCache;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.cache.GuavaCache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.core.node.NodeManagerImpl;
import org.sosy_lab.pjbdd.core.uniquetable.DDCASArrayUniqueTable;
import org.sosy_lab.pjbdd.core.uniquetable.DDConcurrentWeakHashDeque;
import org.sosy_lab.pjbdd.core.uniquetable.DDConcurrentWeakHashMap;
import org.sosy_lab.pjbdd.core.uniquetable.UniqueTable;
import org.sosy_lab.pjbdd.core.uniquetable.WeakArrayUniqueTable;

/**
 * Builder class to create {@link Creator} environments implemented with {@link BDDCreator}.
 *
 * @author Stephan Holzner
 * @see Creator
 * @see BDDCreator
 * @since 1.0
 */
public class BDDCreatorBuilder extends AbstractCreatorBuilder {

  private final DD.Factory<DD> ddFactory;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public BDDCreatorBuilder(DD.Factory<DD> factory) {
    this.ddFactory = factory;
  }

  /**
   * Creates a new BDDCreator environment with previously set parameters.
   *
   * @return a new BDD environment
   */
  @Override
  public Creator build() {
    initParallelismManagerIfNeeded();

    NodeManager<DD> nodeManager = new NodeManagerImpl<>(makeTable(), new BDDReductionRule<>());
    nodeManager.setVarCount(this.selectedVarCount);
    Cache<Integer, Cache.CacheData> cache;
    if (useThreadSafeUT) {
      cache = new CASArrayCache<>();
    } else {
      cache = new ArrayCache<>();
    }
    Cache<DD, BigInteger> satCache = new GuavaCache<>();
    cache.init(this.selectedCacheSize, this.selectedParallelism);
    cache.init(this.selectedCacheSize, this.selectedParallelism);
    satCache.init(this.selectedCacheSize, this.selectedParallelism);

    DDAlgorithm<DD> algorithm = null;
    SatAlgorithm<DD> satAlgorithm = new BDDSat<>(satCache, nodeManager);

    if (useApply) {
      if (!useThreadSafeUT) {
        algorithm = new ApplyBDDAlgorithm<>(cache, nodeManager);
        return new BDDCreator(nodeManager, algorithm, satAlgorithm);
      }
      switch (parallelizationType) {
        case COMPLETABLE_FUTURE:
          algorithm = new CompletableFutureApplyAlgorithm<>(cache, nodeManager, parallelismManager);
          break;
        case FORK_JOIN:
          algorithm = new ForkJoinApplyAlgorithm<>(cache, nodeManager, parallelismManager);
          break;
        default:
          algorithm = new ApplyBDDAlgorithm<>(cache, nodeManager);
          break;
      }
    } else {
      if (!useThreadSafeUT) {
        algorithm = new ITEBDDAlgorithm<>(cache, nodeManager);
        return new BDDCreator(nodeManager, algorithm, satAlgorithm);
      }
      switch (parallelizationType) {
        case NONE:
          algorithm = new ITEBDDAlgorithm<>(cache, nodeManager);
          break;
        case FUTURE:
          algorithm = new FutureITEAlgorithm<>(cache, nodeManager, parallelismManager);
          break;
        case STREAM:
          algorithm = new StreamITEAlgorithm<>(cache, nodeManager, parallelismManager);
          break;
        case FORK_JOIN:
          algorithm = new ForkJoinITEAlgorithm<>(cache, nodeManager, parallelismManager);
          break;
        case GUAVA_FUTURE:
          algorithm = new GuavaFutureITEAlgorithm<>(cache, nodeManager, parallelismManager);
          break;
        case COMPLETABLE_FUTURE:
          algorithm = new CompletableFutureITEAlgorithm<>(cache, nodeManager, parallelismManager);
          break;
      }
    }
    Creator creator = new BDDCreator(nodeManager, algorithm, satAlgorithm);
    if (synchronizeReorderingOperations) {
      creator = new SynchronizedReorderingCreator(creator);
    }
    return creator;
  }

  protected UniqueTable<DD> makeTable() {
    switch (tableType) {
      case ConcurrentHashBucket:
        return useThreadSafeUT
            ? new DDConcurrentWeakHashDeque<>(selectedTableSize, selectedParallelism, ddFactory)
            : new WeakArrayUniqueTable<>(selectedIncreaseFactor, selectedTableSize, ddFactory);
      case CASArray:
        return useThreadSafeUT
            ? new DDCASArrayUniqueTable<>(selectedIncreaseFactor, selectedTableSize, ddFactory)
            : new WeakArrayUniqueTable<>(selectedIncreaseFactor, selectedTableSize, ddFactory);
      default:
        return useThreadSafeUT
            ? new DDConcurrentWeakHashMap<>(selectedTableSize, selectedParallelism, ddFactory)
            : new WeakArrayUniqueTable<>(selectedIncreaseFactor, selectedTableSize, ddFactory);
    }
  }
}
