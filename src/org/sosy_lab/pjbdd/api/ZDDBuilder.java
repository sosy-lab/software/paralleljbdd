// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.core.cache.CASArrayCache;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.cache.GuavaCache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.core.node.NodeManagerImpl;
import org.sosy_lab.pjbdd.core.uniquetable.DDCASArrayUniqueTable;
import org.sosy_lab.pjbdd.core.uniquetable.DDConcurrentWeakHashDeque;
import org.sosy_lab.pjbdd.core.uniquetable.DDConcurrentWeakHashMap;
import org.sosy_lab.pjbdd.core.uniquetable.UniqueTable;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;
import org.sosy_lab.pjbdd.zdd.ZDDAlgorithm;
import org.sosy_lab.pjbdd.zdd.ZDDConcurrentAlgorithm;
import org.sosy_lab.pjbdd.zdd.ZDDCreatorImpl;
import org.sosy_lab.pjbdd.zdd.ZDDReductionRule;
import org.sosy_lab.pjbdd.zdd.ZDDSat;
import org.sosy_lab.pjbdd.zdd.ZDDSerialAlgorithm;

/**
 * Builder class to create {@link ZDDCreator} environments implemented with {@link ZDDCreatorImpl}.
 *
 * @author Stephan Holzner
 * @see ZDDCreator
 * @see ZDDCreatorImpl
 * @since 1.0
 */
public class ZDDBuilder {

  private final DD.Factory<DD> factory;

  protected int selectedParallelism = 1000;

  /** indicates the initial selectedTable size default value = 500000. */
  protected int selectedTableSize = 500000;

  /** indicates the fixed computation cache size default value = 10000. */
  protected int selectedCacheSize = 10000;

  /**
   * indicates the fixed worker thread count default value =
   * Runtime.getRuntime().availableProcessors().
   */
  protected int selectedThreads = Runtime.getRuntime().availableProcessors();

  /** indicates the initial created variable count default value = 10. */
  protected int selectedVarCount = 10;

  /** indicates the selectedTable's fixed increase factor default value = 1. */
  protected int selectedIncreaseFactor = 1;

  /** selected parallelism manager instance. */
  protected ParallelismManager parallelismManager = new ParallelismManagerImpl();

  /** selected table type. */
  protected Builders.TableType tableType = Builders.TableType.CASArray;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public ZDDBuilder(DD.Factory<DD> factory) {
    this.factory = factory;
  }

  public ZDDBuilder setSelectedParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  public ZDDBuilder setSelectedCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  public ZDDBuilder setTableType(Builders.TableType type) {
    this.tableType = type;
    return this;
  }

  public ZDDBuilder setParallelismManager(ParallelismManager manager) {
    this.parallelismManager = manager;
    return this;
  }

  public ZDDBuilder setParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  public ZDDBuilder setTableSize(int tableSize) {
    this.selectedTableSize = tableSize;
    return this;
  }

  public ZDDBuilder setCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  public ZDDBuilder setThreads(int threads) {
    if (threads != this.selectedThreads) {
      parallelismManager = new ParallelismManagerImpl(threads);
    }
    this.selectedThreads = threads;
    return this;
  }

  public ZDDBuilder setVarCount(int varCount) {
    this.selectedVarCount = varCount;
    return this;
  }

  public ZDDBuilder setIncreaseFactor(int increaseFactor) {
    this.selectedIncreaseFactor = increaseFactor;
    return this;
  }

  /**
   * Creates a new ZDDCreator environment with previously set parameters.
   *
   * @return a new ZDD environment.
   */
  public ZDDCreator build() {
    NodeManager<DD> nodeManager = new NodeManagerImpl<>(makeTable(), new ZDDReductionRule<>());
    nodeManager.setVarCount(this.selectedVarCount);

    Cache<Integer, Cache.CacheData> cache = new CASArrayCache<>();
    cache.init(selectedCacheSize, selectedParallelism);
    ZDDAlgorithm<DD> algorithm =
        this.selectedThreads > 1
            ? new ZDDConcurrentAlgorithm<>(cache, nodeManager, parallelismManager)
            : new ZDDSerialAlgorithm<>(cache, nodeManager);
    Cache<DD, BigInteger> satCountCache = new GuavaCache<>();
    satCountCache.init(selectedCacheSize, selectedParallelism);

    return new ZDDCreatorImpl(new ZDDSat<>(satCountCache, nodeManager), nodeManager, algorithm);
  }

  protected UniqueTable<DD> makeTable() {
    if (tableType == Builders.TableType.ConcurrentHashBucket) {
      return new DDConcurrentWeakHashDeque<>(selectedTableSize, selectedParallelism, factory);
    }
    if (tableType == Builders.TableType.ConcurrentHashMap) {
      return new DDConcurrentWeakHashMap<>(selectedTableSize, selectedParallelism, factory);
    }

    return new DDCASArrayUniqueTable<>(selectedIncreaseFactor, selectedTableSize, factory);
  }
}
