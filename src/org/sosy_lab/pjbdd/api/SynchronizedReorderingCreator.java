// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.sosy_lab.pjbdd.util.IfThenElseData;

/**
 * {@link Creator} implementation with {@link DD} objects as bdd data structure.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public class SynchronizedReorderingCreator implements Creator {

  private final Creator creator;

  /** lock object for reorder operations: read lock for normal operations and write for reorder. */
  protected final ReadWriteLock reorderLock;

  /**
   * Creates new SynchronizedReorderingBDDCreator environment.
   *
   * @param creator - the wrapped creator
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public SynchronizedReorderingCreator(Creator creator) {
    this.creator = creator;
    reorderLock = new ReentrantReadWriteLock();
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNot(DD f) {
    reorderLock.readLock().lock();
    try {
      return creator.makeNot(f);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeAnd(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeAnd(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeOr(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeOr(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeXor(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeXor(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNor(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeNor(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeXnor(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeXnor(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNand(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeNand(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeEqual(DD f1, DD f2) {
    return makeXnor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeUnequal(DD f1, DD f2) {
    return makeXor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeImply(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeImply(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD anySat(DD bdd) {
    reorderLock.readLock().lock();
    try {
      return creator.anySat(bdd);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(DD b) {
    reorderLock.readLock().lock();
    try {
      return creator.satCount(b);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public Stats getCreatorStats() {
    return creator.getCreatorStats();
  }

  @Override
  public void cleanUnusedNodes() {
    reorderLock.writeLock().lock();
    try {
      creator.cleanUnusedNodes();
    } finally {
      reorderLock.writeLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public IfThenElseData getIfThenElse(DD f) {
    return creator.getIfThenElse(f);
  }

  @Override
  public DD makeNode(DD low, DD high, int var) {
    return creator.makeNode(low, high, var);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeExists(DD f1, DD[] f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeExists(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeExists(DD f1, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeExists(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  @Override
  public DD makeCompose(DD f1, int var, DD f2) {
    reorderLock.readLock().lock();
    try {
      return creator.makeCompose(f1, var, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  @Override
  public DD makeReplace(DD f1, DD oldVar, DD replaceVar) {
    reorderLock.readLock().lock();
    try {
      return creator.makeReplace(f1, oldVar, replaceVar);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD getLow(DD top) {
    return creator.getLow(top);
  }

  /** {@inheritDoc} */
  @Override
  public DD getHigh(DD top) {
    return creator.getHigh(top);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeTrue() {
    return creator.makeTrue();
  }

  /** {@inheritDoc} */
  @Override
  public DD makeFalse() {
    return creator.makeFalse();
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(DD f1, DD f2, DD f3) {
    reorderLock.readLock().lock();
    try {
      return creator.makeIte(f1, f2, f3);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(IfThenElseData iteData) {
    return makeIte(iteData.getIf(), iteData.getThen(), iteData.getElse());
  }

  /** {@inheritDoc} */
  @Override
  public DD makeVariable() {
    return creator.makeVariable();
  }

  @Override
  public DD makeVariableBefore(DD var) {
    return creator.makeVariableBefore(var);
  }

  /** {@inheritDoc} */
  @Override
  public DD restrict(DD bdd, int var, boolean restrictionVar) {
    return creator.makeVariable();
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }

  /**
   * set new variable order.
   *
   * @param pOrder - the new order
   */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    reorderLock.writeLock().lock();
    try {
      creator.setVarOrder(pOrder);
    } finally {
      reorderLock.writeLock().unlock();
    }
  }

  /**
   * get current variable count.
   *
   * @return current variable count
   */
  @Override
  public int getVariableCount() {
    return creator.getVariableCount();
  }

  /**
   * get current variable order.
   *
   * @return current variable order.
   */
  @Override
  public int[] getVariableOrdering() {
    return creator.getVariableOrdering();
  }

  /**
   * set new variable count.
   *
   * @param count - the new count.
   */
  @Override
  public void setVariableCount(int count) {
    reorderLock.readLock().lock();
    try {
      creator.setVariableCount(count);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    creator.shutDown();
  }
}
