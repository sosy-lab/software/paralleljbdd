// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;

/**
 * Builder base class for creator environment instantiations.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public abstract class AbstractCreatorBuilder implements CreatorBuilder {

  /** indicates the number of concurrent accesses to the selectedTable default value = 1000. */
  protected int selectedParallelism = 1000;

  /** indicates the initial selectedTable size default value = 500000. */
  protected int selectedTableSize = 500000;

  /** indicates the fixed computation cache size default value = 10000. */
  protected int selectedCacheSize = 10000;

  /**
   * indicates the fixed worker thread count default value =
   * Runtime.getRuntime().availableProcessors().
   */
  protected int selectedThreads = Runtime.getRuntime().availableProcessors();

  /** indicates the initial created variable count default value = 10. */
  protected int selectedVarCount = 10;

  /** indicates the selectedTable's fixed increase factor default value = 1. */
  protected int selectedIncreaseFactor = 1;

  /** selected parallelism manager instance. */
  protected ParallelismManager parallelismManager;

  /** selected parallelization type. */
  protected Builders.ParallelizationType parallelizationType =
      Builders.ParallelizationType.FORK_JOIN;

  /** use apply or ite algorithm. */
  protected boolean useApply = true;

  /** selected table type. */
  protected Builders.TableType tableType = Builders.TableType.CASArray;

  protected boolean useThreadSafeUT = true;

  protected boolean synchronizeReorderingOperations = false;

  @Override
  public CreatorBuilder synchronizeReorderingOperations() {
    synchronizeReorderingOperations = true;
    return this;
  }

  @Override
  public CreatorBuilder disableThreadSafety() {
    useThreadSafeUT = false;
    return this;
  }

  @Override
  public CreatorBuilder setUseApply(boolean apply) {
    this.useApply = apply;
    return this;
  }

  @Override
  public CreatorBuilder setTableType(Builders.TableType type) {
    this.tableType = type;
    return this;
  }

  @Override
  public CreatorBuilder setParallelismManager(ParallelismManager manager) {
    this.parallelismManager = manager;
    return this;
  }

  @Override
  public CreatorBuilder setParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  @Override
  public CreatorBuilder setTableSize(int tableSize) {
    this.selectedTableSize = tableSize;
    return this;
  }

  @Override
  public CreatorBuilder setCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  @Override
  public CreatorBuilder setThreads(int threads) {
    this.selectedThreads = threads;
    return this;
  }

  @Override
  public CreatorBuilder setVarCount(int varCount) {
    this.selectedVarCount = varCount;
    return this;
  }

  @Override
  public CreatorBuilder setIncreaseFactor(int increaseFactor) {
    this.selectedIncreaseFactor = increaseFactor;
    return this;
  }

  @Override
  public CreatorBuilder setParallelizationType(Builders.ParallelizationType type) {
    this.parallelizationType = type;
    return this;
  }

  protected void initParallelismManagerIfNeeded() {
    if (!useThreadSafeUT || selectedThreads <= 1) {
      return;
    }
    if (parallelizationType == Builders.ParallelizationType.NONE) {
      return;
    }
    if (parallelismManager == null) {
      parallelismManager = new ParallelismManagerImpl(selectedThreads);
    }
  }
}
