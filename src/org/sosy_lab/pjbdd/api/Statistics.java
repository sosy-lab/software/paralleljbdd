// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.util.HashSet;
import java.util.Set;

/** Statistics API. */
public final class Statistics {

  /**
   * Creates statistics for a given bdd.
   *
   * @param dd - the given BDD.
   * @return Statistics for dd
   */
  public static DDStats of(DD dd) {
    return new DDStats(dd);
  }

  /**
   * Creates statistics for a given Creator environment.
   *
   * @param creator - the given Environment.
   * @return Statistics for creator
   */
  public static Creator.Stats of(Creator creator) {
    return creator.getCreatorStats();
  }

  /** Empty constructor to avoid instantiation. */
  private Statistics() {}

  /** Statistics data class for dds. */
  public static final class DDStats {

    /** Nodes in DD tree. */
    private int nodeCount;

    /** Edges in DD tree. */
    private int edgeCount;

    private final Set<DD> visited;

    /**
     * private Constructor to avoid instantiation.
     *
     * @param pDD - DD to collect statistics for.
     */
    private DDStats(DD pDD) {
      visited = new HashSet<>();
      computeStats(pDD);
    }

    /**
     * Helper method to compute a nodes statistics.
     *
     * @param dd - the node
     */
    private void computeStats(DD dd) {
      if (visited.contains(dd)) {
        return;
      }
      visited.add(dd);
      nodeCount++;
      if (dd.isLeaf()) {
        return;
      }
      edgeCount += 2;
      computeStats(dd.getHigh());
      computeStats(dd.getLow());
    }

    /**
     * Get number of edges in the given DD.
     *
     * @return edgeCount
     */
    public int getEdgeCount() {
      return edgeCount;
    }

    /**
     * Get number of nodes in the given DD.
     *
     * @return nodeCount
     */
    public int getNodeCount() {
      return nodeCount;
    }
  }
}
