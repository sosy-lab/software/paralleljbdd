// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>, Sebastian Niedner
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.cbdd;

import com.google.common.base.Preconditions;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.algorithm.ApplyBDDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.BitMaskIntTupleUtils;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Chained Version of { @link ApplyBDDAlgorithm }.
 *
 * @author Sebastian Niedner
 * @see ApplyBDDAlgorithm
 * @since ?.?
 */
public class CBDDApplyAlgorithm extends ApplyBDDAlgorithm<DD> {

  public CBDDApplyAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager<DD> nodeManager) {
    super(computedTable, nodeManager);
  }

  @Override
  public DD makeNode(DD low, DD high, int var) {
    if (!nodeManager.checkLvl(var)) {
      nodeManager.setVarCount(var + 1);
    }
    return makeChainNode(low, high, var, var);
  }

  /**
   * Serial calculation for applying an operation on two input arguments, with an given terminal
   * function.
   *
   * @param f1 - getIf {@link CDD} argument
   * @param f2 - getThen {@link CDD} argument
   * @param op - the operation to be applied
   * @return (f1 op f2)
   */
  @Override
  public DD makeOp(DD f1, DD f2, ApplyOp op) {
    return terminalCheck(f1, f2, op).orElseGet(() -> makeOpElse(f1, f2, op));
  }

  private DD makeOpElse(DD f1, DD f2, ApplyOp op) {
    int t = calculateTLevel(f1, f2);
    int b = calculateBLevel(f1, f2, t);

    DD f1Low = cofactorLow(f1, b);
    DD f2Low = cofactorLow(f2, b);
    DD f1High = cofactorHigh(f1, b);
    DD f2High = cofactorHigh(f2, b);

    DD low = makeOp(f1Low, f2Low, op);
    DD high = makeOp(f1High, f2High, op);

    DD res = combine(low, high, t, b);

    this.cacheBinaryItem(f1, f2, op.ordinal(), res);
    return res;
  }

  protected DD cofactorLow(DD u, int b) {
    int tF = level(u);
    int bF = chainEndlevel(u);

    if (b < tF) {
      return u;
    } else if (b == bF) {
      return nodeManager.getLow(u);
    } else if (tF <= b && b < bF) {
      return makeChainNode(nodeManager.getLow(u), nodeManager.getHigh(u), b + 1, bF);
      // this.cacheBinaryItem(v.getLow(), v.getHigh(), op.ordinal(), newNode);
    } else {
      throw new RuntimeException("Ran into unknown combination of Levels.");
    }
  }

  protected DD cofactorHigh(DD u, int b) {
    int tF = level(u);
    if (b < tF) {
      return u;
    } else {
      return nodeManager.getHigh(u);
    }
  }

  protected int calculateTLevel(DD f1, DD f2) {
    return topVar(level(f1), level(f2));
  }

  protected int calculateTLevel(DD f1, DD f2, DD f3) {
    return topVar(level(f1), level(f2), level(f3));
  }

  protected int calculateBLevel(DD f1, DD f2, int t) {
    return topVar(singleBLevel(f1, t), singleBLevel(f2, t));
  }

  protected int calculateBLevel(DD f1, DD f2, DD f3, int t) {
    return topVar(singleBLevel(f1, t), singleBLevel(f2, t), singleBLevel(f3, t));
  }

  protected DD combine(DD low, DD high, int t, int b) {
    DD res;

    if (low.equals(high)) {
      res = low;
    } else if (level(low) == b + 1 && low.getHigh().equals(high)) {
      int lowChainEndVar = castToChained(low).getChainEndVariable();
      res = makeChainNode(nodeManager.getLow(low), high, t, lowChainEndVar);
    } else {
      res = makeChainNode(low, high, t, b);
    }

    return res;
  }

  @Override
  public DD makeNot(DD f) {
    return makeNotRecursive(f);
  }

  protected DD makeNotRecursive(DD f) {
    return terminalNotCheck(f)
        .orElseGet(
            () -> {
              DD bdd =
                  makeChainNode(
                      makeNotRecursive(nodeManager.getLow(f)),
                      makeNotRecursive(nodeManager.getHigh(f)),
                      f.getVariable(),
                      castToChained(f).getChainEndVariable());
              this.cacheUnaryItem(f, bdd);
              return bdd;
            });
  }

  public DD makeChainNode(DD low, DD high, int var, int chainEndVar) {
    return nodeManager.makeNode(low, high, BitMaskIntTupleUtils.createTuple(var, chainEndVar));
  }

  /**
   * Get a bdd node's chain end variable level.
   *
   * @param bdd - a bdd node
   * @return nodes's variable level
   */
  protected int chainEndlevel(DD bdd) {
    return nodeManager.level(castToChained(bdd).getChainEndVariable());
  }

  /**
   * Calculate b level of a CBDD.
   *
   * @param bdd - a bdd node
   * @param t - topVar to use
   * @return nodes's variable level
   */
  protected int singleBLevel(DD bdd, int t) {
    int ti = level(bdd);

    if (ti == t) {
      return chainEndlevel(bdd);
    } else if (ti == nodeManager.getVarCount() + 1) {
      return ti;
    } else {
      return ti - 1;
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeCompose(DD f1, int var, DD f2) {
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), var, f2.hashCode());
    @SuppressWarnings("unchecked")
    Cache.CacheDataBinaryOp<DD> data = (Cache.CacheDataBinaryOp<DD>) composeCache.get(hash);
    if (data != null && data.matches(f1, f2, var)) {
      return data.getRes();
    }

    int f1Level = level(f1);
    int varLevel = nodeManager.level(var);

    if (f1Level > varLevel) {
      return f1;
    }

    DD res;
    int f1ChainEndLevel = chainEndlevel(f1);
    boolean isChained = f1ChainEndLevel - f1Level > 0;
    boolean varInChain = f1ChainEndLevel >= varLevel;

    if (isChained && varInChain) {
      DD rupturedU = rupture(f1, var);
      res = makeCompose(rupturedU, var, f2);
    } else if (f1.getVariable() == var) {
      res = makeIte(f2, nodeManager.getHigh(f1), nodeManager.getLow(f1));
    } else {
      DD i = makeCompose(nodeManager.getHigh(f1), var, f2);
      DD e = makeCompose(nodeManager.getLow(f1), var, f2);
      res = makeIte(nodeManager.makeIthVar(f1.getVariable()), i, e);
    }

    data = new Cache.CacheDataBinaryOp<>();
    data.setRes(res);
    data.setOp(var);
    data.setF1(f1);
    data.setF2(f2);
    composeCache.put(hash, data);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(DD ifBDD, DD thenBDD, DD elseBDD) {
    return terminalIteCheck(ifBDD, thenBDD, elseBDD)
        .orElseGet(() -> makeIteElse(ifBDD, thenBDD, elseBDD));
  }

  protected DD makeIteElse(DD ifBDD, DD thenBDD, DD elseBDD) {
    int t = calculateTLevel(ifBDD, thenBDD, elseBDD);
    int b = calculateBLevel(ifBDD, thenBDD, elseBDD, t);

    DD ifLow = cofactorLow(ifBDD, b);
    DD thenLow = cofactorLow(thenBDD, b);
    DD elseLow = cofactorLow(elseBDD, b);
    DD ifHigh = cofactorHigh(ifBDD, b);
    DD thenHigh = cofactorHigh(thenBDD, b);
    DD elseHigh = cofactorHigh(elseBDD, b);

    DD low = makeIte(ifLow, thenLow, elseLow);
    DD high = makeIte(ifHigh, thenHigh, elseHigh);

    DD res = combine(low, high, t, b);

    cacheItem(ifBDD, thenBDD, elseBDD, res);
    return res;
  }

  public DD rupture(DD u, int var) {
    int uLevel = nodeManager.level(u.getVariable());
    int uChainEndLevel = nodeManager.level(castToChained(u).getChainEndVariable());
    int vLevel = nodeManager.level(var);

    if (vLevel < uLevel || vLevel > uChainEndLevel || uLevel == uChainEndLevel) {
      return u;
    }

    if (uLevel == vLevel) {
      // Variable to rupture is at the beginning of the or-chain
      DD hiNode =
          makeChainNode(
              nodeManager.getLow(u),
              nodeManager.getHigh(u),
              nodeManager.var(uLevel + 1),
              nodeManager.var(uChainEndLevel));

      return makeChainNode(hiNode, nodeManager.getHigh(u), u.getVariable(), u.getVariable());

    } else if (uChainEndLevel == vLevel) {
      // Variable to rupture is at the end of the or-chain
      DD hiNode =
          makeChainNode(
              nodeManager.getLow(u),
              nodeManager.getHigh(u),
              castToChained(u).getChainEndVariable(),
              castToChained(u).getChainEndVariable());

      return makeChainNode(
          hiNode, nodeManager.getHigh(u), u.getVariable(), nodeManager.var(uChainEndLevel - 1));
    } else {
      // Variable to rupture inbetween the beginning and the end of the or-chain
      DD hiHiNode =
          makeChainNode(
              nodeManager.getLow(u),
              nodeManager.getHigh(u),
              nodeManager.var(vLevel + 1),
              castToChained(u).getChainEndVariable());

      DD hiNode = makeChainNode(hiHiNode, nodeManager.getHigh(u), var, var);

      return makeChainNode(
          hiNode, nodeManager.getHigh(u), u.getVariable(), nodeManager.var(vLevel - 1));
    }
  }

  private CDD castToChained(DD bdd) {
    Preconditions.checkArgument(bdd instanceof CDD);
    return (CDD) bdd;
  }
}
