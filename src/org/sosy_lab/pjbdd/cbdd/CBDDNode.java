// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>, Sebastian Niedner
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.cbdd;

import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.BDDNode;
import org.sosy_lab.pjbdd.util.BitMaskIntTupleUtils;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Chained Version of the {@link BDDNode}.
 *
 * @see BDDNode
 * @since ?.?
 */
public class CBDDNode implements CDD {
  /** a cbdd nodes variable and chain end variable. */
  private final int variableChain;

  private final CBDDNode low;

  private final CBDDNode high;

  private int hashCode;

  public CBDDNode(CBDDNode low, CBDDNode high, int var) {
    this.high = high;
    this.low = low;
    this.variableChain = var;
    this.rehash();
  }

  /** {@inheritDoc} */
  @Override
  public int getChainEndVariable() {
    return BitMaskIntTupleUtils.getSecond(variableChain);
  }

  @Override
  public CBDDNode getHigh() {
    if (isLeaf()) {
      return this;
    }
    return high;
  }

  @Override
  public boolean isTrue() {
    return variableChain == -1;
  }

  @Override
  public boolean isFalse() {
    return variableChain == -2;
  }

  @Override
  public int getVariable() {
    return variableChain < 0 ? variableChain : BitMaskIntTupleUtils.getFirst(variableChain);
  }

  @Override
  public CBDDNode getLow() {
    if (isLeaf()) {
      return this;
    }
    return low;
  }

  /** Recalculate nodes hashcode after changes due to node recycling or bdd reordering. */
  protected void rehash() {
    hashCode =
        (variableChain < 0)
            ? Math.abs(getVariable())
            : HashCodeGenerator.generateHashCode(
                getVariable(), getChainEndVariable(), getLow().hashCode(), getHigh().hashCode());
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object o) {
    if (!(o instanceof CDD)) {
      return false;
    }
    CDD other = (CDD) o;
    if (other.getChainEndVariable() != getChainEndVariable()) {
      return false;
    }
    if (other.getVariable() != getVariable()) {
      return false;
    }
    return other.getHigh() == getHigh() && other.getLow() == getLow();
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equalsTo(int compVariableChain, DD compLow, DD compHigh) {
    if (compVariableChain != variableChain) {
      return false;
    }
    return compHigh == getHigh() && compLow == getLow();
  }

  /**
   * Main BDD.Factory implementation used for bdd chaining boolean_operations implementations.
   * Creates {@link CDD}s from type {@link CBDDNode}.
   *
   * @author Stephan Holzner
   * @see CBDDNode
   * @see CBDDNode
   * @since 1.0
   */
  public static class Factory implements DD.Factory<DD> {

    /** The logical true leaf representation. */
    private CBDDNode one;

    /** The logical false leaf representation. */
    private CBDDNode zero;

    /** {@inheritDoc} */
    @Override
    public DD createNode(int var, DD low, DD high) {
      return new CBDDNode((CBDDNode) low, (CBDDNode) high, var);
    }

    /** {@inheritDoc} */
    @Override
    public CBDDNode createTrue() {
      if (one == null) {
        one = new CBDDNode(null, null, -1);
      }
      return one;
    }

    /** {@inheritDoc} */
    @Override
    public CBDDNode createFalse() {
      if (zero == null) {
        zero = new CBDDNode(null, null, -2);
      }
      return zero;
    }

    /** {@inheritDoc} */
    @Override
    public void setLow(DD low, DD node) {}

    /** {@inheritDoc} */
    @Override
    public void setHigh(DD high, DD node) {}

    /** {@inheritDoc} */
    @Override
    public void setVariable(int variable, DD node) {}

    @Override
    public ChildNodeResolver<DD> getChildNodeResolver() {
      return new ChildNodeResolver<>() {
        @Override
        public DD getLow(DD root) {
          return root.getLow();
        }

        @Override
        public DD getHigh(DD root) {
          return root.getHigh();
        }

        @Override
        public CBDDNode cast(Object root) {
          return (CBDDNode) root;
        }
      };
    }
  }
}
