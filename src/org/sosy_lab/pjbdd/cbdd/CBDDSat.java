// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>, Sebastian Niedner
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.cbdd;

import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.BitMaskIntTupleUtils;

/**
 * CBDD based {@link SatAlgorithm} implementation. Uses serial algorithms for cbdd sat operations.
 *
 * @author Sebastian Niedner
 * @see SatAlgorithm
 * @since ?.?
 */
public class CBDDSat implements SatAlgorithm<DD> {

  protected final Cache<DD, BigInteger> satCountCache;

  protected final NodeManager<DD> nodeManager;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public CBDDSat(Cache<DD, BigInteger> satCountCache, NodeManager<DD> nodeManager) {
    this.satCountCache = satCountCache;
    this.nodeManager = nodeManager;
  }

  /**
   * Recursive calculation of number of possible satisfying truth assignments for a given BDD.
   *
   * @param b - a DD
   * @return root's number of possible satisfying truth assignments
   */
  @Override
  public BigInteger satCount(DD b) {
    Set<Integer> variables = new HashSet<>();
    varCountRec(b, variables);
    return satCountRec(b, variables.size(), 0);
  }

  /**
   * Helper to recursive calculate number of variables for a given BDD.
   *
   * @param root - a DD
   * @param foundUniqueVariables - Set of already traversed Variables
   */
  protected void varCountRec(DD root, Set<Integer> foundUniqueVariables) {
    if (root.isFalse()) {
      return;
    }

    if (root.isTrue()) {
      return;
    }

    CDD r = castToChained(root);
    int start = r.getVariable();
    int end = r.getChainEndVariable();
    for (int i = start; i <= end; i++) {
      foundUniqueVariables.add(i);
    }
    varCountRec(high(root), foundUniqueVariables);
    varCountRec(low(root), foundUniqueVariables);
  }

  /**
   * Recursive calculation of number of possible satisfying truth assignments for a given BDD.
   *
   * @param root - a BDD
   * @return root's number of possible satisfying truth assignments
   */
  protected BigInteger satCountRec(DD root, int variablesCount, int foundUniqueVariables) {
    if (root.isFalse()) {
      return BigInteger.ZERO;
    }
    if (root.isTrue()) {
      return BigInteger.TWO.pow(variablesCount - foundUniqueVariables);
    }
    CDD r = castToChained(root);
    BigInteger size = BigInteger.ZERO;
    for (int i = 1; i <= r.getChainEndVariable() - r.getVariable() + 1; i++) {
      size =
          size.add(
              satCountRec(nodeManager.getHigh(root), variablesCount, foundUniqueVariables + i));
    }
    size =
        size.add(
            satCountRec(
                nodeManager.getLow(root),
                variablesCount,
                foundUniqueVariables + (r.getChainEndVariable() - r.getVariable()) + 1));

    return size;
  }

  @SuppressWarnings("unused")
  private int level(DD bdd) {
    return nodeManager.level(bdd.getVariable());
  }

  @SuppressWarnings("unused")
  private int chainEndLevel(DD bdd) {
    return nodeManager.level(castToChained(bdd).getChainEndVariable());
  }

  @SuppressWarnings("unused")
  private BigInteger selfValue(int level, int chainEndLevel, int childLevel) {
    int n = childLevel - level;
    int m = childLevel - chainEndLevel;

    return BigInteger.valueOf(2).pow(n).subtract(BigInteger.valueOf(2).pow(m - 1));
  }

  /** {@inheritDoc} */
  @Override
  public DD anySat(DD bdd) {
    if (bdd.isLeaf()) {
      return bdd;
    }
    if (bdd.getLow().isFalse()) {
      return nodeManager.makeNode(
          nodeManager.getFalse(),
          anySat(nodeManager.getHigh(bdd)),
          BitMaskIntTupleUtils.createTuple(
              bdd.getVariable(), castToChained(bdd).getChainEndVariable()));
    } else {
      return nodeManager.makeNode(
          anySat(nodeManager.getLow(bdd)),
          nodeManager.getFalse(),
          BitMaskIntTupleUtils.createTuple(
              bdd.getVariable(), castToChained(bdd).getChainEndVariable()));
    }
  }

  /**
   * helper call to determine bdd's high successor.
   *
   * @param bdd - the bdd
   * @return high successor
   */
  protected DD high(DD bdd) {
    return nodeManager.getHigh(bdd);
  }

  /**
   * helper call to determine bdd's low successor.
   *
   * @param bdd - the bdd
   * @return low successor
   */
  protected DD low(DD bdd) {
    return nodeManager.getLow(bdd);
  }

  /**
   * helper call to cast to CDD.
   *
   * @param bdd - the bdd
   * @return Casted CDD
   */
  private CDD castToChained(DD bdd) {
    Preconditions.checkArgument(bdd instanceof CDD);
    return (CDD) bdd;
  }
}
