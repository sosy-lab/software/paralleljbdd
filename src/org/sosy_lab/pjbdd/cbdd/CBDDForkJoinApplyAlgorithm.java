// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>, Sebastian Niedner
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.cbdd;

import java.util.concurrent.ForkJoinTask;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * Asynchronous and Chained Version of the Apply Algorithm.
 *
 * @author Sebastian Niedner
 * @since ?.?
 */
public class CBDDForkJoinApplyAlgorithm extends CBDDApplyAlgorithm {

  /** Worker thread pool manager. */
  private final ParallelismManager parallelismManager;

  public CBDDForkJoinApplyAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<DD> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /** {@inheritDoc} */
  @Override
  public DD makeOp(DD f1, DD f2, DDAlgorithm.ApplyOp op) {
    return terminalCheck(f1, f2, op).orElseGet(() -> makeOpElse(f1, f2, op));
  }

  /**
   * Helper method for recursive computation task. Apply an operation on two input arguments, with
   * an given terminal function.
   *
   * @param f1 - first {@link CDD} argument
   * @param f2 - second {@link CDD} argument
   * @param op - the operationgetIfo be applied
   * @return resulting CBDD of this recursive step
   */
  @SuppressWarnings("resource")
  private DD makeOpElse(DD f1, DD f2, DDAlgorithm.ApplyOp op) {
    int t = calculateTLevel(f1, f2);
    int b = calculateBLevel(f1, f2, t);

    DD low0 = cofactorLow(f1, b);
    DD high0 = cofactorHigh(f1, b);

    DD low1 = cofactorLow(f2, b);
    DD high1 = cofactorHigh(f2, b);

    // avoid terminal case fork
    DD low = terminalCheck(low0, low1, op).orElse(null);
    DD high = terminalCheck(high0, high1, op).orElse(null);

    if (parallelismManager.canFork(b) && low == null && high == null) {
      ForkJoinTask<DD> lowTask =
          parallelismManager.getThreadPool().submit(() -> makeOpElse(low0, low1, op));
      ForkJoinTask<DD> highTask =
          parallelismManager.getThreadPool().submit(() -> makeOpElse(high0, high1, op));
      low = lowTask.join();
      high = highTask.join();
    } else {
      if (low == null) {
        low = makeOpElse(low0, low1, op);
      }
      if (high == null) {
        high = makeOpElse(high0, high1, op);
      }
    }

    DD res = combine(low, high, t, b);

    this.cacheBinaryItem(f1, f2, op.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("resource")
  public DD makeCompose(DD f1, int var, DD f2) {
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), var, f2.hashCode());
    @SuppressWarnings("unchecked")
    Cache.CacheDataBinaryOp<DD> data = (Cache.CacheDataBinaryOp<DD>) composeCache.get(hash);
    if (data != null && data.matches(f1, f2, var)) {
      return data.getRes();
    }

    int f1Level = level(f1);
    int varLevel = nodeManager.level(var);

    if (f1Level > varLevel) {
      return f1;
    }

    DD res;
    int f1ChainEndLevel = chainEndlevel(f1);
    boolean isChained = f1ChainEndLevel - f1Level > 0;
    boolean varInChain = f1ChainEndLevel >= varLevel;

    if (isChained && varInChain) {
      DD rupturedU = rupture(f1, var);
      res = makeCompose(rupturedU, var, f2);
    } else if (f1.getVariable() == var) {
      res = makeIte(f2, nodeManager.getHigh(f1), nodeManager.getLow(f1));
    } else {
      DD thenBdd;
      DD elseBdd;
      if (parallelismManager.canFork(f1ChainEndLevel)) {
        ForkJoinTask<DD> lowTask =
            parallelismManager
                .getThreadPool()
                .submit(() -> makeCompose(nodeManager.getHigh(f1), var, f2));
        ForkJoinTask<DD> highTask =
            parallelismManager
                .getThreadPool()
                .submit(() -> makeCompose(nodeManager.getLow(f1), var, f2));
        thenBdd = lowTask.join();
        elseBdd = highTask.join();
      } else {
        thenBdd = makeCompose(nodeManager.getHigh(f1), var, f2);
        elseBdd = makeCompose(nodeManager.getLow(f1), var, f2);
      }

      res = makeIte(nodeManager.makeIthVar(f1.getVariable()), thenBdd, elseBdd);
    }

    data = new Cache.CacheDataBinaryOp<>();
    data.setRes(res);
    data.setOp(var);
    data.setF1(f1);
    data.setF2(f2);
    composeCache.put(hash, data);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("resource")
  protected DD makeIteElse(DD ifBDD, DD thenBDD, DD elseBDD) {
    int t = calculateTLevel(ifBDD, thenBDD, elseBDD);
    int b = calculateBLevel(ifBDD, thenBDD, elseBDD, t);

    DD ifLow = cofactorLow(ifBDD, b);
    DD thenLow = cofactorLow(thenBDD, b);
    DD elseLow = cofactorLow(elseBDD, b);
    DD ifHigh = cofactorHigh(ifBDD, b);
    DD thenHigh = cofactorHigh(thenBDD, b);
    DD elseHigh = cofactorHigh(elseBDD, b);

    // avoid terminal case fork
    DD low = terminalIteCheck(ifLow, thenLow, elseLow).orElse(null);

    DD high = terminalIteCheck(ifHigh, thenHigh, elseHigh).orElse(null);

    if (parallelismManager.canFork(b) && low == null && high == null) {
      ForkJoinTask<DD> lowTask =
          parallelismManager.getThreadPool().submit(() -> makeIte(ifLow, thenLow, elseLow));
      ForkJoinTask<DD> highTask =
          parallelismManager.getThreadPool().submit(() -> makeIte(ifHigh, thenHigh, elseHigh));
      low = lowTask.join();
      high = highTask.join();
    } else {
      if (low == null) {
        low = makeIte(ifLow, thenLow, elseLow);
      }
      if (high == null) {
        high = makeIte(ifHigh, thenHigh, elseHigh);
      }
    }

    DD res = combine(low, high, t, b);

    cacheItem(ifBDD, thenBDD, elseBDD, res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
