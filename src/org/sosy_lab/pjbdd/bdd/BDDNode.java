// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd;

import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Main {@link DD} implementation used in most regular bdd boolean_operations implementations.
 *
 * @author Stephan Holzner
 * @see DD
 * @since 1.0
 */
public class BDDNode implements DD {

  /** a bdd nodes low child. */
  protected BDDNode low;

  /** a bdd nodes high child. */
  protected BDDNode high;

  /** a bdd nodes variable. */
  protected int var;

  /** a bdd nodes hashcode, only calculated once cause recursive calculation required. */
  protected int hashCode;

  /**
   * Creates a new {@link BDDNode} with specified variable, low and high branch. Constructor is
   * protected because it should not be called outside {@link BDDNode.Factory}.
   *
   * @param low - the specified low branch
   * @param high - the specified high branch
   * @param var - the specified variable
   * @see BDDNode.Factory
   */
  protected BDDNode(BDDNode low, BDDNode high, int var) {
    this.low = low;
    this.high = high;
    this.var = var;
    rehash();
  }

  protected BDDNode() {}

  /** {@inheritDoc} */
  @Override
  public int getVariable() {
    return var;
  }

  /** {@inheritDoc} */
  @Override
  public BDDNode getLow() {
    if (isLeaf()) {
      return this;
    }
    return low;
  }

  /** {@inheritDoc} */
  @Override
  public BDDNode getHigh() {
    if (isLeaf()) {
      return this;
    }
    return high;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTrue() {
    return var == -1;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFalse() {
    return var == -2;
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object o) {
    if (!(o instanceof BDDNode)) {
      return false;
    }
    BDDNode other = (BDDNode) o;
    if (other.isTrue() && isTrue()) {
      return true;
    }
    if (other.isFalse() && isFalse()) {
      return true;
    }
    if (other.getVariable() != getVariable()) {
      return false;
    }
    return other.getHigh() == getHigh() && other.getLow() == getLow();
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return hashCode;
  }

  /** Recalculate nodes hashcode after changes due to node recycling or bdd reordering. */
  protected void rehash() {
    hashCode =
        (var < 0)
            ? Math.abs(var)
            : HashCodeGenerator.generateHashCode(
                getVariable(), getLow().hashCode(), getHigh().hashCode());
  }

  /**
   * Main BDD.Factory implementation used in most regular bdd boolean_operations implementations.
   * Creates {@link DD}s from type {@link BDDNode}.
   *
   * @author Stephan Holzner
   * @see DD
   * @see BDDNode
   * @since 1.0
   */
  public static class Factory implements DD.Factory<DD> {

    /** The logical true leaf representation. */
    private BDDNode one;

    /** The logical false leaf representation. */
    private BDDNode zero;

    /** {@inheritDoc} */
    @Override
    public BDDNode createNode(int var, DD low, DD high) {
      return new BDDNode((BDDNode) low, (BDDNode) high, var);
    }

    /** {@inheritDoc} */
    @Override
    public BDDNode createTrue() {
      if (one == null) {
        one = new BDDNode(null, null, -1);
      }
      return one;
    }

    /** {@inheritDoc} */
    @Override
    public BDDNode createFalse() {
      if (zero == null) {
        zero = new BDDNode(null, null, -2);
      }
      return zero;
    }

    /** {@inheritDoc} */
    @Override
    public void setLow(DD low, DD node) {
      ((BDDNode) node).low = (BDDNode) low;
      ((BDDNode) node).rehash();
    }

    /** {@inheritDoc} */
    @Override
    public void setHigh(DD high, DD node) {
      ((BDDNode) node).high = (BDDNode) high;
      ((BDDNode) node).rehash();
    }

    /** {@inheritDoc} */
    @Override
    public void setVariable(int variable, DD node) {
      ((BDDNode) node).var = variable;
      ((BDDNode) node).rehash();
    }

    @Override
    public ChildNodeResolver<DD> getChildNodeResolver() {
      return new ChildNodeResolver<>() {
        @Override
        public DD getLow(DD root) {
          return root.getLow();
        }

        @Override
        public DD getHigh(DD root) {
          return root.getHigh();
        }

        @Override
        public BDDNode cast(Object root) {
          return (BDDNode) root;
        }
      };
    }
  }
}
