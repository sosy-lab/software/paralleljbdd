// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd;

import static org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm.ApplyOp.OP_AND;
import static org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm.ApplyOp.OP_OR;

import java.math.BigInteger;
import java.util.Set;
import java.util.TreeSet;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.sosy_lab.pjbdd.api.AbstractCreator;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.BDDCreatorSimpleStats;
import org.sosy_lab.pjbdd.util.IfThenElseData;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * {@link Creator} implementation with {@link DD} objects as bdd data structure.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public class BDDCreator extends AbstractCreator implements Creator {

  protected DDAlgorithm<DD> algorithm;

  @Nullable protected final Stats stats;

  /**
   * Creates new BDDCreator environment.
   *
   * @param nodeManager - the node manager to use
   * @param algorithm - the manipulation algorithm to use
   * @param satAlgorithm the sat algorithm to use
   */
  public BDDCreator(
      NodeManager<DD> nodeManager, DDAlgorithm<DD> algorithm, SatAlgorithm<DD> satAlgorithm) {
    super(nodeManager, satAlgorithm);
    this.algorithm = algorithm;
    this.stats = null;
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNot(DD f) {
    return algorithm.makeNot(f);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeAnd(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_AND);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeOr(DD f1, DD f2) {
    return makeOp(f1, f2, OP_OR);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeXor(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_XOR);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNor(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_NOR);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeXnor(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_XNOR);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNand(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_NAND);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeEqual(DD f1, DD f2) {
    return makeXnor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeUnequal(DD f1, DD f2) {
    return makeXor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeImply(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_IMP);
  }

  /**
   * Operation delegate method to algorithm implementation.
   *
   * @param f1 - first bdd argument
   * @param f2 - second bdd argument
   * @param op - operation to be performed
   * @return f1 op f2
   */
  private DD makeOp(DD f1, DD f2, DDAlgorithm.ApplyOp op) {
    return algorithm.makeOp(f1, f2, op);
  }

  /** {@inheritDoc} */
  @Override
  public DD anySat(DD bdd) {
    return satAlgorithm.anySat(bdd);
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(DD b) {
    return satAlgorithm.satCount(b);
  }

  /** {@inheritDoc} */
  @Override
  public Stats getCreatorStats() {
    if (stats == null) {
      int uniqueTableNodeCount = nodeManager.getNodeCount();
      int uniqueTableSize = nodeManager.getUniqueTableSize();
      int variableCount = nodeManager.getVarCount();
      int cacheSize = algorithm.getCacheSize();
      int cacheNodeCount = algorithm.getCacheNodeCount();
      return new BDDCreatorSimpleStats(
          uniqueTableSize, uniqueTableNodeCount, cacheSize, cacheNodeCount, variableCount);
    }
    return stats;
  }

  @Override
  public void cleanUnusedNodes() {
    nodeManager.cleanUnusedNodes();
  }

  /** {@inheritDoc} */
  @Override
  public IfThenElseData getIfThenElse(DD f) {
    return new IfThenElseData(makeIthVar(f.getVariable()), getHigh(f), getLow(f));
  }

  @Override
  public DD makeNode(DD low, DD high, int var) {
    return algorithm.makeNode(low, high, var);
  }

  private int level(DD node) {
    return nodeManager.level(node.getVariable());
  }

  /** {@inheritDoc} */
  @Override
  public DD makeExists(DD f1, DD[] f2) {

    DD fTemp = f2[0];
    for (DD f : f2) {
      fTemp = makeOp(fTemp, f, OP_AND);
    }
    Set<Integer> varSet = createHighVarLevelSet(fTemp);
    return algorithm.makeExists(f1, IntArrayUtils.toIntArray(varSet));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeExists(DD f1, DD f2) {
    Set<Integer> varSet = createHighVarLevelSet(f2);
    return algorithm.makeExists(f1, IntArrayUtils.toIntArray(varSet));
  }

  @Override
  public DD makeCompose(DD f1, int var, DD f2) {
    return algorithm.makeCompose(f1, var, f2);
  }

  @Override
  public DD makeReplace(DD f1, DD oldVar, DD replaceVar) {
    return makeExists(makeAnd(f1, makeXnor(oldVar, replaceVar)), oldVar);
  }

  /**
   * Takes a {@link DD} and creates a set of all high branch variables.
   *
   * @param f - the {@link DD} argument
   * @return a set of variables
   */
  private Set<Integer> createHighVarLevelSet(DD f) {
    Set<Integer> varLevelSet = new TreeSet<>();
    while (!f.isLeaf()) {
      varLevelSet.add(level(f));
      f = f.getHigh();
    }
    return varLevelSet;
  }

  /** {@inheritDoc} */
  @Override
  public DD getLow(DD top) {
    return nodeManager.getLow(top);
  }

  /** {@inheritDoc} */
  @Override
  public DD getHigh(DD top) {
    return nodeManager.getHigh(top);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeTrue() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public DD makeFalse() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(DD f1, DD f2, DD f3) {
    return algorithm.makeIte(f1, f2, f3);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(IfThenElseData iteData) {
    return makeIte(iteData.getIf(), iteData.getThen(), iteData.getElse());
  }

  /** {@inheritDoc} */
  @Override
  public DD makeVariable() {
    return makeNode(makeFalse(), makeTrue(), nodeManager.getVarCount());
  }

  @Override
  public DD makeVariableBefore(DD var) {
    return nodeManager.makeVariableBefore(var);
  }

  /** {@inheritDoc} */
  @Override
  public DD restrict(DD bdd, int var, boolean restrictionVar) {
    return bdd.getVariable() != var ? bdd : restrictionVar ? getHigh(bdd) : getLow(bdd);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    super.shutDown();
    algorithm.shutdown();
  }
}
