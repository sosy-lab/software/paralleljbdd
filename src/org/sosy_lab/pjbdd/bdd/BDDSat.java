// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd;

import com.google.common.collect.ImmutableSet;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;

/**
 * BDD based {@link SatAlgorithm} implementation. Uses serial algorithms for bdd sat operations.
 *
 * @author Stephan Holzner
 * @see SatAlgorithm
 * @since 1.1
 */
public class BDDSat<V extends DD> implements SatAlgorithm<V> {

  /** {@link Cache} used for computed sat counts. */
  protected final Cache<V, BigInteger> satCountCache;

  protected final NodeManager<V> nodeManager;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public BDDSat(Cache<V, BigInteger> satCountCache, NodeManager<V> nodeManager) {
    this.satCountCache = satCountCache;
    this.nodeManager = nodeManager;
  }

  /** {@inheritDoc} */
  @Override
  public V anySat(V bdd) {
    if (bdd.isLeaf()) {
      return bdd;
    }
    if (bdd.getLow().isFalse()) {
      return nodeManager.makeNode(nodeManager.getFalse(), anySat(high(bdd)), bdd.getVariable());
    } else {
      return nodeManager.makeNode(anySat(low(bdd)), nodeManager.getFalse(), bdd.getVariable());
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(V b) {
    Set<Integer> variables = new HashSet<>();
    varCountRec(b, variables);
    int varCountRec = variables.size();
    return satCountRec(b, varCountRec, ImmutableSet.of());
  }

  /**
   * Recursive calculation of number of possible satisfying truth assignments for a given BDD.
   *
   * @param root - a BDD
   * @return root's number of possible satisfying truth assignments
   */
  protected BigInteger satCountRec(V root, int variablesCount, Set<V> foundUniqueVariables) {
    if (root.isFalse()) {
      return BigInteger.ZERO;
    }
    if (root.isTrue()) {
      return BigInteger.ONE.multiply(
          BigInteger.TWO.pow(variablesCount - foundUniqueVariables.size()));
    }
    BigInteger count = satCountCache.get(root);
    if (count != null) {
      // TODO: Think about caching again.
      // Daniel: i think caching is not possible easily as you might encounter a node
      // twice with different numbers of found unique vars
      // return count;
    }

    BigInteger size =
        satCountRec(
                high(root),
                variablesCount,
                ImmutableSet.<V>builder().addAll(foundUniqueVariables).add(root).build())
            .add(
                satCountRec(
                    low(root),
                    variablesCount,
                    ImmutableSet.<V>builder().addAll(foundUniqueVariables).add(root).build()));

    satCountCache.put(root, size);

    return size;
  }

  protected void varCountRec(V root, Set<Integer> foundUniqueVariables) {
    if (root.isFalse()) {
      return;
    }

    if (root.isTrue()) {
      return;
    }

    foundUniqueVariables.add(root.getVariable());
    varCountRec(high(root), foundUniqueVariables);
    varCountRec(low(root), foundUniqueVariables);
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param bdd - the bdd
   * @return level of bdd
   */
  protected int level(V bdd) {
    return nodeManager.level(bdd.getVariable());
  }

  /**
   * helper call to determine bdd's high successor.
   *
   * @param bdd - the bdd
   * @return high successor
   */
  protected V high(V bdd) {
    return nodeManager.getHigh(bdd);
  }

  /**
   * helper call to determine bdd's low successor.
   *
   * @param bdd - the bdd
   * @return low successor
   */
  protected V low(V bdd) {
    return nodeManager.getLow(bdd);
  }
}
