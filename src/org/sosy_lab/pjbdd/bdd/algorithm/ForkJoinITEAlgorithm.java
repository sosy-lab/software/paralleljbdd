// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.concurrent.ForkJoinTask;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link DDAlgorithm} implementation which uses {@link ParallelITEAlgorithm} as base class. The
 * java ForkJoin framework is used to perform asynchronous, parallel 'ITE' calculations.
 *
 * @author Stephan Holzner
 * @see ParallelITEAlgorithm
 * @since 1.0
 */
public class ForkJoinITEAlgorithm<V extends DD> extends ParallelITEAlgorithm<V> {

  /**
   * Creates new {@link ForkJoinITEAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public ForkJoinITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager, parallelismManager);
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("resource")
  protected V forkITE(V lowF1, V lowF2, V lowF3, V highF1, V highF2, V highF3, int topVar) {
    parallelismManager.taskSupplied();
    ForkJoinTask<V> lowTask =
        parallelismManager.getThreadPool().submit(() -> makeIte(lowF1, lowF2, lowF3));
    ForkJoinTask<V> highTask =
        parallelismManager.getThreadPool().submit(() -> makeIte(highF1, highF2, highF3));
    V bdd = makeNode(lowTask.join(), highTask.join(), topVar);
    parallelismManager.taskDone();
    return bdd;
  }
}
