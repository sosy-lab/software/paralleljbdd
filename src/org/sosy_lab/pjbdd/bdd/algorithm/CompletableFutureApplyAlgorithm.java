// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link DDAlgorithm} implementation which uses {@link ApplyBDDAlgorithm} as base class. But
 * {@link CompletableFuture}s are used to perform asynchronous, parallel 'APPLY' calculations.
 *
 * @author Stephan Holzner
 * @see DDAlgorithm
 * @see ApplyBDDAlgorithm
 * @since 1.0
 */
public class CompletableFutureApplyAlgorithm<V extends DD> extends ApplyBDDAlgorithm<V> {

  /** Threadpool holder instance. */
  private final ParallelismManager parallelismManager;

  /**
   * Creates new {@link CompletableFutureApplyAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link Builders} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public CompletableFutureApplyAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /**
   * Asynchronous calculation for applying an operation on two input arguments, with an given
   * terminal function.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param op - the operation to be applied
   * @return (f1 op f2)
   */
  @Override
  public V makeOp(V f1, V f2, ApplyOp op) {
    return terminalCheck(f1, f2, op).orElseGet(() -> asyncShannonExpansion(f1, f2, op).join());
  }

  /**
   * Creates an asynchronous recursive computation task as CompletableFuture. Apply an operation on
   * two input arguments, with an given terminal function.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param op - the operation to be applied
   * @return constructed task as CompletableFuture
   */
  @SuppressWarnings("FutureReturnValueIgnored")
  private CompletableFuture<V> asyncShannonExpansion(V f1, V f2, ApplyOp op) {
    int topVar = topVar(level(f1), level(f2));

    CompletableFuture<V> lowFut = expandNext(low(f1, topVar), low(f2, topVar), topVar, op);
    CompletableFuture<V> highFut = expandNext(high(f1, topVar), high(f2, topVar), topVar, op);
    CompletableFuture<V> future =
        highFut.thenCombine(lowFut, (pos, neg) -> makeNode(neg, pos, topVar));
    // Listener for caching

    future.thenAccept(result -> this.cacheBinaryItem(f1, f2, op.ordinal(), result));
    return future;
  }

  /**
   * Helper method for {@link #asyncShannonExpansion(DD, DD, ApplyOp)} and creates the next
   * recursion step. Checks for terminal case and forking possibility (tries to avoid
   * over-threading)
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param topLevel - the two arguments topmost level
   * @return next recursion step
   */
  @SuppressWarnings({"FutureReturnValueIgnored", "resource"})
  private CompletableFuture<V> expandNext(V f1, V f2, int topLevel, ApplyOp op) {
    return terminalCheck(f1, f2, op)
        .map(CompletableFuture::completedFuture)
        .orElseGet(
            () -> {
              if (parallelismManager.canFork(topLevel)) {
                parallelismManager.taskSupplied();
                CompletableFuture<V> result =
                    CompletableFuture.supplyAsync(
                            () -> asyncShannonExpansion(f1, f2, op),
                            parallelismManager.getThreadPool())
                        .thenCompose(Function.identity());
                result.thenRun(parallelismManager::taskDone);
                return result;
              } else {
                return asyncShannonExpansion(f1, f2, op);
              }
            });
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
