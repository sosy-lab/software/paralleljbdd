// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.AbstractBDDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link AbstractBDDAlgorithm} implementation which uses {@link ParallelITEAlgorithm} as base
 * class. A parallel {@link Stream} is used to perform asynchronous 'ITE' calculations.
 *
 * @author Stephan Holzner
 * @see ParallelITEAlgorithm
 * @since 1.0
 */
public class StreamITEAlgorithm<V extends DD> extends ParallelITEAlgorithm<V> {

  /**
   * Creates new {@link StreamITEAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   * @param parallelismManager - the worker thread pool manager
   */
  public StreamITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager, parallelismManager);
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("resource")
  protected V forkITE(V lowF1, V lowF2, V lowF3, V highF1, V highF2, V highF3, int topVar) {
    parallelismManager.taskSupplied();
    parallelismManager.taskSupplied();
    Map<Boolean, V> subs =
        parallelismManager
            .getThreadPool()
            .submit(
                () ->
                    Stream.of(false, true)
                        .parallel()
                        .collect(
                            Collectors.toMap(
                                b -> b,
                                b -> {
                                  if (b) {
                                    return makeIte(highF1, highF2, highF3);
                                  } else {
                                    return makeIte(lowF1, lowF2, lowF3);
                                  }
                                })))
            .join();
    parallelismManager.taskDone();
    parallelismManager.taskDone();
    return makeNode(subs.get(false), subs.get(true), topVar);
  }
}
