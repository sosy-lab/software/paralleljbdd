// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.Optional;
import java.util.concurrent.ForkJoinTask;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link DDAlgorithm} implementation which uses {@link ApplyBDDAlgorithm} as base class. But
 * ForkJoin Framework is used to perform asynchronous, parallel 'APPLY' calculations.
 *
 * @author Stephan Holzner
 * @see DDAlgorithm
 * @see ApplyBDDAlgorithm
 * @since 1.0
 */
public class ForkJoinApplyAlgorithm<V extends DD> extends ApplyBDDAlgorithm<V> {

  /** Worker thread pool manager. */
  private final ParallelismManager parallelismManager;

  /**
   * Creates new {@link ForkJoinApplyAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public ForkJoinApplyAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /** {@inheritDoc} */
  @Override
  public V makeOp(V f1, V f2, ApplyOp op) {
    return terminalCheck(f1, f2, op).orElseGet(() -> asyncShannonExpansion(f1, f2, op));
  }

  /**
   * Helper method for recursive computation task. Apply an operation on two input arguments, with
   * an given terminal function.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param op - the operation to be applied
   * @return next recursion step
   */
  @SuppressWarnings("resource")
  private V asyncShannonExpansion(V f1, V f2, ApplyOp op) {
    int topVar = topVar(level(f1), level(f2));
    V res;
    V low = null;
    V high = null;
    V lowF1 = low(f1, topVar);
    V lowF2 = low(f2, topVar);
    V highF1 = high(f1, topVar);
    V highF2 = high(f2, topVar);

    // avoid terminal case fork
    Optional<V> lowCheck = terminalCheck(lowF1, lowF2, op);
    Optional<V> highCheck = terminalCheck(highF1, highF2, op);

    if (lowCheck.isPresent()) {
      low = lowCheck.get();
    }
    if (highCheck.isPresent()) {
      high = highCheck.get();
    }

    if (parallelismManager.canFork(topVar) && low == null && high == null) {
      ForkJoinTask<V> lowTask =
          parallelismManager.getThreadPool().submit(() -> asyncShannonExpansion(lowF1, lowF2, op));
      ForkJoinTask<V> highTask =
          parallelismManager
              .getThreadPool()
              .submit(() -> asyncShannonExpansion(highF1, highF2, op));
      low = lowTask.join();
      high = highTask.join();
    } else {
      if (low == null) {
        low = asyncShannonExpansion(lowF1, lowF2, op);
      }
      if (high == null) {
        high = asyncShannonExpansion(highF1, highF2, op);
      }
    }
    res = makeNode(low, high, topVar);
    this.cacheBinaryItem(f1, f2, op.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
