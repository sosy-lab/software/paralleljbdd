// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link DDAlgorithm} implementation which uses {@link ITEBDDAlgorithm} as base class. {@link
 * CompletableFuture}s are used to perform asynchronous, parallel 'ITE' calculations.
 *
 * @author Stephan Holzner
 * @see ITEBDDAlgorithm
 * @since 1.0
 */
public class CompletableFutureITEAlgorithm<V extends DD> extends ITEBDDAlgorithm<V> {
  /** Worker thread pool manager. */
  private final ParallelismManager parallelismManager;

  /**
   * Creates new {@link CompletableFutureITEAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public CompletableFutureITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /** {@inheritDoc} */
  @Override
  public V makeIte(V f1, V f2, V f3) {
    return terminalIteCheck(f1, f2, f3)
        .map(CompletableFuture::completedFuture)
        .orElseGet(() -> asyncExpand(f1, f2, f3))
        .join();
  }

  /**
   * Recursive construction of an 'ITE' task as async CompletableFuture.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return the constructed CompletableFuture
   */
  @SuppressWarnings({"FutureReturnValueIgnored", "resource"})
  private CompletableFuture<V> asyncExpand(V f1, V f2, V f3) {
    int topVar = topVar(level(f1), level(f2), level(f3));

    ExecutorService service = parallelismManager.getThreadPool();

    CompletableFuture<V> lowFut =
        expandNext(low(f1, topVar), low(f2, topVar), low(f3, topVar), service, topVar);

    CompletableFuture<V> highFut =
        expandNext(high(f1, topVar), high(f2, topVar), high(f3, topVar), service, topVar);

    CompletableFuture<V> future =
        highFut.thenCombine(lowFut, (pos, neg) -> makeNode(neg, pos, topVar));
    // Listener for caching
    future.thenAccept(result -> cacheItem(f1, f2, f3, result));
    return future;
  }

  /**
   * Creates next recursion step. Checks for terminal case and forking possibility (to avoid
   * over-threading)
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @param service - the actual thread pool
   * @param topLevel - the topmost level
   * @return next recursion step as CompletableFuture
   */
  @SuppressWarnings({"FutureReturnValueIgnored", "resource"})
  private CompletableFuture<V> expandNext(V f1, V f2, V f3, ExecutorService service, int topLevel) {
    return terminalIteCheck(f1, f2, f3)
        .map(CompletableFuture::completedFuture)
        .orElseGet(
            () -> {
              if (parallelismManager.canFork(topLevel)) {
                parallelismManager.taskSupplied();
                CompletableFuture<V> result =
                    CompletableFuture.supplyAsync(() -> asyncExpand(f1, f2, f3), service)
                        .thenCompose(Function.identity());
                result.thenRun(parallelismManager::taskDone);
                return result;
              } else {
                return asyncExpand(f1, f2, f3);
              }
            });
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
