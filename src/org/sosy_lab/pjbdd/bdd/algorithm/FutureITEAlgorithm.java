// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link DDAlgorithm} implementation which uses {@link ParallelITEAlgorithm} as base class.
 * {@link Future}s are used to perform asynchronous, parallel 'ITE' calculations.
 *
 * @author Stephan Holzner
 * @see DDAlgorithm
 * @see ParallelITEAlgorithm
 * @since 1.0
 */
public class FutureITEAlgorithm<V extends DD> extends ParallelITEAlgorithm<V> {

  /**
   * Creates new {@link FutureITEAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public FutureITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager, parallelismManager);
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("resource")
  protected V forkITE(V lowF1, V lowF2, V lowF3, V highF1, V highF2, V highF3, int topVar) {
    parallelismManager.taskSupplied();
    parallelismManager.taskSupplied();

    ExecutorService service = parallelismManager.getThreadPool();
    Future<V> lowFut = service.submit(() -> makeIte(lowF1, lowF2, lowF3));
    Future<V> highFut = service.submit(() -> makeIte(highF1, highF2, highF3));
    try {
      V bdd = makeNode(lowFut.get(), highFut.get(), topVar);
      parallelismManager.taskDone();
      parallelismManager.taskDone();
      return bdd;
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e.getCause());
    }
  }
}
