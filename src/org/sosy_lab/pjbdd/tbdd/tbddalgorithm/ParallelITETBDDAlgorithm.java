// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddalgorithm;

import java.util.Optional;
import java.util.concurrent.ForkJoinTask;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.bdd.algorithm.ITEBDDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.tbdd.TBDDNodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * abstract {@link ITEBDDAlgorithm} implementation with {@link TBDD} objects as tbdd data structure.
 * Base class for concurrent ITETBDDAlgorithm implementations Subclasses:
 *
 * @author Simon Raths
 * @see Creator
 * @see ITETBDDAlgorithm
 * @since 1.2
 */
public class ParallelITETBDDAlgorithm extends ITETBDDAlgorithm {
  /** Worker thread pool manager. */
  protected final ParallelismManager parallelismManager;

  /**
   * Creates new {@link ParallelITETBDDAlgorithm} instances with given parameters.
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   * @param parallelismManager - the parallelism manager
   */
  public ParallelITETBDDAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      TBDDNodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /**
   * Asynchronous 'ITE' calculation for input triple.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return ite(f1, f2, f3)
   */
  @Override
  @SuppressWarnings("resource")
  public TBDD makeIte(TBDD f1, TBDD f2, TBDD f3) {
    Optional<TBDD> check = terminalIteCheck(f1, f2, f3);
    if (check.isPresent()) {
      return check.get();
    }
    int tempTopVar = topVar(level(f1.getTag()), level(f2.getTag()), level(f3.getTag()));
    int topVar;
    if ((f1.getTag() == f2.getTag() && f1.getTag() == f3.getTag()) && f2.getTag() == f3.getTag()) {
      topVar = topVar(level(f1), level(f2), level(f3));
    } else {
      topVar = tempTopVar;
    }
    int next = getNextVar(topVar);

    TBDD res;
    TBDD low = null;
    TBDD high = null;
    TBDD lowF1 = low(f1, topVar, next);
    TBDD lowF2 = low(f2, topVar, next);
    TBDD lowF3 = low(f3, topVar, next);
    TBDD highF1 = high(f1, topVar);
    TBDD highF2 = high(f2, topVar);
    TBDD highF3 = high(f3, topVar);

    Optional<TBDD> lowCheck;
    Optional<TBDD> highCheck;
    lowCheck = terminalIteCheck(lowF1, lowF2, lowF3);
    highCheck = terminalIteCheck(highF1, highF2, highF3);

    if (lowCheck.isPresent()) {
      low = lowCheck.get();
    }
    if (highCheck.isPresent()) {
      high = highCheck.get();
    }

    if (forkCheck(low, high, topVar)) {
      parallelismManager.taskSupplied();
      ForkJoinTask<TBDD> lowTask =
          parallelismManager.getThreadPool().submit(() -> makeIte(lowF1, lowF2, lowF3));
      ForkJoinTask<TBDD> highTask =
          parallelismManager.getThreadPool().submit(() -> makeIte(highF1, highF2, highF3));
      res = makeNode(lowTask.join(), highTask.join(), topVar, next);
      parallelismManager.taskDone();
    } else {
      if (low == null) {
        low = makeIte(lowF1, lowF2, lowF3);
      }
      if (high == null) {
        high = makeIte(highF1, highF2, highF3);
      }
      res = makeNode(low, high, topVar);
    }
    this.cacheItem(f1, f2, f3, res);
    return res;
  }

  /**
   * Check if forkITE to be called.
   *
   * @param low - terminal checked low branch
   * @param high - terminal checked high branch
   * @param topVar - ite recursions top var
   * @return true if 'forkITE' will be called in current recursion step false else
   */
  private boolean forkCheck(TBDD low, TBDD high, int topVar) {
    return parallelismManager.canFork(topVar) && high == null && low == null;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
