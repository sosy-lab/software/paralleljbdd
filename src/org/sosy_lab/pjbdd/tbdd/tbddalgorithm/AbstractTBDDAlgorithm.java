// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddalgorithm;

import static org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm.ApplyOp.OP_OR;

import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.ManipulatingAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.tbdd.TBDDNodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * An abstract TBDDManipulatingAlgorithm implementation.
 *
 * @see ManipulatingAlgorithm
 * @since 1.1
 */
public abstract class AbstractTBDDAlgorithm extends TBDDManipulatingAlgorithm
    implements DDAlgorithm<TBDD> {

  /** additional cache for existential quantification. */
  protected final Cache<Integer, Cache.CacheData> quantCache;

  protected final Cache<Integer, Cache.CacheData> composeCache;

  public AbstractTBDDAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, TBDDNodeManager tbddNodeManager) {
    super(computedTable, tbddNodeManager);
    quantCache = computedTable.cleanCopy();
    composeCache = computedTable.cleanCopy();
  }

  @Override
  public TBDD makeExists(TBDD f, int... levels) {
    if (f.isLeaf() || (level(f) > levels[0] && levels.length == 1)) {
      return f;
    }

    int hash = HashCodeGenerator.generateHashCode(f.hashCode(), levels[0]);
    @SuppressWarnings("unchecked")
    Cache.CacheDataExQuantVararg<TBDD> data =
        (Cache.CacheDataExQuantVararg<TBDD>) quantCache.get(hash);
    TBDD res = null;

    if (data != null && data.matches(f, levels)) {
      res = data.getRes();
    }
    if (res == null) {
      int fLevel = level(f);
      if (fLevel > levels[0]) {
        int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
        res = makeExists(f, newTail);
      } else if (fLevel == levels[0]) {
        if (levels.length == 1) {
          res = makeOp(f.getHigh(), f.getLow(), OP_OR);
        } else {
          int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
          res = makeOp(makeExists(f.getLow(), newTail), makeExists(f.getHigh(), newTail), OP_OR);
        }
      } else {
        TBDD high = makeExists(f.getHigh(), levels);
        TBDD low = makeExists(f.getLow(), levels);
        res = makeNode(low, high, f.getVariable());
      }

      Cache.CacheDataExQuantVararg<TBDD> d = new Cache.CacheDataExQuantVararg<>();
      d.setF(f);
      d.setLevels(levels);
      d.setRes(res);
      quantCache.put(hash, d);
    }
    return res;
  }

  @Override
  public TBDD makeCompose(TBDD f1, int var, TBDD f2) {
    if (level(f1) > tbddNodeManager.level(var)) {
      return f1;
    }
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), var, f2.hashCode());
    @SuppressWarnings("unchecked")
    Cache.CacheDataBinaryOp<TBDD> data = (Cache.CacheDataBinaryOp<TBDD>) composeCache.get(hash);
    if (data != null && data.matches(f1, f2, var)) {
      return data.getRes();
    }
    TBDD res;

    if (f1.getVariable() == var) {
      res = makeIte(f2, f1.getHigh(), f1.getLow());
    } else {
      TBDD i = makeCompose(f1.getHigh(), var, f2);
      TBDD e = makeCompose(f1.getLow(), var, f2);
      res = makeIte(tbddNodeManager.makeIthVar(f1.getVariable()), i, e);
    }
    data = new Cache.CacheDataBinaryOp<>();
    data.setRes(res);
    data.setOp(var);
    data.setF1(f1);
    data.setF2(f2);
    composeCache.put(hash, data);
    return res;
  }
}
