// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddalgorithm;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.Algorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.tbdd.TBDDNodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDEdge;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Abstract base class for {@link Algorithm} implementations.
 *
 * @author Stephan Holzner, Simon Raths
 * @see Algorithm
 * @since 1.1
 */
public abstract class TBDDManipulatingAlgorithm implements Algorithm<TBDD> {

  /** the node creating and handling instance. */
  protected final TBDDNodeManager tbddNodeManager;

  /** the computation cache. */
  protected final Cache<Integer, Cache.CacheData> computedTable;

  /**
   * Constructor for initializing variables.
   *
   * @param computedTable - the computation cache to be used
   * @param nodeManager - the node manager to be used
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public TBDDManipulatingAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, TBDDNodeManager nodeManager) {
    this.tbddNodeManager = nodeManager;
    this.computedTable = computedTable;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    tbddNodeManager.shutdown();
    computedTable.clear();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeFalse() {
    return tbddNodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeTrue() {
    return tbddNodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeNode(TBDD low, TBDD high, int var) {
    return tbddNodeManager.makeNode(low, high, var);
  }

  public TBDD makeNode(TBDD low, TBDD high, int var, int next) {
    return tbddNodeManager.makeNode(low, high, var, next);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD restrict(TBDD tbdd, int var, boolean restrictionVar) {
    return tbdd.getVariable() != var ? tbdd : restrictionVar ? tbdd.getHigh() : tbdd.getLow();
  }

  /**
   * Cofactoring for the given TBDDs low branch. These functions must be heavily modified should a
   * different set of reduction rules be used. 1. Checks if rule 1 was applied 2. Checks node is
   * below topVar and must therefore not be modified 3. Checks if rule 2 was applied reducing all
   * the remaining nodes and puts new tag on the edge 4. Checks if rule 2 was applied reducing only
   * a part of the remaining nodes and puts new tag on the edge 5. Checks if rule 2b was applied and
   * the node was added as a result and returns low child 6. Else returns low child
   *
   * @param f - given TBDD
   * @param topVar - the variable with the highest level calculated from the tags of the given TBDDs
   * @param nextVar - the variable directly below topVar
   * @return low, f, or newly tagged f
   */
  protected TBDD low(TBDD f, int topVar, int nextVar) {
    if (f.isFalse()) {
      return tbddNodeManager.getFalse();
    } else if (level(topVar) < level(f.getTag())) {
      return f;
    } else if (f.isTrue()) {
      return new TBDDEdge(f.getNode(), nextVar);
    } else if (level(topVar) < level(f.getVariable())) {
      if (!f.isLeaf() && f.getVariable() == nextVar) {
        if (f.getLow() == f.getHigh()) {
          return f.getLow();
        }
      }
      return new TBDDEdge(f.getNode(), nextVar);
    } else {
      return f.getLow();
    }
  }

  /**
   * Restricts given TBDD to it's high child. Same checks as before, if a reduction due to rule 2 is
   * found returns false.
   *
   * @param f - given TBDD
   * @param topVar - given level
   * @return high, f or false
   */
  protected TBDD high(TBDD f, int topVar) {
    if (f.isFalse()) {
      return tbddNodeManager.getFalse();
    } else if (level(topVar) < level(f.getTag())) {
      return f;
    } else if (f.isTrue()) {
      // Possible change needed for different set of reduction rules, taking the tag into
      // consideration
      return tbddNodeManager.getFalse();
    } else if (level(topVar) < level(f.getVariable())) {
      return tbddNodeManager.getFalse();
    } else {
      return f.getHigh();
    }
  }

  /**
   * find a various number of levels' topmost level variable.
   *
   * @param levels - variable amount of levels
   * @return topmost variable
   */
  protected int topVar(int... levels) {
    return tbddNodeManager.topVar(levels);
  }

  /**
   * Get a bdd node's variable level.
   *
   * @param bdd - a bdd node
   * @return nodes's variable level
   */
  protected int level(DD bdd) {
    return tbddNodeManager.level(bdd.getVariable());
  }

  protected int level(int tag) {
    return tbddNodeManager.level(tag);
  }

  protected int getNextVar(int topVar) {
    return tbddNodeManager.getNext(topVar);
  }

  /**
   * check if 'ITE' input triple already computed in cache.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return Optional of cached item or Optional empty
   */
  protected Optional<TBDD> checkITECache(TBDD f1, TBDD f2, TBDD f3) {
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), f3.hashCode());
    Cache.CacheData data = computedTable.get(hash);

    if (data instanceof Cache.CacheDataITE<?>) {
      @SuppressWarnings("unchecked")
      Cache.CacheDataITE<TBDD> d = (Cache.CacheDataITE<TBDD>) data;
      if ((d.getF1().equals(f1) && d.getF2().equals(f2)) && d.getF3().equals(f3)) {
        return Optional.of(d.getRes());
      }
    }
    return Optional.empty();
  }

  /**
   * Store item for 'ITE' input triple in computation cache.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @param item - output node
   */
  protected void cacheItem(TBDD f1, TBDD f2, TBDD f3, TBDD item) {
    Cache.CacheDataITE<TBDD> data = new Cache.CacheDataITE<>();
    data.setF1(f1);
    data.setF2(f2);
    data.setF3(f3);
    data.setRes(item);
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), f3.hashCode());
    cache(computedTable, data, hash);
  }

  /**
   * Store data in given computation cash for given hash.
   *
   * @param cache - given cash
   * @param data - to write
   * @param hash - data's input hash
   */
  protected void cache(Cache<Integer, Cache.CacheData> cache, Cache.CacheData data, int hash) {
    cache.put(hash, data);
  }
}
