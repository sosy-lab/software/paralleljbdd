// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddnode;

import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * TBDD equivalent to the BDDNode, using tagged edges as its children instead.
 *
 * @author Simon Raths
 * @since 1.0
 */
public class TBDDNode implements DD {

  private int var;

  private TBDD low;

  private TBDD high;

  private int hashCode;

  public TBDDNode(TBDD low, TBDD high, int var) {
    this.var = var;
    this.low = low;
    this.high = high;

    rehash();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTrue() {
    return var == -1;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFalse() {
    return var == -2;
  }

  /**
   * Checks for equal Nodes by first covering trival cases and then comparing the edges coming from
   * that node. Edges need to be checked via their own equals method since they are not unique.
   *
   * <p>{@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {

    if (!(o instanceof TBDDNode)) {
      return false;
    }
    TBDDNode other = (TBDDNode) o;
    if (other.isTrue() && isTrue()) {
      return true;
    }
    if (other.isFalse() && isFalse()) {
      return true;
    }
    if (other.getVariable() != getVariable()) {
      return false;
    }

    /*
    return this.getVariable() == other.getVariable()
            &&getLow().getVariable() == other.getLow().getVariable()
            &&getHigh().getVariable() == other.getHigh().getVariable();*/
    if (getLow().equals(other.getLow()) && getHigh().equals(other.getHigh())) {
      return true;
    }

    return false;
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return hashCode;
  }

  /** Recalculate nodes hashcode after changes due to node recycling or bdd reordering. */
  protected void rehash() {
    hashCode =
        (var < 0)
            ? Math.abs(var)
            : HashCodeGenerator.generateHashCode(
                getVariable(), getLow().hashCode(), getHigh().hashCode());
  }

  @Override
  public TBDD getLow() {
    return this.low;
  }

  @Override
  public TBDD getHigh() {
    return this.high;
  }

  @Override
  public int getVariable() {
    return this.var;
  }
}
