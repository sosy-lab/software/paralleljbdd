// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 * tagge bdds.
 *
 * @since 1.0
 * @author Stephan Holzner
 * @version 1.0
 */
package org.sosy_lab.pjbdd.tbdd.tbddnode;
