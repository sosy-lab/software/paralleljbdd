// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddnode;

import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.BDDNode;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Helperclass that combines a TBDDNode with a tag.
 *
 * @author Simon Raths
 * @since 1.0
 */
public class TBDDEdge implements TBDD {

  private final int tag;

  private final TBDDNode target;

  private int hashCode;

  public TBDDEdge(TBDDNode target, int tag) {
    this.target = target;
    this.tag = tag;
    rehash();
  }

  /** {@inheritDoc} */
  @Override
  public int getVariable() {
    return this.target.getVariable();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD getLow() {
    return this.target.getLow();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD getHigh() {
    return this.target.getHigh();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTrue() {
    return this.target.getVariable() == -1;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFalse() {
    return this.target.getVariable() == -2;
  }

  @Override
  public int getLowTag() {
    return this.target.getLow().getTag();
  }

  @Override
  public int getHighTag() {
    return this.target.getHigh().getTag();
  }

  @Override
  public int getTag() {
    return this.tag;
  }

  @Override
  public TBDDNode getNode() {
    return this.target;
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("ReferenceEquality")
  public boolean equals(Object o) {
    if (!(o instanceof TBDD)) {
      return false;
    }
    TBDD other = (TBDD) o;
    boolean equal = this.getNode().getVariable() == other.getNode().getVariable();
    equal = equal && (this.getTag() == other.getTag());
    if (this.getLow() != null) {
      equal = equal && this.getLow().equals(other.getLow());
    } else {
      equal = equal && this.getLow() == other.getLow();
    }

    if (this.getHigh() != null) {
      equal = equal && this.getHigh().equals(other.getHigh());
    } else {
      equal = equal && this.getHigh() == other.getHigh();
    }

    return equal;
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return hashCode;
  }

  /** Recalculate nodes hashcode after changes due to node recycling or bdd reordering. */
  protected void rehash() {
    hashCode =
        (this.getVariable() < 0)
            ? HashCodeGenerator.generateHashCode(Math.abs(this.getVariable()), this.getTag())
            : HashCodeGenerator.generateHashCode(
                getVariable(), getTag(), getLow().hashCode(), getHigh().hashCode());
  }

  /**
   * Main TBDD.Factory implementation used in most regular TBDD boolean_operations implementations.
   * Creates {@link TBDD}s from type {@link TBDDNode}.
   *
   * @author Stephan Holzner
   * @see BDDNode
   * @since 1.0
   */
  public static class Factory implements TBDD.Factory {

    /** The logical true leaf representation. */
    private TBDDNode one;

    /** The logical false leaf representation. */
    private TBDDNode zero;

    /** {@inheritDoc} */
    @Override
    public TBDDNode createNode(int var, DD low, DD high) {
      if (low instanceof TBDDEdge) {
        if (high instanceof TBDDEdge) {
          return new TBDDNode((TBDD) low, (TBDD) high, var);
        }
      }
      assert low instanceof TBDDNode;
      TBDD lowEdge = new TBDDEdge((TBDDNode) low, low.getVariable());
      TBDD highEdge = new TBDDEdge((TBDDNode) high, high.getVariable());
      return new TBDDNode(lowEdge, highEdge, var);
    }

    @Override
    public TBDDNode createTrue() {
      if (one == null) {
        one = new TBDDNode(null, null, -1);
      }
      return one;
    }

    @Override
    public TBDDNode createFalse() {
      if (zero == null) {
        zero = new TBDDNode(null, null, -2);
      }
      return zero;
    }

    /** the methods below are not used right now. */
    @Override
    public void setVariable(int variable, DD node) {
      // if (node instanceof BDDNode) {
      // ((TBDDNode) node).var = variable;
      // ((TBDDNode) node).rehash();
      // }
    }

    @Override
    public void setLow(DD pLow, DD pNode) {
      // TODO Auto-generated method stub

    }

    @Override
    public void setHigh(DD pHigh, DD pNode) {
      // TODO Auto-generated method stub

    }

    @Override
    public ChildNodeResolver<DD> getChildNodeResolver() {
      return new ChildNodeResolver<>() {
        @Override
        public DD getLow(DD root) {
          return root.getLow();
        }

        @Override
        public DD getHigh(DD root) {
          return root.getHigh();
        }

        @Override
        public TBDDNode cast(Object root) {
          return (TBDDNode) root;
        }
      };
    }
  }
}
