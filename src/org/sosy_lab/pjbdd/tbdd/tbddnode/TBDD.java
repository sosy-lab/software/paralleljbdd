// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddnode;

import org.sosy_lab.pjbdd.api.DD;

/**
 * Tagged bdd interface extending bdd interface to use tags methods.
 *
 * @author Simon Raths
 * @since 1.0
 */
public interface TBDD extends DD {

  int getLowTag();

  int getHighTag();

  int getTag();

  @Override
  TBDD getLow();

  @Override
  TBDD getHigh();

  TBDDNode getNode();

  /**
   * Helper Method to cast TBDD instances to TBDD, avoiding redundant cast checks.
   *
   * @param dd - the bdd
   * @return the casted tbdd
   */
  static TBDD unwrap(DD dd) {
    if (dd instanceof TBDD) {
      return (TBDD) dd;
    } else {
      throw new IllegalArgumentException(
          "Node of type " + dd.getClass().getSimpleName() + " is no tagged DD");
    }
  }

  /**
   * TaggedBDD factory interface defining all TBDD creation or modification methods. The only use
   * case to modify a TBDD is while reordering or node recycling in special cases.
   *
   * @author Stephan Holzner
   * @see TBDD
   * @since 1.0
   */
  interface Factory extends DD.Factory<DD> {

    @Override
    TBDDNode createNode(int var, DD low, DD high);
  }
}
