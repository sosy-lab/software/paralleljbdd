// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddcreator;

import java.math.BigInteger;
import java.util.Set;
import java.util.TreeSet;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.tbdd.TBDDNodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.util.IfThenElseData;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * {@link Creator} implementation with {@link TBDD} objects as TBDD data structure.
 *
 * @author Simon Raths
 * @see Creator
 * @since tba
 */
public class TBDDCreator extends TBDDAbstractCreator implements Creator {

  protected DDAlgorithm<TBDD> algorithm;

  /**
   * Creates new BTBDDCreator environment.
   *
   * @param nodeManager - the node manager to use
   * @param algorithm - the manipulation algorithm to use
   * @param satAlgorithm the sat algorithm to use
   */
  TBDDCreator(
      TBDDNodeManager nodeManager, DDAlgorithm<TBDD> algorithm, SatAlgorithm<TBDD> satAlgorithm) {
    super(nodeManager, satAlgorithm);
    this.algorithm = algorithm;
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeNot(DD f) {
    TBDD ff = TBDD.unwrap(f);
    return algorithm.makeNot(ff);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeAnd(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_AND);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeOr(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_OR);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeXor(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_XOR);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeNor(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_NOR);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeXnor(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_XNOR);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeNand(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_NAND);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeEqual(DD f1, DD f2) {
    return makeXnor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeUnequal(DD f1, DD f2) {
    return makeXor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeImply(DD f1, DD f2) {
    return makeOp(f1, f2, DDAlgorithm.ApplyOp.OP_IMP);
  }

  /**
   * Operation delegate method to algorithm implementation.
   *
   * @param f1 - first bTBDD argument
   * @param f2 - second bTBDD argument
   * @param op - operation to be performed
   * @return f1 op f2
   */
  private TBDD makeOp(DD f1, DD f2, DDAlgorithm.ApplyOp op) {
    reorderLock.readLock().lock();
    try {
      TBDD tbdd1 = TBDD.unwrap(f1);
      TBDD tbdd2 = TBDD.unwrap(f2);
      return algorithm.makeOp(tbdd1, tbdd2, op);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public TBDD anySat(DD tbdd) {
    reorderLock.readLock().lock();
    try {
      TBDD tbdd1 = TBDD.unwrap(tbdd);
      return satAlgorithm.anySat(tbdd1);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(DD b) {
    reorderLock.readLock().lock();
    try {
      TBDD tbdd = TBDD.unwrap(b);
      return satAlgorithm.satCount(tbdd);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public Stats getCreatorStats() {
    throw new UnsupportedOperationException();
  }

  /** {@inheritDoc} */
  @Override
  public IfThenElseData getIfThenElse(DD f) {
    return new IfThenElseData(makeIthVar(f.getVariable()), getHigh(f), getLow(f));
  }

  @Override
  public TBDD makeNode(DD low, DD high, int var) {
    TBDD tbddLow = TBDD.unwrap(low);
    TBDD tbddHigh = TBDD.unwrap(high);
    return nodeManager.makeNode(tbddLow, tbddHigh, var);
  }

  /** {@inheritDoc} */
  @Override
  public boolean entails(DD f1, DD f2) {
    boolean entails;
    reorderLock.readLock().lock();
    try {
      entails = entailsUnblocking(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
    return entails;
  }

  /**
   * Helper method for recursive entails operations to avoid recursive acquire of resize lock.
   *
   * @param dd1 - the first argument
   * @param dd2 - the second argument
   * @return true if f1 entails f2
   */
  private boolean entailsUnblocking(DD dd1, DD dd2) {
    TBDD f1 = TBDD.unwrap(dd1);
    TBDD f2 = TBDD.unwrap(dd2);
    if (f1.equals(f2)) {
      return true;
    }
    if (f1.isLeaf()) {
      return false;
    }
    if (level(f1) <= level(f2)) {
      return false;
    }
    return entailsUnblocking(f1.getLow(), f2) || entailsUnblocking(f1.getHigh(), f2);
  }

  private int level(TBDD node) {
    return nodeManager.level(node.getVariable());
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeExists(DD dd1, DD[] f2) {
    TBDD f1 = TBDD.unwrap(dd1);
    reorderLock.readLock().lock();
    try {

      DD fTemp = f2[0];
      for (DD f : f2) {
        fTemp = makeOp(fTemp, f, DDAlgorithm.ApplyOp.OP_AND);
      }
      Set<Integer> varSet = createHighVarLevelSet(fTemp);
      f1 = algorithm.makeExists(f1, IntArrayUtils.toIntArray(varSet));
    } finally {
      reorderLock.readLock().unlock();
    }
    return f1;
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeExists(DD dd1, DD f2) {
    TBDD f1 = TBDD.unwrap(dd1);
    reorderLock.readLock().lock();
    try {

      Set<Integer> varSet = createHighVarLevelSet(f2);
      f1 = algorithm.makeExists(f1, IntArrayUtils.toIntArray(varSet));
    } finally {
      reorderLock.readLock().unlock();
    }
    return f1;
  }

  @Override
  public TBDD makeCompose(DD dd1, int var, DD dd2) {
    reorderLock.readLock().lock();
    try {
      TBDD f1 = TBDD.unwrap(dd1);
      TBDD f2 = TBDD.unwrap(dd2);
      return algorithm.makeCompose(f1, var, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  @Override
  public TBDD makeReplace(DD f1, DD oldVar, DD replaceVar) {
    return makeExists(makeAnd(f1, makeXnor(oldVar, replaceVar)), oldVar);
  }

  /**
   * Takes a {@link DD} and creates a set of all high branch variables.
   *
   * @param dd - the {@link DD} argument
   * @return a set of variables
   */
  private Set<Integer> createHighVarLevelSet(DD dd) {
    TBDD f = TBDD.unwrap(dd);
    Set<Integer> varLevelSet = new TreeSet<>();
    while (!f.isLeaf()) {
      varLevelSet.add(level(f));
      f = f.getHigh();
    }
    return varLevelSet;
  }

  /** {@inheritDoc} */
  @Override
  public TBDD getLow(DD dd) {
    TBDD top = TBDD.unwrap(dd);
    return nodeManager.getLow(top);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD getHigh(DD dd) {
    TBDD top = TBDD.unwrap(dd);
    return nodeManager.getHigh(top);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeTrue() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeFalse() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeIte(DD dd1, DD dd2, DD dd3) {
    reorderLock.readLock().lock();
    try {
      TBDD f1 = TBDD.unwrap(dd1);
      TBDD f2 = TBDD.unwrap(dd2);
      TBDD f3 = TBDD.unwrap(dd3);
      return algorithm.makeIte(f1, f2, f3);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(IfThenElseData iteData) {
    return makeIte(iteData.getIf(), iteData.getThen(), iteData.getElse());
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeVariable() {
    return nodeManager.makeNext();
  }

  /** {@inheritDoc} */
  @Override
  public DD restrict(DD tbdd, int var, boolean restrictionVar) {
    return tbdd.getVariable() != var ? tbdd : restrictionVar ? getHigh(tbdd) : getLow(tbdd);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    super.shutDown();
    algorithm.shutdown();
  }

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public TBDDNodeManager getNodeManager() {
    return (TBDDNodeManager) this.nodeManager;
  }

  @Override
  public DD makeVariableBefore(DD pVar) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void cleanUnusedNodes() {
    // TODO Auto-generated method stub

  }
}
