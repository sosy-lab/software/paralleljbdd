// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// This file is part of PJBDD,

package org.sosy_lab.pjbdd.tbdd.tbddcreator;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.AbstractCreatorBuilder;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.CreatorBuilder;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.CASArrayCache;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.cache.GuavaCache;
import org.sosy_lab.pjbdd.tbdd.TBDDNodeManager;
import org.sosy_lab.pjbdd.tbdd.TBDDSat;
import org.sosy_lab.pjbdd.tbdd.tbddalgorithm.ITETBDDAlgorithm;
import org.sosy_lab.pjbdd.tbdd.tbddalgorithm.ParallelITETBDDAlgorithm;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.tbdd.tbdduniquetable.TBDDCompareAndSwapArray;
import org.sosy_lab.pjbdd.tbdd.tbdduniquetable.TBDDUniqueTable;
import org.sosy_lab.pjbdd.tbdd.tbdduniquetable.TBDDWeakHashMap;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;

/**
 * {@link CreatorBuilder} implementation with {@link TBDD} objects as TBDD data structure.
 *
 * @author Simon Raths
 * @see Creator
 * @since tba
 */
public class TBDDBuilder extends AbstractCreatorBuilder {

  /** use apply or ite algorithm. apply is not yet implemented for TBDDs */
  private final TBDD.Factory factory;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public TBDDBuilder(TBDD.Factory factory) {
    this.factory = factory;
  }

  /**
   * Creates a new BDDCreator environment with previously set parameters.
   *
   * @return a new BDD environment
   */
  @Override
  public Creator build() {

    TBDDNodeManager nodeManager = new TBDDNodeManager(makeTable());
    nodeManager.setVarCount(this.selectedVarCount);

    Cache<Integer, Cache.CacheData> cache = new CASArrayCache<>();
    Cache<TBDD, BigInteger> satCache = new GuavaCache<>();
    cache.init(this.selectedCacheSize, this.selectedParallelism);
    satCache.init(this.selectedCacheSize, this.selectedParallelism);

    DDAlgorithm<TBDD> algorithm = null;

    switch (parallelizationType) {
      case FORK_JOIN:
        algorithm = new ParallelITETBDDAlgorithm(cache, nodeManager, parallelismManager);
        break;
      default:
        algorithm = new ITETBDDAlgorithm(cache, nodeManager);
        break;
    }
    SatAlgorithm<TBDD> tbddSat = new TBDDSat(satCache, nodeManager);
    return new TBDDCreator(nodeManager, algorithm, tbddSat);
  }

  public TBDDBuilder setSelectedParallelism(int selectedParallelism) {
    this.selectedParallelism = selectedParallelism;
    return this;
  }

  public TBDDBuilder setSelectedCacheSize(int selectedCacheSize) {
    this.selectedCacheSize = selectedCacheSize;
    return this;
  }

  @Override
  public TBDDBuilder setUseApply(boolean apply) {
    this.useApply = apply;
    return this;
  }

  @Override
  public TBDDBuilder setTableType(Builders.TableType type) {
    this.tableType = type;
    return this;
  }

  @Override
  public TBDDBuilder setParallelismManager(ParallelismManager manager) {
    this.parallelismManager = manager;
    return this;
  }

  @Override
  public TBDDBuilder setParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  @Override
  public TBDDBuilder setTableSize(int tableSize) {
    this.selectedTableSize = tableSize;
    return this;
  }

  @Override
  public TBDDBuilder setCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  @Override
  public TBDDBuilder setThreads(int selectedThreads) {
    if (selectedThreads != this.selectedThreads) {
      parallelismManager = new ParallelismManagerImpl(selectedThreads);
    }
    this.selectedThreads = selectedThreads;
    if (selectedThreads > 1) {
      if (parallelismManager == null) {
        parallelismManager = new ParallelismManagerImpl(selectedThreads);
      }
    }
    return this;
  }

  @Override
  public TBDDBuilder setVarCount(int selectedVarCount) {
    this.selectedVarCount = selectedVarCount;
    return this;
  }

  @Override
  public TBDDBuilder setIncreaseFactor(int selectedIncreaseFactor) {
    this.selectedIncreaseFactor = selectedIncreaseFactor;
    return this;
  }

  @Override
  public TBDDBuilder setParallelizationType(Builders.ParallelizationType type) {
    this.parallelizationType = type;
    return this;
  }

  protected TBDDUniqueTable makeTable() {
    switch (tableType) {
      case CASArray:
        return new TBDDCompareAndSwapArray(selectedIncreaseFactor, selectedTableSize, factory);
      default:
        return new TBDDWeakHashMap(selectedTableSize, selectedParallelism);
    }
  }
}
