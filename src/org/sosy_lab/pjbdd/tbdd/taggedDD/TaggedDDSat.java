// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.taggedDD;

import com.google.common.collect.ImmutableSet;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;

public class TaggedDDSat implements SatAlgorithm<TBDD> {

  protected final Cache<TBDD, BigInteger> satCountCache;

  protected final NodeManager<TBDD> nodeManager;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public TaggedDDSat(Cache<TBDD, BigInteger> satCountCache, NodeManager<TBDD> nodeManager) {
    this.satCountCache = satCountCache;
    this.nodeManager = nodeManager;
  }

  @Override
  public TBDD anySat(TBDD tbdd) {
    if (tbdd.isLeaf()) {
      return tbdd;
    }
    if (tbdd.getLow().isFalse()) {
      return nodeManager.makeNode(
          nodeManager.getFalse(), anySat(tbdd.getHigh()), tbdd.getVariable());
    } else {
      return nodeManager.makeNode(
          anySat(tbdd.getLow()), nodeManager.getFalse(), tbdd.getVariable());
    }
  }

  @Override
  public BigInteger satCount(TBDD b) {
    Set<Integer> variables = new HashSet<>();
    varCountRec(b, variables);
    return satCountRec(b, variables.size(), ImmutableSet.of());
  }

  /**
   * Helper to recursive calculate number of variables for a given BDD.
   *
   * @param root - a DD
   * @param foundUniqueVariables - Set of already traversed Variables
   */
  protected void varCountRec(TBDD root, Set<Integer> foundUniqueVariables) {
    if (root.isFalse()) {
      if (root.getTag() >= 0) {
        foundUniqueVariables.add(root.getTag());
      }
      return;
    }
    if (root.isTrue()) {
      if (root.getTag() >= 0) {
        foundUniqueVariables.add(root.getTag());
      }
      return;
    }
    for (int i = root.getTag(); i <= root.getVariable(); i++) {
      foundUniqueVariables.add(i);
    }

    varCountRec(high(root), foundUniqueVariables);
    varCountRec(low(root), foundUniqueVariables);
  }

  /**
   * Recursive calculation of number of possible satisfying truth assignments for a given BDD.
   *
   * @param root - a BDD
   * @return root's number of possible satisfying truth assignments
   */
  protected BigInteger satCountRec(
      TBDD root, int variablesCount, Set<Integer> foundUniqueVariables) {
    if (root.isFalse()) {
      return BigInteger.ZERO;
    }
    if (root.isTrue()) {
      if (root.getTag() >= 0) {
        for (int i = root.getTag(); i < variablesCount; i++) {
          foundUniqueVariables.add(i);
        }
      }
      return BigInteger.TWO.pow(variablesCount - foundUniqueVariables.size());
    }

    Set<Integer> found = new HashSet<>(foundUniqueVariables);
    for (int i = root.getTag(); i <= root.getVariable(); i++) {
      found.add(i);
    }

    BigInteger size = BigInteger.ZERO;
    size = size.add(satCountRec(nodeManager.getHigh(root), variablesCount, found));
    size = size.add(satCountRec(nodeManager.getLow(root), variablesCount, found));

    return size;
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param tbdd - the bdd
   * @return level of bdd
   */
  @SuppressWarnings("unused")
  private int level(TBDD tbdd) {
    return nodeManager.level(tbdd.getVariable());
  }

  @SuppressWarnings("unused")
  private int levelLowTag(TBDD tbdd) {
    int temp = nodeManager.level(tbdd.getLowTag());
    if (temp == -3) {
      return level(nodeManager.getFalse());
    } else {
      return temp;
    }
  }

  @SuppressWarnings("unused")
  private int levelHighTag(TBDD tbdd) {
    int temp = nodeManager.level(tbdd.getHighTag());
    if (temp == -3) {
      return level(nodeManager.getFalse());
    } else {
      return temp;
    }
  }

  /**
   * helper call to determine bdd's high successor.
   *
   * @param tbdd - the bdd
   * @return high successor
   */
  protected TBDD high(TBDD tbdd) {
    return nodeManager.getHigh(tbdd);
  }

  /**
   * helper call to determine bdd's low successor.
   *
   * @param tbdd - the bdd
   * @return low successor
   */
  protected TBDD low(TBDD tbdd) {
    return nodeManager.getLow(tbdd);
  }
}
