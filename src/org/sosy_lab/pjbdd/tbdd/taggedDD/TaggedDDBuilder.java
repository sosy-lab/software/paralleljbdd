// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.taggedDD;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.AbstractCreatorBuilder;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.CASArrayCache;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.cache.GuavaCache;
import org.sosy_lab.pjbdd.tbdd.taggedDD.algorithm.ITETaggedDDAlgorithm;
import org.sosy_lab.pjbdd.tbdd.taggedDD.algorithm.ParallelITETaggedDDAlgorithm;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;

public class TaggedDDBuilder extends AbstractCreatorBuilder {

  private final TaggedDD.Factory factory;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public TaggedDDBuilder(TaggedDD.Factory factory) {
    this.factory = factory;
  }

  /**
   * Creates a new BDDCreator environment with previously set parameters.
   *
   * @return a new BDD environment
   */
  @Override
  public Creator build() {
    initParallelismManagerIfNeeded();

    TaggedDDNodeManager nodeManager = new TaggedDDNodeManager(makeTable());
    nodeManager.setVarCount(this.selectedVarCount);

    Cache<Integer, Cache.CacheData> cache = new CASArrayCache<>();
    Cache<TBDD, BigInteger> satCache = new GuavaCache<>();
    cache.init(this.selectedCacheSize, this.selectedParallelism);
    satCache.init(this.selectedCacheSize, this.selectedParallelism);

    DDAlgorithm<TBDD> algorithm;

    if (parallelizationType == Builders.ParallelizationType.FORK_JOIN) {
      algorithm = new ParallelITETaggedDDAlgorithm(cache, nodeManager, parallelismManager);
    } else {
      algorithm = new ITETaggedDDAlgorithm(cache, nodeManager);
    }
    SatAlgorithm<TBDD> taggedDDSat = new TaggedDDSat(satCache, nodeManager);
    return new TaggedDDCreator(nodeManager, algorithm, taggedDDSat);
  }

  @Override
  public TaggedDDBuilder setUseApply(boolean apply) {
    this.useApply = apply;
    return this;
  }

  @Override
  public TaggedDDBuilder setTableType(Builders.TableType type) {
    this.tableType = type;
    return this;
  }

  @Override
  public TaggedDDBuilder setParallelismManager(ParallelismManager manager) {
    this.parallelismManager = manager;
    return this;
  }

  @Override
  public TaggedDDBuilder setParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  @Override
  public TaggedDDBuilder setTableSize(int tableSize) {
    this.selectedTableSize = tableSize;
    return this;
  }

  @Override
  public TaggedDDBuilder setCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  @Override
  public TaggedDDBuilder setThreads(int selectedThreads) {
    if (selectedThreads != this.selectedThreads) {
      parallelismManager = new ParallelismManagerImpl(selectedThreads);
    }
    this.selectedThreads = selectedThreads;
    return this;
  }

  @Override
  public TaggedDDBuilder setVarCount(int selectedVarCount) {
    this.selectedVarCount = selectedVarCount;
    return this;
  }

  @Override
  public TaggedDDBuilder setIncreaseFactor(int selectedIncreaseFactor) {
    this.selectedIncreaseFactor = selectedIncreaseFactor;
    return this;
  }

  @Override
  public TaggedDDBuilder setParallelizationType(Builders.ParallelizationType type) {
    this.parallelizationType = type;
    return this;
  }

  protected TaggedDDUniqueTable makeTable() {

    if (tableType == Builders.TableType.CASArray) {
      return null; // new TBDDCompareAndSwapArray(selectedIncreaseFactor, selectedTableSize,
      // factory);
    }
    return new TaggedDDConcurrentWeakHashMap(selectedTableSize, selectedParallelism, factory);
  }
}
