// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2007-2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.taggedDD;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.uniquetable.DDConcurrentWeakHashMap;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.util.reference.ComparableWeakBDDReference;

public class TaggedDDConcurrentWeakHashMap extends DDConcurrentWeakHashMap<TBDD>
    implements TaggedDDUniqueTable {

  private final TaggedDD.Factory factory;
  private final ReferenceQueue<TBDD> referenceQueue;
  private final ConcurrentMap<ComparableWeakBDDReference<TBDD>, WeakReference<TBDD>> map;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public TaggedDDConcurrentWeakHashMap(
      int initialCapacity, int parallelism, DD.Factory<TBDD> factory) {
    super(initialCapacity, parallelism, factory);
    this.factory = (TaggedDD.Factory) factory;
    this.map = new ConcurrentHashMap<>(initialCapacity, 0.75f, parallelism);
    this.referenceQueue = new ReferenceQueue<>();
  }

  @Override
  public TBDD getOrCreate(TBDD low, TBDD high, int var, int tag) {
    TBDD lookUp = factory.createNode(var, low, high, tag);
    ComparableWeakBDDReference<TBDD> ref = new ComparableWeakBDDReference<>(lookUp, referenceQueue);

    // handle concurrency: BDD may be created with other task
    WeakReference<TBDD> refRes = map.putIfAbsent(ref, ref);
    if (refRes == null) {
      return lookUp;
    }
    // handle concurrency: other BDD may already be collected
    TBDD res = refRes.get();
    if (res != null) {
      return res;
    }
    return getOrCreate(low, high, var, tag);
  }
}
