// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.taggedDD;

import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDNode;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * TBDD equivalent to the BDDNode, using tagged edges as its children instead.
 *
 * @author Simon Raths
 * @since 1.0
 */
public class TaggedDD implements TBDD {

  private final int var;

  private final int tag;

  private final TaggedDD low;

  private final TaggedDD high;

  private int hashCode;

  public TaggedDD(TaggedDD low, TaggedDD high, int var, int tag) {
    this.var = var;
    this.low = low;
    this.tag = tag;
    this.high = high;

    this.rehash();
  }

  public TaggedDD(TBDD low, TBDD high, int var, int tag) {
    this.var = var;
    this.low = (TaggedDD) low;
    this.tag = tag;
    this.high = (TaggedDD) high;

    this.rehash();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTrue() {
    return var == -1;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFalse() {
    return var == -2;
  }

  /**
   * Checks for equal Nodes by first covering trivial cases and then comparing the edges coming from
   * that node. Edges need to be checked via their own equals method since they are not unique.
   *
   * <p>{@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {

    if (!(o instanceof TaggedDD)) {
      return false;
    }
    TaggedDD other = (TaggedDD) o;

    if (other.getTag() != this.getTag()) {
      return false;
    }
    if (other.isTrue() && isTrue()) {
      return true;
    }
    if (other.isFalse() && isFalse()) {
      return true;
    }
    if (other.getVariable() != getVariable()) {
      return false;
    }
    return getLow() == other.getLow() && getHigh() == other.getHigh();
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return hashCode;
  }

  /** Recalculate nodes hashcode after changes due to node recycling or bdd reordering. */
  protected void rehash() {
    hashCode =
        (var < 0)
            ? Math.abs(var)
            : HashCodeGenerator.generateHashCode(
                getVariable(), getLow().hashCode(), getHigh().hashCode(), getTag());
  }

  @Override
  public TaggedDD getLow() {
    return this.low;
  }

  @Override
  public TaggedDD getHigh() {
    return this.high;
  }

  @Override
  public int getVariable() {
    return this.var;
  }

  @Override
  public int getTag() {
    return this.tag;
  }

  @Override
  public int getLowTag() {
    return this.getLow().getTag();
  }

  @Override
  public int getHighTag() {
    return this.getHigh().getTag();
  }

  // Unused function but necessary for TBDD interface
  @Override
  public TBDDNode getNode() {
    // TODO Auto-generated method stub
    return null;
  }

  public static class Factory implements DD.Factory<TBDD> {

    /** The logical true leaf representation. */
    private TaggedDD one;

    /** The logical false leaf representation. */
    private TaggedDD zero;

    /** {@inheritDoc} */
    @Override
    public TaggedDD createNode(int var, TBDD low, TBDD high) {
      return new TaggedDD(low, high, var, var);
    }

    public TaggedDD createNode(int var, TBDD low, TBDD high, int tag) {
      return new TaggedDD(low, high, var, tag);
    }

    @Override
    public TaggedDD createTrue() {
      if (one == null) {
        one = new TaggedDD(null, null, -1, -3);
      }
      return one;
    }

    @Override
    public TaggedDD createFalse() {
      if (zero == null) {
        zero = new TaggedDD(null, null, -2, -3);
      }
      return zero;
    }

    /** the methods below are not used right now. */
    @Override
    public void setVariable(int variable, TBDD node) {
      // if (node instanceof BDDNode) {
      // ((TBDDNode) node).var = variable;
      // ((TBDDNode) node).rehash();
      // }
    }

    @Override
    public void setLow(TBDD pLow, TBDD pNode) {
      // TODO Auto-generated method stub

    }

    @Override
    public void setHigh(TBDD pHigh, TBDD pNode) {
      // TODO Auto-generated method stub

    }

    @Override
    public ChildNodeResolver<TBDD> getChildNodeResolver() {
      // TODO Auto-generated method stub
      return null;
    }
  }
}
