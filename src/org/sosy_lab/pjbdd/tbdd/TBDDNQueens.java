// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.Builders;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.examples.Example;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.util.parser.DotExporter;

/**
 * Implementation of the theN queens problem as {@link Example} which can be solved with bdd.
 *
 * @author Stephan Holzner
 * @see Example
 * @since 1.0
 */
public class TBDDNQueens implements Example {
  /** The boolean_operations used for bdd operations. */
  protected final Creator creator;

  /** number of queens aka problem size. */
  protected final int theN;

  /** {@link DD} matrix, representing chessboard. */
  protected DD[][] board;

  /** representing whole problem bdd. */
  protected DD queen;

  /**
   * Creates new {@link TBDDNQueens} instances with given parameter.
   *
   * @param n - the problem size
   * @param creator - the boolean_operations used for bdd operations
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public TBDDNQueens(int n, Creator creator) {
    this.creator = creator;
    this.theN = n;
  }

  /** {@inheritDoc} */
  @Override
  public void build() {
    queen = creator.makeTrue();
    board = new TBDD[theN][theN];
    DD zero = creator.makeFalse();

    for (int i = 0; i < theN; ++i) {
      for (int j = 0; j < theN; ++j) {
        board[i][j] = creator.makeIthVar((i * theN + j));
      }
    }
    for (int i = 0; i < theN; ++i) {
      DD f1 = zero;

      for (int j = 0; j < theN; ++j) {
        f1 = creator.makeOr(f1, board[i][j]);
      }
      queen = creator.makeAnd(queen, f1);
    }

    for (int i = 0; i < theN; ++i) {
      for (int j = 0; j < theN; ++j) {
        build(i, j);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger solve() {
    return creator.satCount(queen);
  }

  /** {@inheritDoc} */
  @Override
  public DD solution() {
    return queen;
  }

  public static void main(String[] args) {

    if (args.length == 0) {
      args = new String[] {"4", "5", "6", "7"};
    }

    for (Builders.TableType tableType :
        new Builders.TableType[] {Builders.TableType.ConcurrentHashMap}) {

      for (String str : args) {
        final int n = Integer.parseInt(str);

        Creator creator =
            Builders.newTBDDBuilder()
                .setTableType(tableType)
                .setVarCount(n * n)
                .setTableSize(100000)
                .setThreads(4)
                .setParallelizationType(Builders.ParallelizationType.FORK_JOIN)
                .setSelectedCacheSize(10000)
                .build();

        TBDDNQueens queens = new TBDDNQueens(n, creator);

        long start = System.currentTimeMillis();

        queens.build();
        BigInteger count = queens.solve();
        long end = System.currentTimeMillis();
        System.out.println(
            tableType
                + " "
                + "N="
                + n
                + ":\n SatCount: "
                + count
                + "\n Duration: "
                + (end - start)
                + "ms");

        System.out.println(new DotExporter().bddToString(queens.queen));

        queens.queen = null;
        queens.board = null;
        queens.close();
        System.gc();
      }
    }
  }

  /**
   * Build all rules for one field and append them to {@link #queen}.
   *
   * @param i - row index
   * @param j - column index
   */
  protected void build(int i, int j) {

    DD a = creator.makeTrue();
    DD b = creator.makeTrue();
    DD c = creator.makeTrue();
    DD d = creator.makeTrue();

    int k;
    for (k = 0; k < theN; ++k) {
      if (k != j) {
        // System.out.println(
        // "Combining: " + board[i][k].getVariable() + " and: " + board[i][j].getVariable());
        DD f5 = creator.makeNand(board[i][k], board[i][j]);
        a = creator.makeAnd(a, f5);
      }
    }

    for (k = 0; k < theN; ++k) {
      if (k != i) {
        DD f5 = creator.makeNand(board[i][j], board[k][j]);
        b = creator.makeAnd(b, f5);
      }
    }

    int n;
    for (k = 0; k < this.theN; ++k) {
      n = k - i + j;
      if (n >= 0 && n < this.theN && k != i) {
        DD f6 = creator.makeNand(board[i][j], board[k][n]);
        c = creator.makeAnd(c, f6);
      }
    }

    for (k = 0; k < this.theN; ++k) {
      n = i + j - k;
      if (n >= 0 && n < this.theN && k != i) {
        DD f6 = creator.makeNand(board[i][j], board[k][n]);
        d = creator.makeAnd(d, f6);
      }
    }

    c = creator.makeAnd(c, d);
    b = creator.makeAnd(b, c);
    a = creator.makeAnd(a, b);

    queen = creator.makeAnd(queen, a);
  }

  @Override
  public void close() {
    queen = null;
    board = null;
    creator.shutDown();
  }
}
