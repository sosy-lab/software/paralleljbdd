// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbdduniquetable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.api.DD;

public class CASWeakTBDDArray<V extends DD> {
  protected final VarHandle bddHandle;

  private WeakReference<?>[] table;

  protected final DD.ChildNodeResolver<DD> resolver;

  /** {@link ReferenceQueue} for weak references. */
  protected final ReferenceQueue<V> referenceQueue;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "CT_CONSTRUCTOR_THROW",
      justification = "intentional design")
  CASWeakTBDDArray(int size, DD.ChildNodeResolver<DD> pResolver) {
    bddHandle = MethodHandles.arrayElementVarHandle(WeakReference[].class);
    resolver = pResolver;
    table = new WeakReference<?>[size];
    referenceQueue = new ReferenceQueue<>();
  }

  DD getAtomic(int i) {
    if (table[i] == null) {
      return null;
    }
    return resolver.cast(((WeakReference<?>) bddHandle.get(table, i)).get());
  }

  DD get(int i) {
    if (table[i] == null) {
      return null;
    }
    return resolver.cast(table[i].get());
  }

  void set(int i, V value) {
    table[i] = new WeakReference<>(value, referenceQueue);
  }

  Stream<DD> toStream() {
    return Stream.of(table)
        .filter(Objects::nonNull)
        .map((ref) -> resolver.cast(ref.get()))
        .filter(Objects::nonNull);
  }

  void resize(int newSize) {
    WeakReference<?>[] newUniqueTable = new WeakReference<?>[newSize];
    System.arraycopy(table, 0, newUniqueTable, 0, table.length);
    table = newUniqueTable;
  }
}
