// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbdduniquetable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.uniquetable.UniqueTable;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDNode;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * {@link UniqueTable} implementation which uses backing arrays. Handles concurrent accesses with
 * compare and swap using {@link VarHandle}.
 *
 * @author Stephan Holzner
 * @see UniqueTable
 * @since 1.0
 */
public class TBDDCompareAndSwapArray implements TBDDUniqueTable {

  /**
   * threshold for resizing operations: if table contains more than 'ONLY_CLEAN_THRESHOLD's
   * percentage of recycled elements, there will be no resize but clean operation.
   */
  private static final double ONLY_CLEAN_THRESHOLD = 0.2;

  private static final VarHandle HASH_HANDLE = MethodHandles.arrayElementVarHandle(int[].class);
  private static final VarHandle COLLECTED_COUNTER_HANDLE;
  private static final VarHandle FREE_COUNTER_HANDLE;
  private static final VarHandle NEXT_FREE_HANDLE;

  /** count of garbage collected table entries. */
  protected int collectedCount = 0;

  /** underlying unique table. */
  protected CASWeakTBDDArray<DD> table;

  /** underlying hash table. */
  protected int[] hashTable;

  /** underlying next table. */
  protected int[] nextTable;

  /** logical one/true representation. */
  protected TBDDNode one;

  /** logical zero/false representation. */
  protected TBDDNode zero;

  /** factory for node creations. */
  protected final TBDD.Factory factory;

  /** resolver for generic child node handling. */
  protected final DD.ChildNodeResolver<DD> resolver;

  /** maximal array size. */
  protected int maxSize = Integer.MAX_VALUE;

  /** representation for next free allocated bdd node. */
  protected int nextFree = 0;

  /** number of free allocated bdd nodes. */
  protected int freeCounter = 0;

  /** number of allocated bdd nodes. */
  protected int nodeCount;

  /** resizing table's increase factor. */
  protected int increaseFactor;

  static {
    try {
      COLLECTED_COUNTER_HANDLE =
          MethodHandles.lookup()
              .findVarHandle(TBDDCompareAndSwapArray.class, "collectedCount", int.class);
      FREE_COUNTER_HANDLE =
          MethodHandles.lookup()
              .findVarHandle(TBDDCompareAndSwapArray.class, "freeCounter", int.class);
      NEXT_FREE_HANDLE =
          MethodHandles.lookup()
              .findVarHandle(TBDDCompareAndSwapArray.class, "nextFree", int.class);
    } catch (ReflectiveOperationException e) {
      throw new LinkageError("Unexpected reflection error", e);
    }
  }

  /**
   * Creates new {@link TBDDCompareAndSwapArray} instances with given parameters.
   *
   * @param increaseFactor - backing array's increase factor
   * @param initialSize - backing array's initial size
   * @param factory - BDDNode.Factory instance for node creations
   */
  public TBDDCompareAndSwapArray(int increaseFactor, int initialSize, TBDD.Factory factory) {

    // this.collectedCount = new AtomicInteger();
    this.factory = factory;
    this.resolver = factory.getChildNodeResolver();
    this.increaseFactor = increaseFactor;
    this.nodeCount = PrimeUtils.getGreaterNextPrime(initialSize);
    this.initIntern();
  }

  /** {@inheritDoc} */
  @Override
  public TBDDNode getOrCreateTBDD(TBDDNode tbdd) {
    TBDD low = tbdd.getLow();
    TBDD high = tbdd.getHigh();
    int var = tbdd.getVariable();
    return getOrCreateTBDD(low, high, var);
  }

  public TBDDNode getOrCreateTBDD(TBDD low, TBDD high, int var) {
    int head = getNodeWithHash(hashNode(var, low.hashCode(), high.hashCode()));
    TBDDNode bdd = get(low, high, var);
    if (bdd != null) {
      return bdd;
    }
    int res;
    res = getNextFree();
    if (res > 1) {
      atomicDecrementFreeCounter();
      return createAtomic(low, high, var, res, head);
    } else {
      tryResizing();
    }

    return getOrCreateTBDD(low, high, var);
  }

  /**
   * Lookup a {@link DD} node with specified parameters.
   *
   * @param low - the nodes low branch
   * @param high - the nodes high branch
   * @param var - the nodes bdd variable
   * @return the matching node or <code>null</code> else
   */
  protected TBDDNode get(TBDD low, TBDD high, int var) {
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    int res = getNodeWithHash(hash);

    while (res != 0) {
      if (table.get(res) == null) {
        res = getNext(res);
        continue;
      }
      TBDDNode tbdd = (TBDDNode) table.getAtomic(res);
      if (tbdd == null) {
        res = getNext(res);
        continue;
      }
      if (tbdd.equalsTo(var, low, high)) {
        return tbdd;
      }

      res = getNext(res);
    }
    return null;
  }

  /**
   * Creates a new {@link DD} with given parameters and corresponding table entries. Method do not
   * handle concurrent accesses!
   *
   * @param low - the given low branch
   * @param high - the given high branch
   * @param var - the given bdd variable
   * @param res - the new nodes array index
   * @param head - first checked node in hash bin
   * @return the created {@link DD}
   */
  protected TBDDNode createAtomic(TBDD low, TBDD high, int var, int res, int head) {
    atomicDecrementFreeCounter();
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    if (!setHashAtomic(hash, res, head)) {
      // hash bin changed recheck for existing node
      return getOrCreateTBDD(low, high, var);
    }

    setNext(res, head);
    TBDDNode result = factory.createNode(var, low, high);
    table.set(res, result);
    return result;
  }

  protected boolean setHashAtomic(int hash, int res, int old) {
    // this function needs to be atomic for same hash
    return HASH_HANDLE.weakCompareAndSetPlain(hashTable, hash, old, res);
  }

  private final Lock resizeMonitor = new ReentrantLock();

  /** Try to resize table. Wait if resize op is already in progress. Collect all locks else. */
  protected void tryResizing() {
    if (checkResize()) {
      resizeMonitor.lock();
      try {
        if (checkResize()) { // resize performed
          resizeTable();
        }
      } finally {
        resizeMonitor.unlock();
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<? super DD> tbddConsumer) {
    table.toStream().forEach(tbddConsumer);
  }

  private void atomicDecrementFreeCounter() {
    freeCounter = (int) FREE_COUNTER_HANDLE.getAndAdd(this, -1);
  }

  private int atomicReadCollectedCounter() {
    return (int) COLLECTED_COUNTER_HANDLE.get(this);
  }

  private void atomicResetCollectedCounter() {
    COLLECTED_COUNTER_HANDLE.set(this, 0);
  }

  private void atomicSetFreeCounter(int value) {
    FREE_COUNTER_HANDLE.set(this, value);
  }

  private boolean atomicSetNextFree(int value, int old) {
    return NEXT_FREE_HANDLE.weakCompareAndSetPlain(this, old, value);
  }

  protected int getNextFree() {
    int next = (int) NEXT_FREE_HANDLE.get(this);
    if (atomicSetNextFree(getNext(next), next)) {
      return next;
    }
    return getNextFree();
  }

  protected boolean checkResize() {
    return nextFree <= 1;
  }

  /** Initialize unique table and it's backing arrays. */
  protected void initIntern() {
    one = (TBDDNode) factory.createTrue();
    zero = (TBDDNode) factory.createFalse();
    table = new CASWeakTBDDArray<>(nodeCount, factory.getChildNodeResolver());
    nextTable = new int[this.nodeCount];
    hashTable = new int[this.nodeCount];

    for (int n = 0; n < this.nodeCount; n++) {
      setNext(n, n + 1);
    }
    setNext(this.nodeCount - 1, 0);
    nextFree = 2;
    atomicSetFreeCounter(this.nodeCount - 2);
  }

  /**
   * Set index's next node with equal hash code.
   *
   * @param index - the root node's index
   * @param next - the next node's index with equal hash code
   */
  protected void setNext(int index, int next) {
    if (index != next) {
      nextTable[index] = next;
    }
  }

  protected int getNext(int index) {
    return nextTable[index];
  }

  protected void resizeTable() {
    int oldSize = nodeCount;
    int newSize = nodeCount;

    if (atomicReadCollectedCounter() >= nodeCount * ONLY_CLEAN_THRESHOLD) {
      doResize(oldSize, newSize);
    } else {
      if (increaseFactor > 0) {
        newSize += newSize * increaseFactor;
      } else {
        newSize = newSize << 1;
      }

      if (newSize > maxSize) {
        newSize = maxSize;
      }
      doResize(oldSize, newSize);
    }
    atomicResetCollectedCounter();
  }

  /**
   * Get the index of the {@link DD} with a given hashcode.
   *
   * @param hash - the given hashcode
   * @return node's index with given hashcode
   */
  protected int getNodeWithHash(int hash) {
    return hashTable[hash];
  }

  /**
   * Set the index of the {@link DD} with a given hashcode.
   *
   * @param hash - the node's hashcode
   * @param index - the node's index
   */
  protected void setHash(int hash, int index) {
    hashTable[hash] = index;
  }

  /**
   * Calculates hashcode for given input triple.
   *
   * @param lvl - a node's level
   * @param low - a node's low branch
   * @param high - a node's high branch
   * @return hashcode for given input triple
   */
  protected int hashNode(int lvl, int low, int high) {
    return Math.abs(HashCodeGenerator.generateHashCode(lvl, low, high) % nodeCount);
  }

  protected void doResize(int oldSize, int newSize) {

    newSize = PrimeUtils.getLowerNextPrime(newSize);

    if (oldSize < newSize) {

      int[] newHashTable = new int[newSize];
      int[] newNextTable = new int[newSize];

      System.arraycopy(hashTable, 0, newHashTable, 0, hashTable.length);
      System.arraycopy(nextTable, 0, newNextTable, 0, nextTable.length);
      table.resize(newSize);
      nextTable = newNextTable;
      hashTable = newHashTable;

      nodeCount = newSize;
    }

    for (int n = 0; n < oldSize; n++) {
      setHash(n, 0);
    }

    for (int n = oldSize; n < nodeCount; n++) {
      setNext(n, n + 1);
    }
    setNext(nodeCount - 1, nextFree);
    nextFree = oldSize;
    freeCounter = nodeCount - oldSize;
    rehash();
  }

  /** Rehash table entries after resize task. (Hash value changes with table size). */
  private void rehash() {
    nextFree = 0;
    freeCounter = 0;

    for (int n = nodeCount - 1; n >= 2; n--) {
      if (table.get(n) != null) {
        TBDDNode tbdd = get(n);
        if (tbdd != null) {
          int hash =
              hashNode(tbdd.getVariable(), tbdd.getLow().hashCode(), tbdd.getHigh().hashCode());
          setNext(n, getNodeWithHash(hash));
          setHash(hash, n);
          continue;
        }
      }
      table.set(n, null);
      setNext(n, nextFree);
      nextFree = n;
      freeCounter--;
    }
  }

  private TBDDNode get(int index) {
    return (TBDDNode) table.get(index);
  }

  /** {@inheritDoc} */
  @Override
  public TBDDNode getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public TBDDNode getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    clear();
  }

  /** {@inheritDoc} */
  @Override
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public TBDD.Factory getFactory() {
    return factory;
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    table = null;
    hashTable = null;
    nextTable = null;
  }
}
