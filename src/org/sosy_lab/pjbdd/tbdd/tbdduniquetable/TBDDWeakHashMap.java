// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbdduniquetable;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.BDDNode;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDEdge;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDNode;
import org.sosy_lab.pjbdd.util.reference.ComparableWeakBDDReference;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

public class TBDDWeakHashMap implements TBDDUniqueTable {

  private final TBDD.Factory factory;

  /** the backing concurrent map with weak keys and values. */
  private final ConcurrentMap<ComparableWeakBDDReference<DD>, WeakReference<DD>> map;

  /** reference queue to collect reclaimed nodes. */
  private final ReferenceQueue<DD> referenceQueue;

  /** logical zero/false representation. */
  private final TBDDNode zero;

  /** logical one/true representation. */
  private final TBDDNode one;

  /** demon thread to cleanup reclaimed entries. */
  private final CleanUpThread cleaner;

  public TBDDWeakHashMap(int initialCapacity, int parallelism, TBDD.Factory factory) {
    this.factory = factory;
    this.map = new ConcurrentHashMap<>(initialCapacity, 0.75f, parallelism);
    this.referenceQueue = new ReferenceQueue<>();
    this.cleaner = new CleanUpThread();
    this.zero = (TBDDNode) factory.createFalse();
    this.one = (TBDDNode) factory.createTrue();
    this.startCleaner();
  }

  /**
   * Creates new {@link TBDDWeakHashMap} instances with given parameters.
   *
   * @param initialCapacity - backing map's initial size
   * @param parallelism - backing map's concurrency factor
   */
  public TBDDWeakHashMap(int initialCapacity, int parallelism) {
    this(initialCapacity, parallelism, new Factory());
  }

  public TBDDNode getOrCreate(DD low, DD high, int var) {
    TBDDNode lookUp = factory.createNode(var, low, high);
    ComparableWeakBDDReference<DD> ref = new ComparableWeakBDDReference<>(lookUp, referenceQueue);
    if (map.containsKey(ref)) {
      TBDDNode res = (TBDDNode) map.get(ref).get();
      if (factory instanceof Factory) {
        ((Factory) factory).addToPool(lookUp);
      }
      return res;
    }
    if (lookUp.isLeaf()) {
      return lookUp;
    }
    // handle concurrency: BDD may be created with other task
    WeakReference<DD> refRes = map.putIfAbsent(ref, ref);
    if (refRes == null) {
      return lookUp;
    }
    // handle concurrency: other BDD may already be collected
    TBDDNode res = (TBDDNode) refRes.get();
    if (res != null) {
      return res;
    }
    return getOrCreate(low, high, var);
  }

  @Override
  public TBDDNode getOrCreateTBDD(TBDDNode tbdd) {
    ComparableWeakBDDReference<DD> ref = new ComparableWeakBDDReference<>(tbdd, referenceQueue);
    if (map.containsKey(ref)) {
      TBDDNode res = (TBDDNode) map.get(ref).get();
      if (factory instanceof Factory) {
        ((Factory) factory).addToPool(tbdd);
      }
      return res;
    }
    if (tbdd.isLeaf()) {
      return tbdd;
    }
    // handle concurrency: BDD may be created with other task
    WeakReference<DD> refRes = map.putIfAbsent(ref, ref);
    if (refRes == null) {
      return tbdd;
    }
    // handle concurrency: other BDD may already be collected
    TBDDNode res = (TBDDNode) refRes.get();
    if (res != null) {
      return res;
    }
    return getOrCreateTBDD(tbdd);
  }

  private final class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      Reference<? extends DD> sv = referenceQueue.remove(1000);
      if (sv != null) {
        if (sv instanceof ComparableWeakBDDReference) {
          map.remove(sv);
        }
      }
    }
  }

  /** Start cleanup thread. */
  private void startCleaner() {
    cleaner.start();
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<? super DD> ddConsumer) {
    map.values().stream().map(Reference::get).filter(Objects::nonNull).forEach(ddConsumer);
  }

  /** {@inheritDoc} */
  @Override
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public DD.Factory<DD> getFactory() {
    return factory;
  }

  /** {@inheritDoc} */
  @Override
  public TBDDNode getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public TBDDNode getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    cleaner.shutdown();
    clear();
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    map.clear();
  }

  /**
   * BDDNode.Factory sub class which tries to recycle {@link RecyclingNodePool}s {@link BDDNode}
   * objects before new creation.
   *
   * @author Stephan Holzner
   * @see BDDNode
   * @see RecyclingNodePool
   * @since 1.0
   */
  public static class Factory extends TBDDEdge.Factory {

    /** recycling pool for object reuse. */
    private final RecyclingNodePool pool = new RecyclingNodePool();

    /** {@inheritDoc} */
    @Override
    public TBDDNode createNode(int var, DD low, DD high) {

      TBDDNode node = pool.restore();
      if (node != null) {
        setHigh(high, node);
        setLow(low, node);
        setVariable(var, node);
        return node;
      }

      return super.createNode(var, low, high);
    }

    /**
     * Add node to recycling pool.
     *
     * @param tbdd - node to append
     */
    public void addToPool(TBDDNode tbdd) {
      pool.add(tbdd);
    }

    /**
     * Node pool to recycle for look up only instantiated {@link TBDD} objects.
     *
     * @author Stephan Holzner
     * @since 1.0
     */
    private static final class RecyclingNodePool {
      /** {@link ConcurrentLinkedDeque} as backing collection. */
      private final ConcurrentLinkedDeque<TBDDNode> pool = new ConcurrentLinkedDeque<>();

      /** maximal pool size. */
      private static final int MAX_LENGTH = 100;

      /**
       * Add bdd to pool if pool.size() <= {@link #MAX_LENGTH}.
       *
       * @param tbdd - to be recycled
       */
      private void add(TBDDNode tbdd) {
        if (pool.size() <= MAX_LENGTH) {
          pool.add(tbdd);
        }
      }

      /**
       * Recycle unused node.
       *
       * @return unused node
       */
      private TBDDNode restore() {
        return pool.poll();
      }
    }
  }
}
