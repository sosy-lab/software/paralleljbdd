// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd;

import com.google.common.primitives.Ints;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDEdge;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDNode;
import org.sosy_lab.pjbdd.tbdd.tbdduniquetable.TBDDUniqueTable;
import org.sosy_lab.pjbdd.util.reference.ComparableWeakBDDReference;

/**
 * Main and simple {@link NodeManager} implementation for TBDD creations.
 *
 * @author Simon Raths
 * @see NodeManager
 * @since tba
 */
public class TBDDNodeManager implements NodeManager<TBDD> {

  protected final TBDDUniqueTable uniqueTable;

  /** number of defined TBDD variables. */
  protected int varCount;

  /** Backing arrays for storing level and variable relation. */
  protected int[] levelVar;

  protected int[] varLevel;

  private final ConcurrentMap<ComparableWeakBDDReference<DD>, WeakReference<DD>> edgeMap;

  private final ReferenceQueue<DD> referenceQueue;

  /**
   * Set of defined {@link TBDD} variables. Prevents variable deletion from unique table (due to
   * {@link java.lang.ref.WeakReference}).
   */
  protected Set<TBDD> varSet;

  /**
   * Creates new node manager instance.
   *
   * @param uniqueTable - the uniquetable to use
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public TBDDNodeManager(TBDDUniqueTable uniqueTable) {
    this.uniqueTable = uniqueTable;
    this.edgeMap = new ConcurrentHashMap<>(10000, 0.75f, 100);
    this.referenceQueue = new ReferenceQueue<>();
  }

  /**
   * {@inheritDoc} Creates an auxiliary TBDD from the given attributes to hand over to TBDDmakeNode
   */
  @Override
  public TBDD makeNode(TBDD low, TBDD high, int var) {

    if (!checkLvl(var)) {
      setVarCount(var + 1);
    }

    low = TBDD.unwrap(low);
    high = TBDD.unwrap(high);

    int next = (var >= 0) ? var : getNext(level(var));

    return makeTBDDNode(var, low, high, next);
  }

  /**
   * Alternative function call for makeNode with additional arguments "lowtag" and "hightag" already
   * given. Currently unused.
   */
  public TBDD makeNode(TBDD low, TBDD high, int var, int next) {

    if (!checkLvl(var)) {
      setVarCount(var + 1);
    }
    low = TBDD.unwrap(low);
    high = TBDD.unwrap(high);

    return makeTBDDNode(var, low, high, next);
  }

  /**
   * Main function for the TBDD reductions.
   *
   * @param var - the variable of the node
   * @param low - the low child
   * @param high - the high child
   * @param next - the next variable after var
   * @return tbddEdge with the node and the appropriate tag
   */
  public TBDD makeTBDDNode(int var, TBDD low, TBDD high, int next) {

    TBDDNode node;
    // First reduction rule:

    if (high.equals(low)) {
      return low;
    } else if (high.getNode().equals(getFalseNode())) {
      // Second reduction rule:

      // Checks if a reduction took place on the child of the TBDD to ensure proper tagging.

      if (low.getTag() == next) {
        low = new TBDDEdge(low.getNode(), var);
        return low;
      } else {
        node = uniqueTable.getOrCreateTBDD(new TBDDNode(low, high, next));
      }

    } else {
      // No reduction applied.

      node = uniqueTable.getOrCreateTBDD(new TBDDNode(low, high, var));
    }

    return new TBDDEdge(node, var);
  }

  @Override
  public int getNext(int lvl) {

    if (lvl >= levelVar.length) {
      throw new IndexOutOfBoundsException("no such variable");
    }
    if (lvl + 1 == levelVar.length) {
      return -3;
    }

    return levelVar[lvl + 1];
  }

  /** {@inheritDoc} */
  @Override
  public int var(int level) {
    return levelVar[level];
  }

  /** {@inheritDoc} */
  @Override
  public int setVarCount(int count) {
    if (count < 1 || count <= varCount) {
      return varCount;
    }
    int oldCount = varCount;
    varCount = count;

    if (varSet == null) {
      varSet = Collections.synchronizedSet(new HashSet<>());
      levelVar = new int[varCount];
      varLevel = new int[varCount];
    } else {
      int[] newLevelVar = new int[varCount];
      int[] newVarLevel = new int[varCount];
      System.arraycopy(levelVar, 0, newLevelVar, 0, levelVar.length);
      System.arraycopy(varLevel, 0, newVarLevel, 0, levelVar.length);
      levelVar = newLevelVar;
      varLevel = newVarLevel;
    }

    for (int i = oldCount; i < count; i++) {
      makeVariable(i);
    }
    return varCount;
  }

  /** {@inheritDoc} */
  @Override
  public int level(int variable) {
    return (variable < 0) ? varCount : varLevel[variable];
  }

  /** {@inheritDoc} */
  @Override
  public int getVarCount() {
    return varCount;
  }

  public int[] getLevelVar() {
    return levelVar.clone();
  }

  /** {@inheritDoc} */
  @Override
  public int[] getCurrentOrdering() {
    return Arrays.copyOf(levelVar, levelVar.length);
  }

  /** {@inheritDoc} */
  @Override
  public boolean checkLvl(int level) {
    return (level < varCount);
  }

  /** {@inheritDoc} */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    // Fill up existing order

    int max = Collections.max(pOrder);
    if (!checkLvl(max)) {
      setVarCount(max);
    }
    // actually reorder
    IntStream.range(0, pOrder.size())
        .forEach(
            i -> {
              if (!Objects.equals(pOrder.get(i), levelVar[i])) {
                swapVarToLevel(pOrder.get(i), i);
              }
            });
  }

  /** {@inheritDoc} */
  @Override
  public int topVar(int[] levels) {
    return var(Ints.min(levels));
  }

  /** {@inheritDoc} */
  @Override
  public TBDD getFalse() {
    return new TBDDEdge(uniqueTable.getFalse(), -3);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD getTrue() {
    return new TBDDEdge(uniqueTable.getTrue(), -3);
  }

  public TBDDNode getFalseNode() {
    return uniqueTable.getFalse();
  }

  public TBDDNode getTrueNode() {
    return uniqueTable.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    varLevel = null;
    levelVar = null;
    varSet.clear();
    uniqueTable.shutDown();
  }

  /**
   * Swaps a specific variable up or down to a certain level.
   *
   * @param var - the variable to be swapped
   * @param level - the target level
   */
  private void swapVarToLevel(int var, int level) {
    int levelOfVar = varLevel[var];
    while (levelOfVar != level) {
      if (levelOfVar < level) {
        swapLevel(levelOfVar + 1, levelOfVar++);
      } else {
        swapLevel(levelOfVar, --levelOfVar);
      }
    }
  }

  /**
   * Swaps variables by given variable levels.
   *
   * @param levelA - getIf variable's level
   * @param levelB - getThen variable's level
   */
  private void swapLevel(int levelA, int levelB) {
    int varA = levelVar[levelA];
    int varB = levelVar[levelB];
    varLevel[varA] = levelB;
    varLevel[varB] = levelA;
    levelVar[levelB] = varA;
    levelVar[levelA] = varB;
    // uniqueTable.swap(varA, varB, this::makeNode);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeNext() {
    return makeVariable(varCount + 1);
  }

  /**
   * Actually creates the variable and its negation as {@link TBDD}.
   *
   * @param var - the variable to be created
   * @return the created variable as {@link TBDD}
   */
  private TBDD makeVariable(int var) {
    // System.out.println("makeVariable called with " + var);
    TBDD posVar = makeNode(getFalse(), getTrue(), var);
    varSet.add(posVar);
    levelVar[var] = var;
    varLevel[var] = var;

    // BDD negVar = makeNode(getTrue(), getFalse(), var);
    // varSet.add(negVar);
    return posVar;
  }

  @Override
  public TBDD getHigh(TBDD pBdd) {
    return pBdd.getHigh();
  }

  @Override
  public TBDD getLow(TBDD pBdd) {
    return pBdd.getLow();
  }

  public TBDD foaEdge(TBDDNode node, int tag) {
    TBDD lookUp = new TBDDEdge(node, tag);
    ComparableWeakBDDReference<DD> ref = new ComparableWeakBDDReference<>(lookUp, referenceQueue);
    if (edgeMap.containsKey(ref)) {
      return (TBDD) edgeMap.get(ref).get();
    }
    if (lookUp.isLeaf()) {
      return lookUp;
    }
    // handle concurrency: BDD may be created with other task
    WeakReference<DD> refRes = edgeMap.putIfAbsent(ref, ref);
    if (refRes == null) {
      return lookUp;
    }
    // handle concurrency: other BDD may already be collected
    TBDD res = (TBDD) refRes.get();
    if (res != null) {
      return res;
    }
    return foaEdge(node, tag);
  }

  @Override
  public int getNodeCount() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getUniqueTableSize() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public TBDD makeVariableBefore(TBDD pVar) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void cleanUnusedNodes() {
    // TODO Auto-generated method stub

  }
}
