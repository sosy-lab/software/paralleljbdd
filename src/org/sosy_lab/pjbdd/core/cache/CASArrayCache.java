// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.cache;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Simple {@link Cache} implementation with backing Array. Uses {@link VarHandle}'s methods for
 * atomic compare and swap operations.
 *
 * @author Stephan Holzner
 * @see Cache
 * @since 1.0
 */
public class CASArrayCache<V extends DD> implements Cache<Integer, Cache.CacheData> {

  /** the backing array. */
  private CacheData[] cache;

  /** atomicAccess mechanism. */
  private final VarHandle arrayHandle;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "CT_CONSTRUCTOR_THROW",
      justification = "intentional design")
  public CASArrayCache() {
    arrayHandle = MethodHandles.arrayElementVarHandle(CacheData[].class);
  }

  /** {@inheritDoc} */
  @Override
  public void init(int cacheSize, int parallelism) {
    cache = new CacheData[cacheSize];
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    IntStream.range(0, cache.length).forEach(i -> cache[i] = null);
  }

  /** {@inheritDoc} */
  @Override
  public void put(Integer key, CacheData value) {
    int index = Math.abs(key % cache.length);
    arrayHandle.set(cache, index, value);
  }

  /** {@inheritDoc} */
  @Override
  public CacheData get(Integer key) {
    int index = Math.abs(key % cache.length);
    return (CacheData) arrayHandle.get(cache, index);
  }

  @Override
  public Cache<Integer, CacheData> cleanCopy() {
    CASArrayCache<V> copy = new CASArrayCache<>();
    copy.init(cache.length, 0);
    return copy;
  }

  @Override
  public int nodeCount() {
    return (int) Arrays.stream(cache).filter(Objects::nonNull).count();
  }

  @Override
  public int size() {
    return cache.length;
  }
}
