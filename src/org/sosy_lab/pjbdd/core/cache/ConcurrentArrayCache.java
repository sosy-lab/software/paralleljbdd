// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.cache;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Simple {@link Cache} implementation with backing Array}.
 *
 * @author Stephan Holzner
 * @see Cache
 * @since 1.0
 */
public class ConcurrentArrayCache<V extends DD> implements Cache<Integer, Cache.CacheData> {

  /** the backing array. */
  private Cache.CacheData[] cache;

  /** synchronization mechanism. */
  private Lock[] locks;

  /** {@inheritDoc} */
  @Override
  public void init(int cacheSize, int parallelism) {
    cache = new Cache.CacheData[cacheSize];
    locks = new ReentrantLock[parallelism];
    for (int i = 0; i < parallelism; i++) {
      locks[i] = new ReentrantLock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    IntStream.range(0, cache.length).forEach(i -> cache[i] = null);
  }

  /** {@inheritDoc} */
  @Override
  public void put(Integer key, Cache.CacheData value) {
    int index = Math.abs(key % cache.length);
    Lock lock = locks[index % locks.length];
    lock.lock();
    try {
      cache[index] = value;
    } finally {
      lock.unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public Cache.CacheData get(Integer key) {
    int index = Math.abs(key % cache.length);
    Lock lock = locks[index % locks.length];
    lock.lock();
    try {
      return cache[index];
    } finally {
      lock.unlock();
    }
  }

  @Override
  public Cache<Integer, CacheData> cleanCopy() {
    ConcurrentArrayCache<V> copy = new ConcurrentArrayCache<>();
    copy.init(cache.length, locks.length);
    return copy;
  }

  @Override
  public int nodeCount() {
    return (int) Arrays.stream(cache).filter(Objects::nonNull).count();
  }

  @Override
  public int size() {
    return cache.length;
  }
}
