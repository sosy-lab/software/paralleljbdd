// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.algorithm;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Abstract base class for {@link Algorithm} implementations.
 *
 * @author Stephan Holzner
 * @see Algorithm
 * @since 1.1
 */
public abstract class ManipulatingAlgorithm<V extends DD> implements Algorithm<V> {

  /** the node creating and handling instance. */
  protected final NodeManager<V> nodeManager;

  /** the computation cache. */
  protected final Cache<Integer, Cache.CacheData> computedTable;

  /**
   * Constructor for initializing variables.
   *
   * @param computedTable - the computation cache to be used
   * @param nodeManager - the node manager to be used
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public ManipulatingAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager<V> nodeManager) {
    this.nodeManager = nodeManager;
    this.computedTable = computedTable;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    nodeManager.shutdown();
    computedTable.clear();
  }

  /** {@inheritDoc} */
  @Override
  public V makeFalse() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public V makeTrue() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public V makeNode(V low, V high, int var) {
    if (!nodeManager.checkLvl(var)) {
      nodeManager.setVarCount(var + 1);
    }
    return nodeManager.makeNode(low, high, var);
  }

  /** {@inheritDoc} */
  @Override
  public V restrict(V bdd, int var, boolean restrictionVar) {
    return bdd.getVariable() != var
        ? bdd
        : restrictionVar ? nodeManager.getHigh(bdd) : nodeManager.getLow(bdd);
  }

  /**
   * Restricts given BDD to it's low child if it's level equal to given level.
   *
   * @param f - given BDD
   * @param topVar - given level
   * @return low or f
   */
  protected V low(V f, int topVar) {
    return restrict(f, topVar, false);
  }

  /**
   * Restricts given BDD to it's high child if it's level equal to given level.
   *
   * @param f - given BDD
   * @param topVar - given level
   * @return high or f
   */
  protected V high(V f, int topVar) {
    return restrict(f, topVar, true);
  }

  /**
   * find a various number of levels' topmost level variable.
   *
   * @param levels - variable amount of levels
   * @return topmost variable
   */
  protected int topVar(int... levels) {
    return nodeManager.topVar(levels);
  }

  /**
   * Get a bdd node's variable level.
   *
   * @param bdd - a bdd node
   * @return nodes's variable level
   */
  protected int level(V bdd) {
    return nodeManager.level(bdd.getVariable());
  }

  /**
   * check if 'ITE' input triple already computed in cache.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return Optional of cached item or Optional empty
   */
  protected Optional<V> checkITECache(V f1, V f2, V f3) {
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), f3.hashCode());
    Cache.CacheData data = computedTable.get(hash);

    if (data instanceof Cache.CacheDataITE) {
      @SuppressWarnings("unchecked")
      Cache.CacheDataITE<V> d = (Cache.CacheDataITE<V>) data;
      if (d.getF1().equals(f1) && d.getF2().equals(f2) && d.getF3().equals(f3)) {
        return Optional.of(d.getRes());
      }
    }
    return Optional.empty();
  }

  /**
   * Store item for 'ITE' input triple in computation cache.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @param item - output node
   */
  protected void cacheItem(V f1, V f2, V f3, V item) {
    Cache.CacheDataITE<V> data = new Cache.CacheDataITE<>();
    data.setF1(f1);
    data.setF2(f2);
    data.setF3(f3);
    data.setRes(item);
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), f3.hashCode());
    cache(computedTable, data, hash);
  }

  /**
   * Store data in given computation cash for given hash.
   *
   * @param cache - given cash
   * @param data - to write
   * @param hash - data's input hash
   */
  protected void cache(Cache<Integer, Cache.CacheData> cache, Cache.CacheData data, int hash) {
    cache.put(hash, data);
  }

  /**
   * Store calculated apply operation with input tuple in cache.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param op - applied operator
   * @param item - output {@link DD} argument
   */
  protected void cacheBinaryItem(V f1, V f2, int op, V item) {
    Cache.CacheDataBinaryOp<V> data = new Cache.CacheDataBinaryOp<>();
    data.setF1(f1);
    data.setF2(f2);
    data.setOp(op);
    data.setRes(item);
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), op);
    cacheEntry(data, hash);
  }

  /**
   * Store calculated operation with input in cache.
   *
   * @param f - input {@link DD} argument
   * @param item - output {@link DD} argument
   */
  protected void cacheUnaryItem(V f, V item) {
    Cache.CacheDataNot<V> data = new Cache.CacheDataNot<>();
    data.setF(f);
    data.setRes(item);
    int hash = f.hashCode();
    cacheEntry(data, hash);
  }

  /**
   * Actually write data in computation cache.
   *
   * @param data - to be saved
   * @param hash - data's hash value
   */
  private void cacheEntry(Cache.CacheData data, int hash) {
    computedTable.put(hash, data);
  }

  /**
   * check if computation cache contains apply operation with input tuple.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param op - applied operator
   * @return Optional of cached item or Optional empty
   */
  @SuppressWarnings("unchecked")
  protected Optional<V> checkBinaryCache(V f1, V f2, int op) {
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), op);
    Cache.CacheData data = computedTable.get(hash);
    if (data instanceof Cache.CacheDataBinaryOp) {
      Cache.CacheDataBinaryOp<V> d = (Cache.CacheDataBinaryOp<V>) data;
      if (d.getF1().equals(f1) && d.getF2().equals(f2) && d.getOp() == op) {
        return Optional.of(d.getRes());
      }
    }

    return Optional.empty();
  }

  /**
   * check if computation cache contains negated input argument.
   *
   * @param f - the {@link DD} argument
   * @return Optional of cached item or Optional empty
   */
  protected Optional<V> checkNotCache(V f) {
    int hash = f.hashCode();
    Cache.CacheData data = computedTable.get(hash);
    if (data instanceof Cache.CacheDataNot<?>) {
      @SuppressWarnings("unchecked")
      Cache.CacheDataNot<V> d = (Cache.CacheDataNot<V>) data;
      if (d.getF().equals(f)) {
        return Optional.of(d.getRes());
      }
    }

    return Optional.empty();
  }

  @Override
  public int getCacheSize() {
    return computedTable.size();
  }

  @Override
  public int getCacheNodeCount() {
    return computedTable.nodeCount();
  }

  protected V getLow(V root) {
    return nodeManager.getLow(root);
  }

  protected V getHigh(V root) {
    return nodeManager.getHigh(root);
  }
}
