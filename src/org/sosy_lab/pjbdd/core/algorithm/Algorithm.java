// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.algorithm;

import org.sosy_lab.pjbdd.api.DD;

/**
 * general Interface for BDD manipulating algorithms.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public interface Algorithm<V extends DD> {

  /** Shutdown algorithm instance and components. */
  void shutdown();

  /**
   * Get the logical false bdd.
   *
   * @return false bdd
   */
  V makeFalse();

  /**
   * Get the logical true bdd.
   *
   * @return true bdd
   */
  V makeTrue();

  /**
   * Create a new node with given input triple or return a matching existing.
   *
   * @param low - low branch bdd
   * @param high - high branch bdd
   * @param var - bdd variable
   * @return new or matching bdd
   */
  V makeNode(V low, V high, int var);

  /**
   * Restrict a given variable in a given bdd to high or low branch.
   *
   * @param bdd - given bdd
   * @param var - given variable
   * @param restrictionVar - high or low branch
   * @return restricted bdd
   */
  V restrict(V bdd, int var, boolean restrictionVar);

  int getCacheSize();

  int getCacheNodeCount();
}
