// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.algorithm;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.DD;

/**
 * BDD based interface for sat algorithms.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public interface SatAlgorithm<V extends DD> {
  /**
   * Get {@link DD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param b - the {@link DD} node
   * @return the number of satisfying variable assignments.
   */
  BigInteger satCount(V b);

  /**
   * Takes a {@link DD} and finds any satisfying variable assignment.
   *
   * @param bdd - the {@link DD} node
   * @return one satisfying variable assignment.
   */
  V anySat(V bdd);
}
