// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.node;

import java.util.List;
import org.sosy_lab.pjbdd.api.DD;

/**
 * interface for {@link DD} based variable management.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public interface NodeManager<V extends DD> {

  /**
   * Get the bdd variable at given level.
   *
   * @param level - given level
   * @return variable for given level
   */
  int var(int level);

  /**
   * set variable count and create bdd variables.
   *
   * @param count - variable count
   * @return max level
   */
  int setVarCount(int count);

  /**
   * get a given level's next greater level.
   *
   * @param lvl - old level
   * @return next level
   */
  int getNext(int lvl);

  /**
   * Get the level of a given bdd variable.
   *
   * @param variable - given variable
   * @return variable's level
   */
  int level(int variable);

  /**
   * get number of created bdd variables.
   *
   * @return the number of created bdd variables
   */
  int getVarCount();

  /**
   * Get the current bdd variable ordering.
   *
   * @return the current bdd variable ordering
   */
  int[] getCurrentOrdering();

  /**
   * make next bdd variable.
   *
   * @return created variable as {@link DD}
   */
  V makeNext();

  /**
   * Check if the given level already exists.
   *
   * @param level - the level to be checked
   * @return <code>true</code> if the level exists, <code>false</code> else
   */
  boolean checkLvl(int level);

  /**
   * Change the current variable ordering and reorder bdd.
   *
   * @param pOrder - the new order
   */
  void setVarOrder(List<Integer> pOrder);

  /**
   * find a various number of levels' topmost variable.
   *
   * @param levels - variable amount of levels
   * @return topmost variable
   */
  int topVar(int[] levels);

  /**
   * Look up for existing node with given properties or creates a new bdd node.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the variable
   * @return - existing or created node
   */
  V makeNode(V low, V high, int var);

  /**
   * get logical false representation.
   *
   * @return false bdd
   */
  V getFalse();

  /**
   * get logical true representation.
   *
   * @return true bdd
   */
  V getTrue();

  /** Shutdown manager and corresponding components. */
  void shutdown();

  /**
   * Get ith-variable bdd representation.
   *
   * @param variable - the variable
   * @return variable's bdd representation.
   */
  default V makeIthVar(int variable) {
    return makeNode(getFalse(), getTrue(), variable);
  }

  V getHigh(V bdd);

  V getLow(V bdd);

  int getNodeCount();

  int getUniqueTableSize();

  V makeVariableBefore(V var);

  void cleanUnusedNodes();
}
