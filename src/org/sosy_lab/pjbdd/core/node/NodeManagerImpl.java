// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.node;

import com.google.common.primitives.Ints;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.uniquetable.UniqueTable;

/**
 * Main and simple {@link NodeManager} implementation.
 *
 * @author Stephan Holzner
 * @see NodeManager
 * @see DD
 * @since 1.0
 */
public class NodeManagerImpl<V extends DD> implements NodeManager<V> {

  /** number of defined BDD variables. */
  protected int varCount;

  /** Backing arrays for storing level and variable relation. */
  protected int[] levelVar;

  protected int[] varLevel;

  protected final ReductionRule<V> reductionRule;

  /**
   * Set of defined {@link DD} variables. Prevents variable deletion from unique table (due to
   * {@link java.lang.ref.WeakReference}).
   */
  protected Set<V> varSet;

  /** Backing unique table for bdd storage. */
  protected final UniqueTable<V> uniqueTable;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public NodeManagerImpl(UniqueTable<V> uniqueTable, ReductionRule<V> reductionRule) {
    this.reductionRule = reductionRule;
    this.uniqueTable = uniqueTable;
  }

  /** {@inheritDoc} */
  @Override
  public int var(int level) {
    return levelVar[level];
  }

  /** {@inheritDoc} */
  @Override
  public int setVarCount(int count) {
    if (count < 1 || count <= varCount) {
      return varCount;
    }
    int oldCount = varCount;
    varCount = count;

    if (varSet == null) {
      varSet = Collections.synchronizedSet(new HashSet<>());
      levelVar = new int[varCount];
      varLevel = new int[varCount];
    } else {
      int[] newLevelVar = new int[varCount];
      int[] newVarLevel = new int[varCount];
      System.arraycopy(levelVar, 0, newLevelVar, 0, levelVar.length);
      System.arraycopy(varLevel, 0, newVarLevel, 0, levelVar.length);
      levelVar = newLevelVar;
      varLevel = newVarLevel;
    }

    for (int i = oldCount; i < count; i++) {
      makeVariable(i);
    }
    return varCount;
  }

  /** {@inheritDoc} */
  @Override
  public int getNext(int lvl) {
    if (lvl >= levelVar.length) {
      throw new IndexOutOfBoundsException("no such variable");
    }
    return levelVar[lvl + 1];
  }

  /** {@inheritDoc} */
  @Override
  public int level(int variable) {
    return (variable < 0) ? varCount : varLevel[variable];
  }

  /** {@inheritDoc} */
  @Override
  public int getVarCount() {
    return varCount;
  }

  /** {@inheritDoc} */
  @Override
  public int[] getCurrentOrdering() {
    return Arrays.copyOf(levelVar, levelVar.length);
  }

  /** {@inheritDoc} */
  @Override
  public boolean checkLvl(int level) {
    return (level < varCount);
  }

  /** {@inheritDoc} */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    // Fill up existing order
    int max = Collections.max(pOrder);
    if (!checkLvl(max)) {
      setVarCount(max);
    }
    // actually reorder
    IntStream.range(0, pOrder.size())
        .forEach(
            i -> {
              if (!Objects.equals(pOrder.get(i), levelVar[i])) {
                swapVarToLevel(pOrder.get(i), i);
              }
            });
  }

  /** {@inheritDoc} */
  @Override
  public int topVar(int[] levels) {
    return var(Ints.min(levels));
  }

  /** {@inheritDoc} */
  @Override
  public V makeNode(V low, V high, int var) {
    return reductionRule
        .apply(low, high, var)
        .orElseGet(() -> uniqueTable.getOrCreate(low, high, var));
  }

  /** {@inheritDoc} */
  @Override
  public V getFalse() {
    return uniqueTable.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public V getTrue() {
    return uniqueTable.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    varLevel = null;
    levelVar = null;
    varSet.clear();
    uniqueTable.shutDown();
  }

  @Override
  public V getHigh(V bdd) {
    return uniqueTable.getHigh(bdd);
  }

  @Override
  public V getLow(V bdd) {
    return uniqueTable.getLow(bdd);
  }

  @Override
  public int getNodeCount() {
    return uniqueTable.nodeCount() + 2;
  }

  @Override
  public int getUniqueTableSize() {
    return uniqueTable.size();
  }

  @Override
  public V makeVariableBefore(V var) {
    V newVariable = makeNext();
    int newLevel = level(var.getVariable());
    // shift all existing elements >= var by 1
    for (int i = levelVar.length - 2; i + 1 > newLevel; i--) {
      int newVarIndex = levelVar[i];
      levelVar[i + 1] = levelVar[i];
      varLevel[newVarIndex] = i + 1;
    }

    levelVar[newLevel] = newVariable.getVariable();
    varLevel[newVariable.getVariable()] = newLevel;

    return newVariable;
  }

  @Override
  public void cleanUnusedNodes() {
    uniqueTable.cleanUnusedNodes();
  }

  /**
   * Swaps a specific variable up or down to a certain level.
   *
   * @param var - the variable to be swapped
   * @param level - the target level
   */
  private void swapVarToLevel(int var, int level) {
    int levelOfVar = varLevel[var];
    while (levelOfVar != level) {
      if (levelOfVar < level) {
        swapLevel(levelOfVar + 1, levelOfVar++);
      } else {
        swapLevel(levelOfVar, --levelOfVar);
      }
    }
  }

  /**
   * Swaps variables by given variable levels.
   *
   * @param levelA - getIf variable's level
   * @param levelB - getThen variable's level
   */
  private void swapLevel(int levelA, int levelB) {
    int varA = levelVar[levelA];
    int varB = levelVar[levelB];
    varLevel[varA] = levelB;
    varLevel[varB] = levelA;
    levelVar[levelB] = varA;
    levelVar[levelA] = varB;
    uniqueTable.swap(varA, varB, this::makeNode);
  }

  /** {@inheritDoc} */
  @Override
  public V makeNext() {
    return makeVariable(varCount);
  }

  /**
   * Actually creates the variable and its negation as {@link DD}.
   *
   * @param var - the variable to be created
   * @return the created variable as {@link DD}
   */
  private V makeVariable(int var) {

    if (!checkLvl(var)) {
      setVarCount(var + 1);
    }

    V posVar = makeNode(getFalse(), getTrue(), var);
    varSet.add(posVar);
    levelVar[var] = var;
    varLevel[var] = var;

    V negVar = makeNode(getTrue(), getFalse(), var);
    varSet.add(negVar);
    return posVar;
  }
}
