// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.uniquetable;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.reference.ComparableWeakBDDReference;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

/**
 * {@link UniqueTable} implementation which uses a {@link ConcurrentMap} as backing concurrent
 * collection. The backing map uses the custom {@link WeakReference} type {@link
 * ComparableWeakBDDReference} for keys and regular type {@link WeakReference} for values.
 *
 * <p>Custom {@link ComparableWeakBDDReference} enables {@link ConcurrentMap#get(Object)} with
 * regular {@link DD} instances.
 *
 * @author Stephan Holzner
 * @see UniqueTable
 * @see ComparableWeakBDDReference
 * @see DD
 * @since 1.0
 */
public class DDConcurrentWeakHashMap<V extends DD> implements UniqueTable<V> {
  /** the backing concurrent map with weak keys and values. */
  private final ConcurrentMap<ComparableWeakBDDReference<V>, WeakReference<V>> map;

  /** reference queue to collect reclaimed nodes. */
  private final ReferenceQueue<V> referenceQueue;

  /** logical zero/false representation. */
  private final V zero;

  /** logical one/true representation. */
  private final V one;

  /** custom factory for bdd creations and node recycling. */
  private final DD.Factory<V> factory;

  /** demon thread to cleanup reclaimed entries. */
  private final CleanUpThread cleaner;

  private final DD.ChildNodeResolver<V> resolver;

  /**
   * Creates new {@link DDConcurrentWeakHashMap} instances with given parameters.
   *
   * @param initialCapacity - backing map's initial size
   * @param parallelism - backing map's concurrency factor
   * @param factory - the node Factory
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public DDConcurrentWeakHashMap(int initialCapacity, int parallelism, DD.Factory<V> factory) {
    this.map = new ConcurrentHashMap<>(initialCapacity, 0.75f, parallelism);
    this.referenceQueue = new ReferenceQueue<>();
    this.cleaner = new CleanUpThread();
    this.factory = factory;
    this.zero = factory.createFalse();
    this.one = factory.createTrue();
    this.resolver = factory.getChildNodeResolver();
    this.startCleaner();
  }

  /** Start cleanup thread. */
  private void startCleaner() {
    cleaner.start();
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<V> bddConsumer) {
    map.values().stream().map(Reference::get).filter(Objects::nonNull).forEach(bddConsumer);
  }

  @Override
  public V getLow(V root) {
    return resolver.getLow(root);
  }

  @Override
  public V getHigh(V root) {
    return resolver.getHigh(root);
  }

  @Override
  public void rehash(DD node, int oldHash) {
    // no rehash needed with this map type
  }

  /** {@inheritDoc} */
  @Override
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public DD.Factory<V> getFactory() {
    return factory;
  }

  @Override
  public int nodeCount() {
    return map.size();
  }

  @Override
  public int size() {
    return map.size();
  }

  @Override
  public void cleanUnusedNodes() {
    Reference<? extends DD> ref = referenceQueue.poll();
    while (ref != null) {
      if (ref instanceof ComparableWeakBDDReference) {
        map.remove(ref);
      }
      ref = referenceQueue.poll();
    }
  }

  /** {@inheritDoc} */
  @Override
  public V getOrCreate(V low, V high, int var) {
    V lookUp = factory.createNode(var, low, high);
    ComparableWeakBDDReference<V> ref = new ComparableWeakBDDReference<>(lookUp, referenceQueue);
    // handle concurrency: BDD may be created with other task
    WeakReference<V> refRes = map.putIfAbsent(ref, ref);
    if (refRes == null) {
      return lookUp;
    }
    // handle concurrency: other BDD may already be collected
    V res = refRes.get();
    if (res != null) {
      return res;
    }
    return getOrCreate(low, high, var);
  }

  /** {@inheritDoc} */
  @Override
  public V getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public V getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    cleaner.shutdown();
    clear();
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    map.clear();
  }

  /**
   * {@link ReclaimedReferenceCleaningThread} sub class to handle garbage collected bdd instances.
   *
   * @author Stephan Holzner
   * @see ReclaimedReferenceCleaningThread
   * @since 1.0
   */
  private final class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      Reference<? extends DD> sv = referenceQueue.remove(1000);
      if (sv != null) {
        if (sv instanceof ComparableWeakBDDReference) {
          map.remove(sv);
        }
      }
    }
  }
}
