// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.uniquetable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * {@link UniqueTable} implementation which uses backing arrays. Handles concurrent accesses with
 * compare and swap using {@link VarHandle}.
 *
 * @author Stephan Holzner
 * @see UniqueTable
 * @since 1.0
 */
public class DDCASArrayUniqueTable<V extends DD> extends WeakArrayUniqueTable<V>
    implements UniqueTable<V> {

  private static final VarHandle HASH_HANDLE = MethodHandles.arrayElementVarHandle(int[].class);
  private static final VarHandle COLLECTED_COUNTER_HANDLE;
  private static final VarHandle FREE_COUNTER_HANDLE;
  private static final VarHandle NEXT_FREE_HANDLE;

  static {
    try {
      COLLECTED_COUNTER_HANDLE =
          MethodHandles.lookup()
              .findVarHandle(WeakArrayUniqueTable.class, "collectedCount", int.class);
      FREE_COUNTER_HANDLE =
          MethodHandles.lookup()
              .findVarHandle(WeakArrayUniqueTable.class, "freeCounter", int.class);
      NEXT_FREE_HANDLE =
          MethodHandles.lookup().findVarHandle(WeakArrayUniqueTable.class, "nextFree", int.class);
    } catch (ReflectiveOperationException e) {
      throw new LinkageError("Unexpected reflection error", e);
    }
  }

  /**
   * Creates new {@link DDCASArrayUniqueTable} instances with given parameters.
   *
   * @param increaseFactor - backing array's increase factor
   * @param initialSize - backing array's initial size
   * @param factory - BDDNode.Factory instance for node creations
   */
  public DDCASArrayUniqueTable(int increaseFactor, int initialSize, DD.Factory<V> factory) {
    super(increaseFactor, initialSize, factory);
    this.nodeCount = PrimeUtils.getGreaterNextPrime(initialSize);
    this.initIntern();
  }

  /** {@inheritDoc} */
  @Override
  public V getOrCreate(V low, V high, int var) {
    int head = getNodeWithHash(hashNode(var, low.hashCode(), high.hashCode()));
    V bdd = get(low, high, var);
    if (bdd != null) {
      return bdd;
    }

    int res;
    res = getNextFree();
    if (res > 1) {
      atomicDecrementFreeCounter();
      return createAtomic(low, high, var, res, head);
    } else {
      tryResizing();
    }

    return getOrCreate(low, high, var);
  }

  /**
   * Lookup a {@link DD} node with specified parameters.
   *
   * @param low - the nodes low branch
   * @param high - the nodes high branch
   * @param var - the nodes bdd variable
   * @return the matching node or <code>null</code> else
   */
  @Override
  protected V get(V low, V high, int var) {
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    int res = getNodeWithHash(hash);

    while (res != 0) {
      if (table.get(res) == null) {
        res = getNext(res);
        continue;
      }
      V bdd = table.get(res);
      if (bdd == null) {
        res = getNext(res);
        continue;
      }
      if (bdd.equalsTo(var, low, high)) {
        return bdd;
      }

      res = getNext(res);
    }
    return null;
  }

  /**
   * Creates a new {@link DD} with given parameters and corresponding table entries. Method do not
   * handle concurrent accesses!
   *
   * @param low - the given low branch
   * @param high - the given high branch
   * @param var - the given bdd variable
   * @param res - the new nodes array index
   * @param head - first checked node in hash bin
   * @return the created {@link DD}
   */
  protected V createAtomic(V low, V high, int var, int res, int head) {
    atomicDecrementFreeCounter();
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    if (!setHashAtomic(hash, res, head)) {
      // hash bin changed recheck for existing node
      return getOrCreate(low, high, var);
    }

    setNext(res, head);
    V result = factory.createNode(var, low, high);
    table.set(res, result);
    return result;
  }

  protected boolean setHashAtomic(int hash, int res, int old) {
    // this function needs to be atomic for same hash
    return HASH_HANDLE.weakCompareAndSetPlain(hashTable, hash, old, res);
  }

  private final Lock resizeMonitor = new ReentrantLock();
  private volatile boolean resizeInProgress = false;

  /** Try to resize table. Wait if resize op is already in progress. Collect all locks else. */
  @Override
  protected void tryResizing() {
    if (checkResize()) {
      resizeMonitor.lock();
      try {
        if (checkResize()) { // resize performed
          resizeInProgress = true;
          try {

            resizeTable();
          } finally {
            resizeInProgress = false;
          }
        }
      } finally {
        resizeMonitor.unlock();
      }
    }
  }

  @Override
  protected boolean checkResize() {
    if (nextFree > 1) {
      return resizeInProgress;
    }
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<V> bddConsumer) {
    table.toStream().forEach(bddConsumer);
  }

  /** Initialize unique table and it's backing arrays. */
  @Override
  protected void initIntern() {
    resizeInProgress = false;

    one = factory.createTrue();
    zero = factory.createFalse();
    table = new CASWeakDDArray<>(nodeCount, factory.getChildNodeResolver());
    nextTable = new int[this.nodeCount];
    hashTable = new int[this.nodeCount];

    for (int n = 0; n < this.nodeCount; n++) {
      setNext(n, n + 1);
    }
    setNext(this.nodeCount - 1, 0);
    nextFree = 2;

    atomicSetFreeCounter(this.nodeCount - 2);
  }

  /**
   * Set index's next node with equal hash code.
   *
   * @param index - the root node's index
   * @param next - the next node's index with equal hash code
   */
  @Override
  protected void setNext(int index, int next) {
    if (index != next) {
      nextTable[index] = next;
    }
  }

  /** {@inheritDoc} */
  @Override
  protected int getNext(int index) {
    return nextTable[index];
  }

  /** {@inheritDoc} */
  @Override
  protected void resizeTable() {
    int oldSize = nodeCount;
    int newSize = nodeCount;

    if (atomicReadCollectedCounter() >= nodeCount * onlyCleanThreshold) {
      doResize(oldSize, newSize);
    } else {
      super.resizeTable();
    }
    atomicResetCollectedCounter();
  }

  private void atomicDecrementFreeCounter() {
    freeCounter = (int) FREE_COUNTER_HANDLE.getAndAdd(this, -1);
  }

  private int atomicReadCollectedCounter() {
    return (int) COLLECTED_COUNTER_HANDLE.get(this);
  }

  private void atomicResetCollectedCounter() {
    COLLECTED_COUNTER_HANDLE.set(this, 0);
  }

  private void atomicSetFreeCounter(int value) {
    FREE_COUNTER_HANDLE.set(this, value);
  }

  private boolean atomicSetNextFree(int value, int old) {
    return NEXT_FREE_HANDLE.weakCompareAndSetPlain(this, old, value);
  }

  @Override
  protected int getNextFree() {
    int next = (int) NEXT_FREE_HANDLE.get(this);
    if (atomicSetNextFree(getNext(next), next)) {
      return next;
    }
    return getNextFree();
  }
}
