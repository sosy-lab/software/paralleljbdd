// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.uniquetable;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.reference.ComparableWeakBDDReference;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

/**
 * {@link UniqueTable} implementation which uses a {@link ConcurrentMap} as backing concurrent
 * collection. The backing map uses {@link Integer} keys and {@link Deque} values. The key {@link
 * Integer} corresponds to {@link DD} hash codes. All {@link DD} with equal hashcode stored as
 * {@link ComparableWeakBDDReference} in the conforming {@link Deque}.
 *
 * @author Stephan Holzner
 * @see UniqueTable
 * @see ComparableWeakBDDReference
 * @see DD
 * @since 1.0
 */
public class DDConcurrentWeakHashDeque<V extends DD> implements UniqueTable<V> {
  /** the concurrent map as backing hash bucket. */
  private final ConcurrentMap<Integer, Deque<WeakReference<V>>> weakValuesHashBucket;

  /** reference queue to collect reclaimed nodes. */
  private final ReferenceQueue<V> referenceQueue;

  /** current number of node entries. */
  private final LongAdder size;

  /** logical zero/false representation. */
  private V zero;

  /** logical one/true representation. */
  private V one;

  /** factory for bdd creations. */
  private final DD.Factory<V> factory;

  /** demon thread to cleanup reclaimed entries. */
  private final CleanUpThread cleaner;

  private final DD.ChildNodeResolver<V> resolver;

  /**
   * Creates new {@link DDConcurrentWeakHashDeque} instances with given parameters.
   *
   * @param tableSize - backing map's initial size
   * @param parallelism - backing map's concurrency factor
   * @param factory - the factory for node creation
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public DDConcurrentWeakHashDeque(int tableSize, int parallelism, DD.Factory<V> factory) {
    weakValuesHashBucket = new ConcurrentHashMap<>(tableSize, 0.75f, parallelism);
    referenceQueue = new ReferenceQueue<>();
    size = new LongAdder();
    this.factory = factory;
    one = factory.createTrue();
    zero = factory.createFalse();
    this.resolver = factory.getChildNodeResolver();
    cleaner = new CleanUpThread();
    cleaner.start();
  }

  /** {@inheritDoc} */
  @Override
  public V getOrCreate(V low, V high, int var) {
    int hash = HashCodeGenerator.generateHashCode(var, low.hashCode(), high.hashCode());
    if (weakValuesHashBucket.containsKey(hash)) {
      synchronized (weakValuesHashBucket.get(hash)) {
        for (Reference<? extends DD> ref : weakValuesHashBucket.get(hash)) {
          V value = resolver.cast(ref.get());
          if (value == null) {
            continue;
          }
          if (value.equalsTo(var, low, high)) {
            return value;
          }
        }
      }
    }
    return add(factory.createNode(var, low, high));
  }

  /**
   * Add {@link DD} to corresponding hash bucket. Creates bucket if there is no existing.
   *
   * @param node - to add
   * @return the node
   */
  private V add(V node) {
    if (node.isTrue()) {
      one = node;
    } else if (node.isFalse()) {
      zero = node;
    } else {
      WeakReference<V> ref = new ComparableWeakBDDReference<>(node, referenceQueue);
      weakValuesHashBucket.putIfAbsent(node.hashCode(), new ConcurrentLinkedDeque<>());
      weakValuesHashBucket.get(node.hashCode()).add(ref);
    }
    size.increment();
    return node;
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    weakValuesHashBucket.clear();
    size.reset();
  }

  /** {@inheritDoc} */
  @Override
  public V getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public V getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    clear();
    cleaner.shutdown();
  }

  /** {@inheritDoc} */
  @Override
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public DD.Factory<V> getFactory() {
    return factory;
  }

  @Override
  public int nodeCount() {
    return (int) toStream().count();
  }

  @Override
  public int size() {
    return (int) toStream().count();
  }

  @Override
  public void cleanUnusedNodes() {
    Reference<? extends DD> ref = referenceQueue.poll();
    while (ref != null) {
      if (ref instanceof ComparableWeakBDDReference) {
        removeReference((ComparableWeakBDDReference<?>) ref);
      }
      ref = referenceQueue.poll();
    }
  }

  private Stream<V> toStream() {
    return weakValuesHashBucket.values().stream()
        .flatMap(Deque::stream)
        .map(Reference::get)
        .filter(Objects::nonNull);
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<V> bddConsumer) {
    toStream().forEach(bddConsumer);
  }

  /** {@inheritDoc} */
  @Override
  public V getLow(V root) {
    return resolver.getLow(root);
  }

  /** {@inheritDoc} */
  @Override
  public V getHigh(V root) {
    return resolver.getHigh(root);
  }

  /** {@inheritDoc} */
  @Override
  public void rehash(V node, int oldHash) {
    if (node.hashCode() != oldHash) {
      for (WeakReference<? extends DD> next :
          weakValuesHashBucket.getOrDefault(oldHash, new ArrayDeque<>())) {
        if (Objects.equals(next.get(), node)) {
          removeReference(next);
          break;
        }
      }
      add(node);
    }
  }

  /**
   * remove a given reference from unique table.
   *
   * @param reference - to be removed
   */
  private void removeReference(WeakReference<? extends DD> reference) {
    int hash = reference.hashCode();
    weakValuesHashBucket.getOrDefault(hash, new ArrayDeque<>()).remove(reference);
    size.decrement();
  }

  /**
   * {@link ReclaimedReferenceCleaningThread} sub class to handle garbage collected bdd instances.
   *
   * @author Stephan Holzner
   * @see ReclaimedReferenceCleaningThread
   * @since 1.0
   */
  private final class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      Reference<? extends DD> reference = referenceQueue.remove(1000);
      if (reference != null) {
        if (reference instanceof ComparableWeakBDDReference && !isShutdown()) {
          removeReference((ComparableWeakBDDReference<?>) reference);
        }
      }
    }
  }
}
