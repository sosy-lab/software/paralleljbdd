// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.uniquetable;

import java.util.function.Consumer;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Main {@link DD} based unique table interface defining all unique table methods.
 *
 * @param <V> the generic {@link DD} subtype
 * @author Stephan Holzner
 * @see DD
 * @since 1.0
 */
public interface UniqueTable<V extends DD> {

  /** Clear all unique table entries. */
  void clear();

  /**
   * Get existing or else create the {@link DD} with following attributes.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the bdd variable
   * @return the matching bdd
   */
  V getOrCreate(V low, V high, int var);

  /**
   * Get the logical true {@link DD} representation.
   *
   * @return logical true representation
   */
  V getTrue();

  /**
   * Get the logical false {@link DD} representation.
   *
   * @return logical false representation
   */
  V getFalse();

  /** Shutdown unique table: cleanup resources. */
  void shutDown();

  /**
   * Apply function on each bdd in table.
   *
   * @param function - the function
   */
  void forEach(Consumer<V> function);

  V getLow(V root);

  V getHigh(V root);

  /**
   * Swap two variables and update all corresponding bdd.
   *
   * @param upper - the upper variable to be swapped down
   * @param lower - the lower variable to be swapped up
   * @param delegate - the node managing delegate
   */
  default void swap(int upper, int lower, NodeMaker<V> delegate) {
    forEach(
        bdd -> {
          if (bdd.getVariable() == lower) {

            DD.Factory<V> factory = getFactory();

            V low = getLow(bdd);
            V high = getHigh(bdd);

            if (low.getVariable() == upper || high.getVariable() == upper) {
              V low0 = low;
              V low1 = low;
              V high0 = high;
              V high1 = high;

              if (low.getVariable() == upper) {
                low0 = getLow(low);
                low1 = getHigh(low);
              }

              if (high.getVariable() == upper) {
                high0 = getLow(high);
                high1 = getHigh(high);
              }

              low = delegate.makeNode(low0, high0, lower);
              high = delegate.makeNode(low1, high1, lower);
              factory.setVariable(upper, bdd);
              factory.setHigh(high, bdd);
              factory.setLow(low, bdd);

              int oldHash = bdd.hashCode();
              rehash(bdd, oldHash);
            }
          }
        });
  }

  /**
   * Rehash a given node after reorder operations - old hash code needed.
   *
   * @param node - the node to be rehashed
   * @param oldHash - the old hash code
   */
  void rehash(V node, int oldHash);

  /**
   * Get the table's bdd node factory.
   *
   * @return the node factory
   */
  DD.Factory<V> getFactory();

  int nodeCount();

  int size();

  void cleanUnusedNodes();

  interface NodeMaker<V extends DD> {
    V makeNode(V low, V high, int var);
  }
}
