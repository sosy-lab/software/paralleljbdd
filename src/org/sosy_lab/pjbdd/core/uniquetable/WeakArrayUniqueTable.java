// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.uniquetable;

import java.util.function.Consumer;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * {@link UniqueTable} implementation which uses backing arrays. Does not handle concurrent
 * accesses!
 *
 * @author Stephan Holzner
 * @see UniqueTable
 * @since 1.0
 */
public class WeakArrayUniqueTable<V extends DD> implements UniqueTable<V> {

  /**
   * threshold for resizing operations: if table contains more than 'ONLY_CLEAN_THRESHOLD's
   * percentage of recycled elements, there will be no resize but clean operation.
   */
  protected static final double onlyCleanThreshold = 0.2;

  /** count of garbage collected table entries. */
  protected int collectedCount = 0;

  /** underlying unique table. */
  protected WeakDDArray<V> table;

  /** underlying hash table. */
  protected int[] hashTable;

  /** underlying next table. */
  protected int[] nextTable;

  /** logical one/true representation. */
  protected V one;

  /** logical zero/false representation. */
  protected V zero;

  /** factory for node creations. */
  protected final DD.Factory<V> factory;

  /** resolver for generic child node handling. */
  protected final DD.ChildNodeResolver<V> resolver;

  /** maximal array size. */
  protected int maxSize = Integer.MAX_VALUE;

  /** representation for next free allocated bdd node. */
  protected int nextFree = 0;

  /** number of free allocated bdd nodes. */
  protected int freeCounter = 0;

  /** number of allocated bdd nodes. */
  protected int nodeCount;

  /** resizing table's increase factor. */
  protected int increaseFactor;

  /**
   * Creates new {@link WeakArrayUniqueTable} instances with given parameters.
   *
   * @param increaseFactor - backing array's increase factor
   * @param initialSize - backing array's initial size
   * @param factory - BDDNode.Factory instance for node creations
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public WeakArrayUniqueTable(int increaseFactor, int initialSize, DD.Factory<V> factory) {
    this.factory = factory;
    this.resolver = factory.getChildNodeResolver();
    this.increaseFactor = increaseFactor;
    this.nodeCount = PrimeUtils.getGreaterNextPrime(initialSize);
    this.initIntern();
  }

  /** {@inheritDoc} */
  @Override
  public V getOrCreate(V low, V high, int var) {
    int head = getNodeWithHash(hashNode(var, low.hashCode(), high.hashCode()));
    V bdd = get(low, high, var);
    if (bdd != null) {
      return bdd;
    }

    int res;
    res = getNextFree();
    if (res > 1) {
      return create(low, high, var, res, head);
    } else {
      tryResizing();
    }

    return getOrCreate(low, high, var);
  }

  /**
   * Lookup a {@link DD} node with specified parameters.
   *
   * @param low - the nodes low branch
   * @param high - the nodes high branch
   * @param var - the nodes bdd variable
   * @return the matching node or <code>null</code> else
   */
  protected V get(V low, V high, int var) {
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    int res = getNodeWithHash(hash);

    while (res != 0) {
      if (table.get(res) == null) {
        res = getNext(res);
        continue;
      }
      V bdd = table.get(res);
      if (bdd == null) {
        res = getNext(res);
        continue;
      }
      if (bdd.equalsTo(var, low, high)) {
        return bdd;
      }

      res = getNext(res);
    }
    return null;
  }

  /**
   * Creates a new {@link DD} with given parameters and corresponding table entries. Method do not
   * handle concurrent accesses!
   *
   * @param low - the given low branch
   * @param high - the given high branch
   * @param var - the given bdd variable
   * @param res - the new nodes array index
   * @param head - first checked node in hash bin
   * @return the created {@link DD}
   */
  protected V create(V low, V high, int var, int res, int head) {
    freeCounter--;
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    setHash(hash, res);
    setNext(res, head);
    V result = factory.createNode(var, low, high);
    table.set(res, result);
    return result;
  }

  /** Try to resize table. Wait if resize op is already in progress. Collect all locks else. */
  protected void tryResizing() {
    if (checkResize()) {
      resizeTable();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<V> bddConsumer) {
    table.toStream().forEach(bddConsumer);
  }

  /** Initialize unique table and it's backing arrays. */
  protected void initIntern() {
    this.freeCounter = 2;
    one = factory.createTrue();
    zero = factory.createFalse();
    table = new WeakDDArray<>(nodeCount, factory.getChildNodeResolver());
    nextTable = new int[this.nodeCount];
    hashTable = new int[this.nodeCount];

    for (int n = 0; n < this.nodeCount; n++) {
      setNext(n, n + 1);
    }
    setNext(this.nodeCount - 1, 0);
    nextFree = 2;
    freeCounter = this.nodeCount - 2;
  }

  /**
   * Set index's next node with equal hash code.
   *
   * @param index - the root node's index
   * @param next - the next node's index with equal hash code
   */
  protected void setNext(int index, int next) {
    if (index != next) {
      nextTable[index] = next;
    }
  }

  protected int getNext(int index) {
    return nextTable[index];
  }

  protected void resizeTable() {
    int oldSize = nodeCount;
    int newSize = nodeCount;

    if (freeCounter + collectedCount < nodeCount * onlyCleanThreshold) {
      if (increaseFactor > 0) {
        newSize += newSize * increaseFactor;
      } else {
        newSize = newSize << 1;
      }

      if (newSize > maxSize) {
        newSize = maxSize;
      }
    }
    doResize(oldSize, newSize);
    collectedCount = 0;
  }

  /**
   * Get the index of the {@link DD} with a given hashcode.
   *
   * @param hash - the given hashcode
   * @return node's index with given hashcode
   */
  protected int getNodeWithHash(int hash) {
    return hashTable[hash];
  }

  /**
   * Set the index of the {@link DD} with a given hashcode.
   *
   * @param hash - the node's hashcode
   * @param index - the node's index
   */
  protected void setHash(int hash, int index) {
    hashTable[hash] = index;
  }

  /**
   * Calculates hashcode for given input triple.
   *
   * @param lvl - a node's level
   * @param low - a node's low branch
   * @param high - a node's high branch
   * @return hashcode for given input triple
   */
  protected int hashNode(int lvl, int low, int high) {
    return Math.abs(HashCodeGenerator.generateHashCode(lvl, low, high) % nodeCount);
  }

  protected void doResize(int oldSize, int newSize) {

    newSize = PrimeUtils.getLowerNextPrime(newSize);

    if (oldSize < newSize) {

      int[] newHashTable = new int[newSize];
      int[] newNextTable = new int[newSize];

      System.arraycopy(hashTable, 0, newHashTable, 0, hashTable.length);
      System.arraycopy(nextTable, 0, newNextTable, 0, nextTable.length);
      table.resize(newSize);
      nextTable = newNextTable;
      hashTable = newHashTable;

      nodeCount = newSize;
    }

    for (int n = 0; n < oldSize; n++) {
      setHash(n, 0);
    }

    for (int n = oldSize; n < nodeCount; n++) {
      setNext(n, n + 1);
    }
    setNext(nodeCount - 1, nextFree);
    freeCounter = nodeCount;
    rehash();
  }

  /** Rehash table entries after resize task. (Hash value changes with table size). */
  private void rehash() {
    nextFree = 0;
    for (int n = nodeCount - 1; n >= 2; n--) {
      if (table.get(n) != null) {
        V bdd = get(n);
        if (bdd != null) {
          int hash = hashNode(bdd.getVariable(), bdd.getLow().hashCode(), bdd.getHigh().hashCode());
          setNext(n, getNodeWithHash(hash));
          setHash(hash, n);
          freeCounter--;
          continue;
        }
      }
      table.set(n, null);
      setNext(n, nextFree);
      nextFree = n;
    }
  }

  /** {@inheritDoc} */
  @Override
  public void rehash(V node, int oldHash) {
    int newHash = hashNode(node.getVariable(), node.getLow().hashCode(), node.getHigh().hashCode());
    oldHash = Math.abs(oldHash % nodeCount);
    if (oldHash != newHash) {

      int index = getNodeWithHash(oldHash);
      V res = get(index);
      int previous = index;
      while (res != node) {
        previous = index;
        index = getNext(index);
        res = get(index);
      }
      if (previous == index) { // bdd is result of getNodeWithHash
        setHash(oldHash, getNext(index));
      } else {
        setNext(previous, getNext(index));
      }
      setNext(index, getNodeWithHash(newHash));
      setHash(newHash, index);
    }
  }

  /**
   * Returns next free index and set next free pointer to next uses lock.
   *
   * @return next free index
   */
  protected int getNextFree() {
    int next = nextFree;
    nextFree = getNext(next);
    return next;
  }

  /**
   * Checks if table's reaches threshold and table resize must be performed.
   *
   * @return true if minimal threshold of free nodes reached
   */
  protected boolean checkResize() {
    return nextFree <= 1;
  }

  protected V get(int index) {
    return table.get(index);
  }

  /** {@inheritDoc} */
  @Override
  public V getHigh(V root) {
    return resolver.getHigh(root);
  }

  /** {@inheritDoc} */
  @Override
  public V getLow(V root) {
    return resolver.getLow(root);
  }

  /** {@inheritDoc} */
  @Override
  public V getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public V getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    clear();
  }

  /** {@inheritDoc} */
  @Override
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public DD.Factory<V> getFactory() {
    return factory;
  }

  @Override
  public int nodeCount() {
    return table.nodeCount();
  }

  @Override
  public int size() {
    return table.size();
  }

  @Override
  public void cleanUnusedNodes() {
    resizeTable();
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    table = null;
    hashTable = null;
    nextTable = null;
  }
}
