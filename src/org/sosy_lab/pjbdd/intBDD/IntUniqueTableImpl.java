// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import java.util.function.IntConsumer;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * Main IntUniqueTable implementation. Concurrent IntUniqueTable implementation which underlying
 * uses a resizing int array as data structure. Does not guaranty threadsafe concurrent access.
 *
 * @author Stephan Holzner
 * @see IntUniqueTable
 * @since 1.0
 */
public class IntUniqueTableImpl implements IntUniqueTable {

  /** offset of reference count and level in table. */
  protected static final int OFFSET_VAR = 0;

  /** offset of low index in table. */
  protected static final int OFFSET_LOW = 1;

  /** offset of high index in table. */
  protected static final int OFFSET_HIGH = 2;

  /** offset of hash value in table. */
  protected static final int OFFSET_HASH = 3;

  /** offset of next index with same hash value in table. */
  protected static final int OFFSET_NEXT = 4;

  /** offset of reference count. */
  protected static final int OFFSET_REF_COUNT = 5;

  /** Max reference count indicator. */
  protected static final int MAX_REF = -1;

  /** each node's size in table. */
  protected final int nodeSize;

  /** bdd node table containing 5 or 6 entries for each node. */
  protected int[] table;

  /** Flag to disable reference counting and garbage collection. */
  protected final boolean disableGC;

  /** maximal array size. */
  protected final int maxSize;

  /** representation for next free allocated bdd node. */
  protected int nextFree;

  /** number of free allocated bdd nodes. */
  protected int freeCounter;

  /** number of allocated bdd nodes. */
  protected int nodeCount;

  /** resizing table's increase factor. */
  protected final int increaseFactor;

  /**
   * Creates a new {@link IntUniqueTableImpl} with specified initial size, parallelism and increase
   * factor.
   *
   * @param initialSize - the initial table size
   * @param increaseFactor - the table's increase factor
   * @param disableGC - flag to disable reference counting and garbage collection
   */
  public IntUniqueTableImpl(int initialSize, int increaseFactor, boolean disableGC) {
    maxSize = Integer.MAX_VALUE / (disableGC ? 5 : 6);
    this.disableGC = disableGC;
    this.nodeSize = disableGC ? 5 : 6;
    this.nodeCount = PrimeUtils.getGreaterNextPrime(initialSize);
    this.increaseFactor = increaseFactor;
    table = new int[this.nodeCount * nodeSize];

    for (int n = 2; n < this.nodeCount; n++) {
      setLow(n, INVALID_BDD);
      setNext(n, n + 1);
    }
    setNext(this.nodeCount - 1, 0);

    setMaxRef(0);
    setMaxRef(1);
    setLow(0, 0);
    setHigh(0, 0);
    setVariable(0, -1);
    setLow(1, 1);
    setHigh(1, 1);
    setVariable(1, -2);

    nextFree = 2;
    freeCounter = this.nodeCount - 2;
  }

  /** {@inheritDoc} */
  @Override
  public int getOrCreate(int var, int low, int high) {
    // Add reference for Nodes in use to avoid gbc while resize
    int res;
    tryResizing();
    int hash = hashNode(var, low, high);
    res = getNodeWithHash(hash);
    int head = res;
    while (res != 0) {
      if (getVariable(res) == var && getLow(res) == low && getHigh(res) == high) {
        return res;
      }
      res = getNext(res);
    }
    res = getNextFree();
    if (res < 2) {
      tryResizing();
      return getOrCreate(var, low, high);
    }
    createNode(res, low, high, var, hash, head);
    return res;
  }

  protected int getNextFree() {
    int next = nextFree;
    nextFree = getNext(next);
    return next;
  }

  protected boolean createNode(int node, int low, int high, int var, int hash, int head) {
    setHash(hash, node);
    setNext(node, head);
    setVariable(node, var);
    setLow(node, low);
    setHigh(node, high);
    freeCounter--;
    return true;
  }

  /** {@inheritDoc} */
  @Override
  public void printStats() {
    throw new UnsupportedOperationException("Statistic printing not yet implemented");
  }

  /**
   * Checks if table's reaches threshold and table resize must be performed.
   *
   * @return true if minimal threshold of free nodes reached
   */
  protected boolean checkResize() {
    return nextFree <= 1;
  }

  /** Try to resize table. Wait if resize op is already in progress. Collect all locks else. */
  private void tryResizing() {
    if (checkResize()) {
      resizeTable();
    }
  }

  /** Start table resize task. resolves new size and calls {@link #doResize(int, int)}. */
  protected void resizeTable() {
    int oldSize = nodeCount;
    int newSize = nodeCount;

    if (increaseFactor > 0) {
      newSize += newSize * increaseFactor;
    } else {
      newSize = newSize << 1;
    }

    if (newSize > maxSize) {
      newSize = maxSize;
    }

    doResize(oldSize, newSize);
  }

  protected void doResize(int oldSize, int newSize) {
    newSize = PrimeUtils.getLowerNextPrime(newSize);

    if (oldSize < newSize) {
      int[] newUniqueTable;
      newUniqueTable = new int[newSize * nodeSize];
      System.arraycopy(table, 0, newUniqueTable, 0, table.length);
      table = newUniqueTable;
    }
    nodeCount = newSize;
    int n;
    boolean[] marks = markTable(oldSize);

    for (n = 0; n < oldSize; n++) {
      if (!marks[n] && !disableGC) {
        setLow(n, INVALID_BDD);
      }
      setHash(n, 0);
      setNext(n, 0);
    }

    for (n = oldSize; n < nodeCount; n++) {
      setLow(n, INVALID_BDD);
    }
    rehash();
  }

  private boolean[] markTable(int oldsize) {
    boolean[] marks = new boolean[oldsize];
    if (!disableGC) {
      for (int i = oldsize - 1; i >= 0; i--) {
        if (hasRef(i)) {
          markNode(i, marks);
        }
      }
    }
    return marks;
  }

  private void markNode(int i, boolean[] marks) {
    if (!marks[i]) {
      marks[i] = true;
      markNode(getHigh(i), marks);
      markNode(getLow(i), marks);
    }
  }

  /** Rehash table entries after resize task, because hash value changes with table's size. */
  private void rehash() {
    nextFree = 0;
    freeCounter = nodeCount;

    for (int n = nodeCount - 1; n >= 2; n--) {
      if (getLow(n) != INVALID_BDD) {
        int hash;

        hash = hashNode(getVariable(n), getLow(n), getHigh(n));
        setNext(n, getNodeWithHash(hash));
        setHash(hash, n);
        freeCounter--;
      } else {
        setNext(n, nextFree);
        nextFree = n;
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setMaxRef(int node) {
    if (!disableGC) {
      table[node * nodeSize + OFFSET_REF_COUNT] = MAX_REF;
    }
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    table = null;
    table = new int[nodeCount * nodeSize];
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(IntConsumer function) {
    IntStream.range(0, nodeCount)
        .forEach(
            i -> {
              if (getLow(i) != INVALID_BDD) { // filter unused nodes
                function.accept(i);
              }
            });
  }

  /** {@inheritDoc} */
  @Override
  public void swap(int upper, int lower, NodeMaker delegate) {
    forEach(
        bdd -> {
          if (getVariable(bdd) == lower) {
            int oldHash = hashNode(bdd);

            int low = getLow(bdd);
            int high = getHigh(bdd);

            if (getVariable(low) == upper || getVariable(high) == upper) {
              int low0 = low;
              int low1 = low;
              int high0 = high;
              int high1 = high;

              if (getVariable(low) == upper) {
                low0 = getLow(low);
                low1 = getHigh(low);
              }

              if (getVariable(high) == upper) {
                high0 = getLow(high);
                high1 = getHigh(high);
              }

              low = delegate.makeNode(lower, low0, high0);
              high = delegate.makeNode(lower, low1, high1);
              decRef(getHigh(bdd));
              decRef(getLow(bdd));

              setVariable(bdd, upper);
              setHigh(bdd, high);
              setLow(bdd, low);
              rehash(bdd, oldHash);
            }
          }
        });
  }

  @Override
  public int getSize() {
    return nodeCount;
  }

  @Override
  public void cleanUnusedNodes() {
    doResize(nodeCount, nodeCount);
  }

  /**
   * Rehash bdd node after variable swap changes.
   *
   * @param bdd - node to be rehashed
   * @param oldHash - old hash code before swap
   */
  private void rehash(int bdd, int oldHash) {
    int newHash = hashNode(bdd);
    if (oldHash != newHash) {
      int res = getNodeWithHash(oldHash);
      int previous = res;
      while (res != bdd) {
        previous = res;
        res = getNext(res);
      }
      if (previous == res) { // bdd is result of getNodeWithHash
        setHash(oldHash, getNext(res));
      } else {
        setNext(previous, getNext(res));
      }
      setNext(bdd, getNodeWithHash(newHash));
      setHash(newHash, bdd);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void incRef(int... roots) {
    if (disableGC) {
      return;
    }
    for (int r : roots) {
      if (table[r * nodeSize + OFFSET_REF_COUNT] != MAX_REF) {
        table[r * nodeSize + OFFSET_REF_COUNT]++;
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setVariable(int node, int val) {
    table[node * nodeSize + OFFSET_VAR] = val;
  }

  /** {@inheritDoc} */
  @Override
  public int getHigh(int r) {
    if (r <= ONE) {
      return r;
    }
    return table[r * nodeSize + OFFSET_HIGH];
  }

  /** {@inheritDoc} */
  @Override
  public int getLow(int r) {
    if (r <= ONE) {
      return r;
    }
    return table[r * nodeSize + OFFSET_LOW];
  }

  /** {@inheritDoc} */
  @Override
  public int getVariable(int node) {
    return table[node * nodeSize + OFFSET_VAR];
  }

  /** {@inheritDoc} */
  @Override
  public int getNodeCount() {
    return nodeCount - freeCounter;
  }

  /** {@inheritDoc} */
  @Override
  public void decRef(int... roots) {
    if (disableGC) {
      return;
    }
    for (int r : roots) {
      int old = table[r * nodeSize + OFFSET_REF_COUNT];
      if (old > 0) {
        table[r * nodeSize + OFFSET_REF_COUNT]--;
      }
    }
  }

  protected boolean hasRef(int r) {
    return table[r * nodeSize + OFFSET_REF_COUNT] != 0;
  }

  /**
   * Calculates the hashcode for a node at given index.
   *
   * @param index - given bdd's index
   * @return hash code of given bdd
   */
  protected int hashNode(int index) {
    return hashNode(getVariable(index), getLow(index), getHigh(index));
  }

  /**
   * Set low for given bdd.
   *
   * @param root - the bdd
   * @param low - bdd
   */
  protected void setLow(int root, int low) {
    table[root * nodeSize + OFFSET_LOW] = low;
  }

  /**
   * Set high for given bdd.
   *
   * @param root - the bdd
   * @param high - bdd
   */
  protected void setHigh(int root, int high) {
    table[root * nodeSize + OFFSET_HIGH] = high;
  }

  /**
   * Lookup node with given hash value.
   *
   * @param hash - the hash value
   * @return node with given hash value
   */
  protected int getNodeWithHash(int hash) {
    return table[hash * nodeSize + OFFSET_HASH];
  }

  /**
   * Set hash for given index.
   *
   * @param hash bdd's hash value
   * @param index bdd's index
   */
  protected boolean setHash(int hash, int index) {
    table[hash * nodeSize + OFFSET_HASH] = index;
    return true;
  }

  protected int getNext(int root) {
    return table[root * nodeSize + OFFSET_NEXT];
  }

  /**
   * set next bdd with same hashcode.
   *
   * @param root bdd
   * @param next bdd with same hash
   */
  protected void setNext(int root, int next) {
    table[root * nodeSize + OFFSET_NEXT] = next;
  }

  /**
   * calculate hashcode for given input params.
   *
   * @param lvl - bdd's level
   * @param low - bdd's low branch
   * @param high - bdd's high branch
   * @return hashcode
   */
  protected int hashNode(int lvl, int low, int high) {
    return Math.abs(HashCodeGenerator.generateHashCode(lvl, low, high) % nodeCount);
  }
}
