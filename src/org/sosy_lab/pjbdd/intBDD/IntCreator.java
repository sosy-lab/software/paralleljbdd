// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import java.lang.ref.Reference;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.reference.IntHoldingWeakReference;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

/**
 * {@link Creator} implementation with ints as underlying bdd data structure.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public class IntCreator extends SerialIntCreator {

  /** demon thread to cleanup reclaimed entries. */
  private final CleanUpThread cleaner;

  /** Constructor creates and starts a new {@link CleanUpThread}. */
  public IntCreator(
      IntAlgorithm algorithm, IntSatAlgorithm satAlgorithm, IntNodeManager nodeManager) {
    super(algorithm, satAlgorithm, nodeManager);
    cleaner = new CleanUpThread();
    startCleaner();
  }

  /** Start cleanup thread. */
  protected void startCleaner() {
    cleaner.start();
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    algorithm.shutDown();
    cleaner.shutdown();
  }

  /**
   * convert internal index representation to BDD object for external use. Creates WeakReference for
   * object to modify reference count on garbage collection reclaim.
   *
   * @param id - bdd's index
   * @return created bdd object
   */
  @Override
  @SuppressWarnings("unused")
  protected DD makeBDD(int id) {
    BDDimpl bdd = new BDDimpl(id);
    new IntHoldingWeakReference<>(bdd, queue, id);
    return bdd;
  }

  /**
   * {@link ReclaimedReferenceCleaningThread} sub class to handle garbage collected bdd instances.
   *
   * @author Stephan Holzner
   * @see ReclaimedReferenceCleaningThread
   * @since 1.0
   */
  private final class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      Reference<? extends DD> sv = queue.remove(1000);
      if (sv != null) {
        if (sv instanceof IntHoldingWeakReference && !isShutdown()) {
          freeRef(((IntHoldingWeakReference<?>) sv).intValue());
        }
      }
    }
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }
}
