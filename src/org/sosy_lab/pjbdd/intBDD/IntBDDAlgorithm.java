// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import static org.sosy_lab.pjbdd.intBDD.IntAlgorithm.ApplyOp.OP_AND;
import static org.sosy_lab.pjbdd.intBDD.IntAlgorithm.ApplyOp.OP_NAND;
import static org.sosy_lab.pjbdd.intBDD.IntAlgorithm.ApplyOp.OP_OR;

import com.google.common.primitives.Ints;
import org.sosy_lab.pjbdd.api.CreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.intBDD.cache.IntNotCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntOpCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntQuantCache;
import org.sosy_lab.pjbdd.intBDD.cache.NotCacheData;
import org.sosy_lab.pjbdd.intBDD.cache.OpCacheData;
import org.sosy_lab.pjbdd.intBDD.cache.QuantCacheData;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * Serial {@link IntAlgorithm} implementation. Uses serial algorithms for bdd node operations.
 *
 * @author Stephan Holzner
 * @see IntAlgorithm
 * @since 1.0
 */
public class IntBDDAlgorithm implements IntAlgorithm {
  protected final IntOpCache opCache;
  protected final IntOpCache iteCache;
  protected final IntQuantCache quantCache;
  protected final IntNotCache notCache;
  protected final IntOpCache composeCache;

  /** the {@link IntNodeManager} implementation used for variable management. */
  protected final IntNodeManager nodeManager;

  /**
   * Creates a new {@link IntBDDAlgorithm} with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link CreatorBuilder} for instantiation.
   *
   * @param iteCache - the ite cache
   * @param opCache - the op cache
   * @param notCache - the not cache
   * @param nodeManager - the variable manager
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public IntBDDAlgorithm(
      IntOpCache opCache,
      IntOpCache iteCache,
      IntNotCache notCache,
      IntQuantCache quantCache,
      IntOpCache composeCache,
      IntNodeManager nodeManager) {
    this.notCache = notCache;
    this.iteCache = iteCache;
    this.opCache = opCache;
    this.quantCache = quantCache;
    this.composeCache = composeCache;
    this.nodeManager = nodeManager;
  }

  /** {@inheritDoc} */
  @Override
  public int makeNot(int bdd) {
    if (isZero(bdd)) {
      return makeTrue();
    }
    if (isOne(bdd)) {
      return makeFalse();
    }

    int res = notCacheLookup(bdd);
    if (res != -1) {
      return res;
    }

    int low = makeNot(low(bdd));
    int high = makeNot(high(bdd));
    res = makeNode(low, high, var(bdd));

    cacheNot(bdd, res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    opCache.clear();
    notCache.clear();
  }

  /** {@inheritDoc} */
  @Override
  public int makeFalse() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public int makeTrue() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public int makeNode(int low, int high, int var) {
    return nodeManager.makeNode(var, low, high);
  }

  /**
   * Perform terminal check for 'ITE' calculations.
   *
   * @param f - the if branch
   * @param g - the then branch
   * @param h - the else branch
   * @return result or -1
   */
  protected int iteCheck(int f, int g, int h) {
    if (isOne(f) || g == h) {
      return g;
    }

    if (isZero(f)) {
      return h;
    }

    if (isOne(g) && isZero(h)) {
      return f;
    }

    if (isZero(g) && isOne(h)) {
      return makeNot(f);
    }

    if (isOne(g)) {
      return makeOp(f, h, OP_OR);
    }
    if (isZero(g)) {
      return makeOp(makeNot(f), h, OP_AND);
    }

    if (isOne(h)) {
      return makeOp(makeNot(f), g, OP_OR);
    }
    if (isZero(h)) {
      return makeOp(f, g, OP_AND);
    }

    return iteCacheLookup(f, g, h);
  }

  /** {@inheritDoc} */
  @Override
  public int makeIte(int f, int g, int h) {
    int res = iteCheck(f, g, h);
    if (res != -1) {
      return res;
    }

    int topVar = topVarForLevels(level(f), level(g), level(h));

    int low =
        makeIte(
            restrictIf(f, topVar, makeFalse()),
            restrictIf(g, topVar, makeFalse()),
            restrictIf(h, topVar, makeFalse()));
    int high =
        makeIte(
            restrictIf(f, topVar, makeTrue()),
            restrictIf(g, topVar, makeTrue()),
            restrictIf(h, topVar, makeTrue()));
    res = makeNode(low, high, topVar);

    cacheIte(f, g, h, res);

    return res;
  }

  /** {@inheritDoc} */
  @Override
  public int makeOp(int f1, int f2, ApplyOp op) {
    int res = applyCheck(f1, f2, op);
    if (res != -1) {
      return res;
    }
    int topVar = topVarForLevels(level(f1), level(f2));

    int low = makeOp(restrictIf(f1, topVar, makeFalse()), restrictIf(f2, topVar, makeFalse()), op);
    int high = makeOp(restrictIf(f1, topVar, makeTrue()), restrictIf(f2, topVar, makeTrue()), op);
    res = makeNode(low, high, topVar);
    cacheApply(f1, f2, op, res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public int makeExquant(int f, int[] levels) {
    if (isConst(f) || (level(f) > levels[0] && levels.length == 1)) {
      return f;
    }

    int res = quantCheck(f, levels);
    if (res != -1) {
      return res;
    }

    int fLow = nodeManager.low(f);
    int fHigh = nodeManager.high(f);
    int fLevel = level(f);
    if (fLevel > levels[0]) {
      int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
      res = makeExquant(f, newTail);
    } else if (fLevel == levels[0]) {
      if (levels.length == 1) {
        res = makeOp(fHigh, fLow, OP_OR);
      } else {
        int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
        res = makeOp(makeExquant(fLow, newTail), makeExquant(fHigh, newTail), OP_OR);
      }
    } else {
      res = expandExquant(fLow, fHigh, levels, var(f));
    }

    cacheQuant(f, levels, res);

    return res;
  }

  /**
   * Helper function for lightweight parellelisation of exquant calls.
   *
   * @param fLow - the roots low
   * @param fHigh - the roots high
   * @param levels - the vars to be existential qunatified
   * @param var - the roots var
   * @return exist (f, var)
   */
  protected int expandExquant(int fLow, int fHigh, int[] levels, int var) {
    int high = makeExquant(fHigh, levels);
    int low = makeExquant(fLow, levels);
    return makeNode(low, high, var);
  }

  /** {@inheritDoc} */
  @Override
  public int makeExquant(int f, int level) {
    if (isConst(f) || level(f) > level) {
      return f;
    }
    int res = quantCheck(f, new int[] {level});
    if (res != -1) {
      return res;
    }
    if (level(f) == level) {
      res = makeOp(nodeManager.high(f), nodeManager.low(f), OP_OR);
    } else {
      int high = makeExquant(nodeManager.high(f), level);
      int low = makeExquant(nodeManager.low(f), level);
      res = makeNode(low, high, var(f));
    }

    cacheQuant(f, new int[] {level}, res);

    return res;
  }

  /** {@inheritDoc} */
  @Override
  public int restrict(int bdd, int var, boolean restrictionVar) {
    return var(bdd) != var ? bdd : restrictionVar ? high(bdd) : low(bdd);
  }

  @Override
  public int makeCompose(int f1, int v, int f2) {
    if (level(f1) > nodeManager.level(v)) {
      return f1;
    }

    int res = checkCache(f1, v, f2, composeCache);
    if (res != -1) {
      return res;
    }

    if (var(f1) == v) {
      res = makeIte(f2, high(f1), low(f1));
    } else {
      int i = makeCompose(high(f1), v, f2);
      int e = makeCompose(low(f1), v, f2);
      res = makeIte(makeIthVar(var(f1)), i, e);
    }
    cacheCompose(f1, v, f2, res);
    return res;
  }

  @Override
  public int getCacheSize() {
    return opCache.size();
  }

  @Override
  public int getCacheNodeCount() {
    return opCache.nodeCount();
  }

  /**
   * Get bdd representation for variable .
   *
   * @param var - the variable
   * @return - the bdd representation
   */
  protected int makeIthVar(int var) {
    return makeNode(makeFalse(), makeTrue(), var);
  }

  /**
   * Perform terminal check for 'APPLY' calculations.
   *
   * @param f1 - the getIf bdd
   * @param f2 - the getThen bdd
   * @param op - the operation to be applied
   * @return result or -1
   */
  protected int applyCheck(int f1, int f2, ApplyOp op) {
    switch (op) {
      case OP_OR:
        return orCheck(f1, f2);
      case OP_AND:
        return andCheck(f1, f2);
      case OP_XOR:
        return xorCheck(f1, f2);
      case OP_NAND:
        return nandCheck(f1, f2);
      case OP_NOR:
        return norCheck(f1, f2);
      case OP_IMP:
        return implyCheck(f1, f2);
      case OP_XNOR:
        return xnorCheck(f1, f2);
      default:
        break;
    }
    if (isConst(f1) && isConst(f2)) {
      return baseOprResults[op.ordinal()][f1 << 1 | f2];
    }

    return opCacheLookup(f1, f2, op);
  }

  /**
   * Nested base case and cache check for apply op 'XOR' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private int xorCheck(int f1, int f2) {
    if (f1 == f2) {
      return makeFalse();
    }
    if (isOne(f1)) {
      return makeNot(f2);
    }
    if (isOne(f2)) {
      return makeNot(f1);
    }
    if (isZero(f1)) {
      return f2;
    }
    if (isZero(f2)) {
      return f1;
    }
    return opCacheLookup(f1, f2, ApplyOp.OP_XOR);
  }

  /**
   * Nested base case and cache check for apply op 'NOR' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private int norCheck(int f1, int f2) {
    if (isOne(f1) || isOne(f2)) {
      return makeFalse();
    }
    if (isZero(f1) || f1 == f2) {
      return makeNot(f2);
    }
    if (isZero(f2)) {
      return makeNot(f1);
    }
    return opCacheLookup(f1, f2, ApplyOp.OP_NOR);
  }

  /**
   * Nested base case and cache check for apply op 'XNOR' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private int xnorCheck(int f1, int f2) {
    if (f1 == f2) {
      return makeTrue();
    }
    if (isZero(f1)) {
      return makeNot(f2);
    }
    if (isZero(f2)) {
      return makeNot(f1);
    }
    if (isOne(f1)) {
      return f2;
    }
    if (isOne(f2)) {
      return f1;
    }
    return opCacheLookup(f1, f2, ApplyOp.OP_XNOR);
  }

  /**
   * Nested base case and cache check for apply op 'NAND' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private int nandCheck(int f1, int f2) {
    if (isZero(f1) || isZero(f2)) {
      return makeTrue();
    }
    if (isOne(f1) || f1 == f2) {
      return makeNot(f2);
    }
    if (isOne(f2)) {
      return makeNot(f1);
    }

    return opCacheLookup(f1, f2, OP_NAND);
  }

  /**
   * Nested base case and cache check for apply op 'IMPLY' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private int implyCheck(int f1, int f2) {
    if (isZero(f1) || isOne(f2) || f1 == f2) {
      return makeTrue();
    }
    if (isOne(f1)) {
      return f2;
    }
    if (isZero(f2)) {
      return makeNot(f1);
    }

    return opCacheLookup(f1, f2, ApplyOp.OP_IMP);
  }

  /**
   * Perform terminal check for 'AND' calculations.
   *
   * @param f1 - the getIf bdd
   * @param f2 - the getThen bdd
   * @return result or -1
   */
  protected int andCheck(int f1, int f2) {
    if (f1 == f2) {
      return f1;
    }

    if (isZero(f1) || isZero(f2)) {
      return makeFalse();
    }
    if (isOne(f1)) {
      return f2;
    }
    if (isOne(f2)) {
      return f1;
    }
    return opCacheLookup(f1, f2, ApplyOp.OP_AND);
  }

  /**
   * Perform terminal check for 'OR' calculations.
   *
   * @param f1 - the getIf bdd
   * @param f2 - the getThen bdd
   * @return result or -1
   */
  protected int orCheck(int f1, int f2) {
    if (f1 == f2 || isZero(f2)) {
      return f1;
    }
    if (isOne(f1) || isOne(f2)) {
      return makeTrue();
    }
    if (isZero(f1)) {
      return f2;
    }

    return opCacheLookup(f1, f2, OP_OR);
  }

  /**
   * Lookup performed not computation of bdd.
   *
   * @param root - the bdd
   * @return cached computation or -1 if there is no cached computation
   */
  protected int notCacheLookup(int root) {
    NotCacheData e = notCache.get(root);
    if (e == null || e.getInput() != root) {
      return -1;
    }
    return e.getResult();
  }

  /**
   * Lookup performed ite computation of input triple.
   *
   * @param i - the if branch
   * @param t - the then branch
   * @param e - the else branch
   * @return cached computation or -1 if there is no cached computation
   */
  protected int iteCacheLookup(int i, int t, int e) {
    return checkCache(i, t, e, iteCache);
  }

  /**
   * Lookup performed apply computation of input triple.
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the apply operation
   * @return cached computation or -1 if there is no cached computation
   */
  protected int opCacheLookup(int f1, int f2, ApplyOp op) {
    return checkCache(f1, f2, op.ordinal(), opCache);
  }

  /**
   * Save computed apply operation in cash (increases bdds' reference count).
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the applied operator
   * @param res - the computed result
   */
  protected void cacheApply(int f1, int f2, ApplyOp op, int res) {
    cacheApply(f1, f2, op.ordinal(), res);
  }

  /**
   * Save computed apply operation in cash (increases bdds' reference count).
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the applied operator
   * @param res - the computed result
   */
  protected void cacheApply(int f1, int f2, int op, int res) {
    int hash = hash(f1, f2, op);
    nodeManager.addRefs(f1, f2, res);
    OpCacheData entry = opCache.createEntry(f1, f2, op, res);
    OpCacheData old = opCache.put(hash, entry);
    if (old != null) {
      nodeManager.freeRefs(old.f1(), old.f2(), old.f3());
    }
  }

  /**
   * Save computed compose operation in cash (increases bdds' reference count).
   *
   * @param f1 - the getIf argument
   * @param v - the getThen argument
   * @param f2 - the applied operator
   * @param res - the computed result
   */
  protected void cacheCompose(int f1, int v, int f2, int res) {
    int hash = hash(f1, v, f2);
    nodeManager.addRefs(f1, v, f2, res);
    OpCacheData entry = opCache.createEntry(f1, v, f2, res);
    OpCacheData old = opCache.put(hash, entry);
    if (old != null) {
      nodeManager.freeRefs(old.f1(), old.f2(), old.f3(), old.result());
    }
  }

  /**
   * Save computed not in cash (increases bdds' reference count).
   *
   * @param root - the getIf argument
   * @param res - the computed result
   */
  protected void cacheNot(int root, int res) {
    nodeManager.addRefs(root, res);
    NotCacheData old = notCache.put(root, res);
    if (old != null) {
      nodeManager.freeRefs(root, res);
    }
  }

  /**
   * Save computed ite operation in cash (increases bdds' reference count).
   *
   * @param i - the if branch
   * @param t - the then branch
   * @param e - the else branch
   * @param res - the computed result
   */
  protected void cacheIte(int i, int t, int e, int res) {
    int applyHash = hash(i, t, e);
    nodeManager.addRefs(i, t, e, res);
    OpCacheData old = iteCache.put(applyHash, iteCache.createEntry(i, t, e, res));
    if (old != null) {
      nodeManager.freeRefs(old.f1(), old.f2(), old.f3(), old.result());
    }
  }

  /**
   * Save computed ex quant operation in cash (increases bdds' reference count).
   *
   * @param f - the if branch
   * @param levels - the levels
   * @param res - the computed result
   */
  protected void cacheQuant(int f, int[] levels, int res) {
    int applyHash = hash(f, levels[0]);
    nodeManager.addRefs(f, res);
    QuantCacheData old = quantCache.put(applyHash, quantCache.createEntry(f, levels, res));
    if (old != null) {
      nodeManager.freeRefs(old.f1(), old.result());
    }
  }

  /**
   * check given cache for computation with given input triple.
   *
   * @param a - getIf argument
   * @param b - getThen argument
   * @param c - getElse argument
   * @param cache - to be checked
   * @return cached computation or -1 if there is no cached computation
   */
  protected int checkCache(int a, int b, int c, IntOpCache cache) {
    int hash = hash(a, b, c);
    OpCacheData data = cache.get(hash);
    if (data == null) {
      return -1;
    }
    if (data.f1() == a && data.f2() == b && data.f3() == c) {
      return data.result();
    }
    return -1;
  }

  /**
   * check quant cache for computation with given input tuple.
   *
   * @param f - getIf argument
   * @param levels - to be checked
   * @return cached computation or -1 if there is no cached computation
   */
  protected int quantCheck(int f, int[] levels) {
    int hash = hash(f, levels[0]);
    QuantCacheData data = quantCache.get(hash);
    if (data == null) {
      return -1;
    }
    if (data.matches(f, levels)) {
      return data.result();
    }
    return -1;
  }

  /**
   * Get variable level for bdd.
   *
   * @param r - the bdd
   * @return level of r's variable
   */
  protected int level(int r) {
    return nodeManager.level(var(r));
  }

  /**
   * Return the topmost level of a various number of bdds.
   *
   * @param levels - the variable levels
   * @return topmost level
   */
  protected int topVarForLevels(int... levels) {
    return nodeManager.varForLevel(Ints.min(levels));
  }

  /**
   * Restrict root to low or high branch if root's level is equal to given level.
   *
   * @param root - the bdd
   * @param var - the restriction var
   * @param restrictWith - the restrict to low or high
   * @return restricted bdd
   */
  protected int restrictIf(int root, int var, int restrictWith) {
    return var(root) != var ? root : isOne(restrictWith) ? high(root) : low(root);
  }

  /**
   * hash input triple.
   *
   * @param a - getIf value
   * @param b - getThen value
   * @param c - getElse value
   * @return hash value
   */
  protected int hash(int a, int b, int c) {
    return HashCodeGenerator.generateHashCode(a, b, c);
  }

  /**
   * hash input tuple.
   *
   * @param a - getIf value
   * @param b - getThen value
   * @return hash value
   */
  protected int hash(int a, int b) {
    return HashCodeGenerator.generateHashCode(a, b);
  }

  /** Operator results - entry = left<<1 | right (left,right in {0,1}). */
  private static final int[][] baseOprResults = {
    {0, 0, 0, 1}, /* and                       ( & )         */
    {0, 1, 1, 0}, /* xor                       ( ^ )         */
    {0, 1, 1, 1}, /* or                        ( | )         */
    {1, 1, 1, 0}, /* nand                                    */
    {1, 0, 0, 0}, /* nor                                     */
    {1, 1, 0, 1}, /* implication               ( >> )        */
    {1, 0, 0, 1}, /* bi-implication                          */
    {0, 0, 1, 0}, /* difference /greater than  ( - ) ( > )   */
    {0, 1, 0, 0}, /* less than                 ( < )         */
    {1, 0, 1, 1}, /* inverse implication       ( << )        */
    {1, 1, 0, 0}, /* not                       ( ! )         */
  };

  /**
   * check if a bdd represents the logical false representation.
   *
   * @param root - the bdd
   * @return true if root is zero
   */
  protected boolean isZero(int root) {
    return root == makeFalse();
  }

  /**
   * check if a bdd represents the logical true representation.
   *
   * @param root - the bdd
   * @return if root is one
   */
  protected boolean isOne(int root) {
    return root == makeTrue();
  }

  /**
   * check whether a bdd represents the logical true or false representation.
   *
   * @param root - the bdd
   * @return if root is one or zero
   */
  protected boolean isConst(int root) {
    return root <= makeTrue() && root >= makeFalse();
  }

  /**
   * get low branch bdd of root.
   *
   * @param root - the root bdd
   * @return low branch of root
   */
  protected int low(int root) {
    return nodeManager.low(root);
  }

  /**
   * get high branch bdd of root.
   *
   * @param root - the root bdd
   * @return high branch of root
   */
  protected int high(int root) {
    return nodeManager.high(root);
  }

  /**
   * get bdd variable of root.
   *
   * @param root - the root bdd
   * @return variable of root
   */
  protected int var(int root) {
    return nodeManager.var(root);
  }
}
