// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import com.google.common.collect.ImmutableSet;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.cache.Cache;

/**
 * Int based sat algorithm implementation. Uses serial algorithms for bdd sat operations.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public class IntSatAlgorithm {

  /** the node manager instance. */
  private final IntNodeManager nodeManager;

  /** sat count cache for computed results. */
  private final Cache<Integer, BigInteger> satCache;

  /**
   * Creates new {@link IntSatAlgorithm} instances with.
   *
   * @param nodeManager - the node managing component
   * @param satCache - the sat cache
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public IntSatAlgorithm(IntNodeManager nodeManager, Cache<Integer, BigInteger> satCache) {
    this.nodeManager = nodeManager;
    this.satCache = satCache;
  }

  /**
   * Takes a bdd and finds any satisfying variable assignment.
   *
   * @param root - the bdd node
   * @return one satisfying variable assignment.
   */
  public int anySat(int root) {
    if (isConst(root)) {
      return root;
    }
    if (nodeManager.getFalse() == low(root)) {
      return nodeManager.makeNode(var(root), nodeManager.getFalse(), anySat(high(root)));
    } else {
      return nodeManager.makeNode(var(root), anySat(low(root)), nodeManager.getFalse());
    }
  }

  /**
   * Get {@link DD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param root - the {@link DD} node
   * @return the number of satisfying variable assignments.
   */
  public BigInteger satCount(int root) {
    return satCountRec(root, varCount(root), ImmutableSet.of());
  }

  /**
   * helper to count number of nodes from DD.
   *
   * @param root - a root node
   * @return - Number of nodes
   */
  public int varCount(int root) {
    Set<Integer> vars = new HashSet<>();
    varCountRec(root, vars);
    return vars.size();
  }

  /**
   * recursive calculation of number of nodes.
   *
   * @param root - a root node
   * @param foundUniqueVariables - Set of all nodes
   */
  protected void varCountRec(int root, Set<Integer> foundUniqueVariables) {
    if (root == 1 || root == 0) {
      return;
    }

    foundUniqueVariables.add(var(root));
    varCountRec(high(root), foundUniqueVariables);
    varCountRec(low(root), foundUniqueVariables);
  }

  /**
   * Recursive calculation of number of possible satisfying truth assignments for a given root.
   *
   * @param root - a root node
   * @return root's number of possible satisfying truth assignments
   */
  protected BigInteger satCountRec(
      int root, int variablesCount, Set<Integer> foundUniqueVariables) {
    if (root == -0) {
      return BigInteger.ZERO;
    }
    if (root == 1) {
      return BigInteger.ONE.multiply(
          BigInteger.TWO.pow(variablesCount - foundUniqueVariables.size()));
    }
    BigInteger cached = satCache.get(root);
    if (cached != null) {
      // TODO: Think about caching again.
      // return cached;
    }

    BigInteger size =
        satCountRec(
            high(root),
            variablesCount,
            ImmutableSet.<Integer>builder().addAll(foundUniqueVariables).add(root).build());
    size =
        size.add(
            satCountRec(
                low(root),
                variablesCount,
                ImmutableSet.<Integer>builder().addAll(foundUniqueVariables).add(root).build()));

    satCache.put(root, size);

    return size;
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param root - the bdd
   * @return level of root
   */
  @SuppressWarnings("unused")
  private int level(int root) {
    return nodeManager.level(var(root));
  }

  /**
   * helper call to determine bdd's variable.
   *
   * @param root - the bdd
   * @return variable of root
   */
  private int var(int root) {
    return nodeManager.var(root);
  }

  /**
   * helper call to determine bdd's high branch successor.
   *
   * @param root - the bdd
   * @return high successor of root
   */
  private int high(int root) {
    return nodeManager.high(root);
  }

  /**
   * helper call to determine bdd's low branch successor.
   *
   * @param root - the bdd
   * @return low successor of root
   */
  private int low(int root) {
    return nodeManager.low(root);
  }

  /**
   * helper call to determine if root is a terminal node.
   *
   * @param root - the bdd
   * @return is root terminal node
   */
  private boolean isConst(int root) {
    return root == nodeManager.getFalse() || root == nodeManager.getTrue();
  }
}
