// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import org.sosy_lab.pjbdd.api.DD;

/** Interface for algorithm implementations to perform BDD manipulation. */
public interface IntAlgorithm {

  /**
   * Performs binary BDD manipulation operations.
   *
   * @param f1 - the first bdd node
   * @param f2 - the second bdd node
   * @param op - the operation to be performed
   * @return f1 'op' f2
   */
  int makeOp(int f1, int f2, ApplyOp op);

  /**
   * Performs if then else computation on input triple.
   *
   * @param f1 - the if bdd node
   * @param f2 - the then bdd node
   * @param f3 - the else bdd node
   * @return ite (f1, f2, f3)
   */
  int makeIte(int f1, int f2, int f3);

  /**
   * Negates bdd argument.
   *
   * @param f - the bdd argument
   * @return not f
   */
  int makeNot(int f);

  /** shutdown algorithm and corresponding components. */
  void shutDown();

  /**
   * Make logical false bdd representation.
   *
   * @return false bdd
   */
  int makeFalse();

  /**
   * Make logical true bdd representation.
   *
   * @return true bdd
   */
  int makeTrue();

  /**
   * Create a new node with given input triple or return a matching existing.
   *
   * @param low - low branch bdd
   * @param high - high branch bdd
   * @param var - bdd variable
   * @return new or matching bdd
   */
  int makeNode(int low, int high, int var);

  /**
   * Existential quantify f1 with all variables in f2.
   *
   * @param f1 - the first bdd
   * @param f2 - the second bdd
   * @return exquant f1 f2
   */
  int makeExquant(int f1, int f2);

  /**
   * Existential quantify f1 with all variables in f2.
   *
   * @param f1 - the first bdd
   * @param f2 - the second bdd
   * @return exquant f1 f2
   */
  int makeExquant(int f1, int[] f2);

  /**
   * Restrict a given variable in a given bdd to high or low branch.
   *
   * @param bdd - given bdd
   * @param var - given variable
   * @param restrictionVar - high or low branch
   * @return restricted bdd
   */
  int restrict(int bdd, int var, boolean restrictionVar);

  /**
   * Creates a {@link DD} representing a modification of the #f1 argument. Substitute #var
   * withfunction #f2
   *
   * @param f1 - {@link DD} argument
   * @param v - variable to be substituted
   * @param f2 - function substitute
   * @return compose(f1,var,f2)
   */
  int makeCompose(int f1, int v, int f2);

  int getCacheSize();

  int getCacheNodeCount();

  /**
   * Enum with 'Apply' operation types. Each type belongs to one binary method {@link ApplyOp}s
   * corresponding to following methods.
   *
   * <ul>
   *   <li>{@link ApplyOp#OP_AND}: Perform boolean AND for to arguments
   *   <li>{@link ApplyOp#OP_XOR}: Perform boolean XOR for to arguments
   *   <li>{@link ApplyOp#OP_OR}: Perform boolean OR for to arguments
   *   <li>{@link ApplyOp#OP_NAND}: Perform boolean NAND for to arguments
   *   <li>{@link ApplyOp#OP_NOR}: Perform boolean NOR for to arguments
   *   <li>{@link ApplyOp#OP_IMP}: Perform boolean IMP for to arguments
   *   <li>{@link ApplyOp#OP_XNOR}: Perform boolean OP_XNOR for to arguments
   * </ul>
   *
   * @author Stephan Holzner
   * @version 1.0
   */
  enum ApplyOp {
    OP_AND,
    OP_XOR,
    OP_OR,
    OP_NAND,
    OP_NOR,
    OP_IMP,
    OP_XNOR
  }
}
