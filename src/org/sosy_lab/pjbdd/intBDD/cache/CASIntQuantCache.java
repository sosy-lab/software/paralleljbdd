// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD.cache;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;

/**
 * Int operations cache implementation with underlying arrays and cas varhandle.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class CASIntQuantCache extends IntQuantCache {

  /** Use Unsafe for atomic int swap operations. */
  private final VarHandle arrayHandle;

  /**
   * Creates new operations cache with given parameters.
   *
   * @param size - the cache size
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "CT_CONSTRUCTOR_THROW",
      justification = "intentional design")
  public CASIntQuantCache(int size) {
    super(size);
    arrayHandle = MethodHandles.arrayElementVarHandle(QuantCacheData[].class);
  }

  /**
   * Get saved entry for given hash.
   *
   * @param hash - the hash
   * @return the saved entry, may return null
   */
  @Override
  public QuantCacheData get(int hash) {
    int index = Math.abs(hash % table.length);
    return (QuantCacheData) arrayHandle.get(table, index);
  }

  /**
   * Put a computed operation in cache.
   *
   * @param hash - the input value
   * @param data - the resulting
   * @return old value if hash collision occurs else null
   */
  @Override
  public QuantCacheData put(int hash, QuantCacheData data) {
    int index = Math.abs(hash % table.length);
    QuantCacheData old = (QuantCacheData) arrayHandle.get(table, index);

    while (!arrayHandle.weakCompareAndSet(table, index, old, data)) {
      old = (QuantCacheData) arrayHandle.get(table, index);
      if (old == data) {
        throw new IllegalStateException("CAS FAILED SPURIOSLY");
      }
    }

    return old;
  }
}
