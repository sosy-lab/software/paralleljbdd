// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD.cache;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;

/**
 * {@link CASIntNotCache} implementation with underlying arrays and cas varhandle.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public class CASIntNotCache extends IntNotCache {

  private final VarHandle arrayHandle;

  /**
   * Creates new Cache instances.
   *
   * @param size - the cache size
   */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "CT_CONSTRUCTOR_THROW",
      justification = "intentional design")
  public CASIntNotCache(int size) {
    super(size);
    arrayHandle = MethodHandles.arrayElementVarHandle(NotCacheData[].class);
  }

  /**
   * Lookup a entry for given hash.
   *
   * @param hash - the given hash
   * @return found entry or -1
   */
  @Override
  public NotCacheData get(int hash) {
    int index = Math.abs(hash % table.length);
    return ((NotCacheData) arrayHandle.get(table, index));
  }

  /**
   * Put a computed not operation in cache.
   *
   * @param input - the input value
   * @param res - the resulting negation
   * @return old value if hash collision occurs else -1
   */
  @Override
  public NotCacheData put(int input, int res) {
    int index = Math.abs(input % table.length);
    NotCacheData old = (NotCacheData) arrayHandle.get(table, index);
    NotCacheData data = new NotCacheData(input, res);
    while (!arrayHandle.weakCompareAndSet(table, index, old, data)) {
      old = (NotCacheData) arrayHandle.get(table, index);
      if (old == data) {
        throw new IllegalStateException("CAS FAILED SPURIOSLY");
      }
    }
    return old;
  }
}
