// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD.cache;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * Int operations cache implementation with underlying arrays.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class IntQuantCache {

  /** the backing array used for caching. */
  protected final QuantCacheData[] table;

  /**
   * Creates new operations cache with given parameters.
   *
   * @param size - the cache size
   */
  public IntQuantCache(int size) {
    size = PrimeUtils.getGreaterNextPrime(size);
    table = new QuantCacheData[size];
  }

  /**
   * Get saved entry for given hash.
   *
   * @param hash - the hash
   * @return the saved entry, may return null
   */
  public QuantCacheData get(int hash) {
    int index = Math.abs(hash % table.length);
    return table[index];
  }

  /**
   * Put a computed operation in cache.
   *
   * @param hash - the input value
   * @param data - the resulting
   * @return old value if hash collision occurs else null
   */
  public QuantCacheData put(int hash, QuantCacheData data) {
    int index = Math.abs(hash % table.length);
    QuantCacheData old = table[index];
    table[index] = data;
    return old;
  }

  /** Clear all entries. */
  public void clear() {
    IntStream.range(0, table.length).forEach(i -> table[i] = null);
  }

  /**
   * Create new cache entry.
   *
   * @param f1 - first argument
   * @param f2 - second argument
   * @param res - operation result
   * @return new cache entry
   */
  public QuantCacheData createEntry(int f1, int[] f2, int res) {
    return new QuantCacheData(f1, f2, res);
  }

  public int size() {
    return table.length;
  }

  public int nodeCount() {
    return (int) Arrays.stream(table).filter(Objects::nonNull).count();
  }
}
