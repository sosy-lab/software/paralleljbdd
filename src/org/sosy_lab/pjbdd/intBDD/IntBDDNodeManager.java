// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import com.google.common.collect.BiMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Main and simple {@link IntNodeManager} implementation.
 *
 * @author Stephan Holzner
 * @see IntNodeManager
 * @since 1.0
 */
public class IntBDDNodeManager implements IntNodeManager {

  /** number of defined BDD variables. */
  private int varCount;

  /** Backing {@link BiMap} for storing level and variable relation. */
  private int[] levelVar;

  private int[] varLevel;

  /** Set of defined BDD variables. */
  private List<Integer> varSet;

  /** backing unique table for node storage. */
  private final IntUniqueTable uniqueTable;

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public IntBDDNodeManager(IntUniqueTable intUniqueTable) {
    this.uniqueTable = intUniqueTable;
  }

  /** {@inheritDoc} */
  @Override
  public int varForLevel(int level) {
    return (level >= varCount) ? varCount : levelVar[level];
  }

  /** {@inheritDoc} */
  @Override
  public void setVarCount(int count) {
    if (count < 1 || count <= varCount) {
      return;
    }
    int oldCount = varCount;
    varCount = count;

    if (varSet == null) {
      varSet = new ArrayList<>();
    }

    if (levelVar == null) {
      levelVar = new int[varCount];
      varLevel = new int[varCount];
    } else {
      int[] newLevelVar = new int[varCount];
      int[] newVarLevel = new int[varCount];
      System.arraycopy(levelVar, 0, newLevelVar, 0, levelVar.length);
      System.arraycopy(varLevel, 0, newVarLevel, 0, levelVar.length);
      levelVar = newLevelVar;
      varLevel = newVarLevel;
    }

    for (; oldCount < count; oldCount++) {
      varSet.add(makeNode(oldCount, 0, 1));
      varSet.add(makeNode(oldCount, 1, 0));

      setMaxRef(varSet.get(oldCount * 2));
      setMaxRef(varSet.get(oldCount * 2 + 1));
      levelVar[oldCount] = oldCount;
      varLevel[oldCount] = oldCount;
    }
  }

  /** {@inheritDoc} */
  @Override
  public int getVarCount() {
    return varCount;
  }

  /** {@inheritDoc} */
  @Override
  public int[] getCurrentOrdering() {
    return Arrays.copyOf(levelVar, levelVar.length);
  }

  /** {@inheritDoc} */
  @Override
  public boolean checkLvl(int level) {
    return (level < varCount);
  }

  /** {@inheritDoc} */
  @Override
  public void setVarOrdering(int... order) {
    // Fill up existing order

    // actually reorder
    IntStream.range(0, order.length)
        .forEach(
            i -> {
              if (order[i] != levelVar[i]) {
                swapVarToLevel(order[i], i);
              }
            });
  }

  /** {@inheritDoc} */
  @Override
  public int level(int variable) {
    return (variable >= varCount || variable < 0) ? varCount : varLevel[variable];
  }

  /** {@inheritDoc} */
  @Override
  public void setMaxRef(int root) {
    uniqueTable.setMaxRef(root);
  }

  /** {@inheritDoc} */
  @Override
  public void swap(int upper, int lower) {
    uniqueTable.swap(upper, lower, this::makeNode);
  }

  /**
   * Swaps a specific variable up or down to a certain level.
   *
   * @param var - the variable to be swapped
   * @param level - the target level
   */
  private void swapVarToLevel(int var, int level) {
    int levelOfVar = varLevel[var];
    while (levelOfVar != level) {
      if (levelOfVar < level) {
        swapLevel(levelOfVar, ++levelOfVar);
      } else {
        swapLevel(levelOfVar, --levelOfVar);
      }
    }
  }

  @Override
  public int makeVariableBefore(int var) {
    int newVariable = getVarCount();
    int newNode = makeNode(newVariable, getFalse(), getTrue());
    int newLevel = level(var);
    // shift all existing elements >= var by 1
    for (int i = levelVar.length - 2; i + 1 > newLevel; i--) {
      int newVarIndex = levelVar[i];
      levelVar[i + 1] = levelVar[i];
      varLevel[newVarIndex] = i + 1;
    }

    levelVar[newLevel] = newVariable;
    varLevel[newVariable] = newLevel;
    return newNode;
  }

  @Override
  public void cleanUnusedNodes() {
    uniqueTable.cleanUnusedNodes();
  }

  /**
   * Swaps variables by given variable levels.
   *
   * @param levelA - getIf variable's level
   * @param levelB - getThen variable's level
   */
  private void swapLevel(int levelA, int levelB) {
    int varA = levelVar[levelA];
    int varB = levelVar[levelB];
    varLevel[varA] = levelB;
    varLevel[varB] = levelA;
    levelVar[levelB] = varA;
    levelVar[levelA] = varB;
    swap(varA, varB);
  }

  /** {@inheritDoc} */
  @Override
  public void addRefs(int... roots) {
    uniqueTable.incRef(roots);
  }

  /** {@inheritDoc} */
  @Override
  public void freeRefs(int... roots) {
    uniqueTable.decRef(roots);
  }

  /** {@inheritDoc} */
  @Override
  public int makeNode(int var, int low, int high) {

    if (low == high) {
      return low;
    }
    if (!checkLvl(var)) {
      this.setVarCount(var + 1);
    }
    uniqueTable.incRef(low, high);

    int res = uniqueTable.getOrCreate(var, low, high);
    addRefs(res);
    freeRefs(low, high);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public int low(int r) {
    return uniqueTable.getLow(r);
  }

  /** {@inheritDoc} */
  @Override
  public int high(int r) {
    return uniqueTable.getHigh(r);
  }

  /** {@inheritDoc} */
  @Override
  public int var(int root) {
    return uniqueTable.getVariable(root);
  }

  /** {@inheritDoc} */
  @Override
  public int getFalse() {
    return IntUniqueTable.ZERO;
  }

  /** {@inheritDoc} */
  @Override
  public int getTrue() {
    return IntUniqueTable.ONE;
  }

  @Override
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI",
      justification = "intentional design")
  public IntUniqueTable getNodeTable() {
    return uniqueTable;
  }

  @Override
  public int getNodeCount() {
    return uniqueTable.getNodeCount();
  }

  @Override
  public int getUniqueTableSize() {
    return uniqueTable.getSize();
  }
}
