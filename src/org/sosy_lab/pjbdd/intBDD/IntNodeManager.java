// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

/**
 * interface for int based variable management.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public interface IntNodeManager {

  /**
   * Get the bdd variable at given level.
   *
   * @param level - given level
   * @return variable for given level
   */
  int varForLevel(int level);

  /**
   * set variable count and create bdd variables.
   *
   * @param count - variable count
   */
  void setVarCount(int count);

  /**
   * get number of created bdd variables.
   *
   * @return the number of created bdd variables
   */
  int getVarCount();

  /**
   * Get the current bdd variable ordering.
   *
   * @return the current bdd variable ordering
   */
  int[] getCurrentOrdering();

  /**
   * Check if the given level already exists.
   *
   * @param level - the level to be checked
   * @return <code>true</code> if the level exists, <code>false</code> else
   */
  boolean checkLvl(int level);

  /**
   * Set variable ordering.
   *
   * @param order - the new order
   */
  void setVarOrdering(int... order);

  /**
   * Get the level of a given bdd variable at given level.
   *
   * @param variable - given variable
   * @return level for given variable
   */
  int level(int variable);

  /**
   * set root's reference count to max.
   *
   * @param root - a bdd
   */
  void setMaxRef(int root);

  /**
   * swap two levels.
   *
   * @param upper - the new upper level
   * @param lower - the new lower level
   */
  void swap(int upper, int lower);

  /**
   * add a reference on various number of bdd arguments.
   *
   * @param roots - bdds to be referenced
   */
  void addRefs(int... roots);

  /**
   * free a various number of bdd arguments.
   *
   * @param roots - bdds to be freed
   */
  void freeRefs(int... roots);

  /**
   * make bdd with given parameters.
   *
   * @param var - variables
   * @param low - low branch
   * @param high - high branch
   * @return resulting bdd
   */
  int makeNode(int var, int low, int high);

  /**
   * get low bdd for root r.
   *
   * @param r - the root bdd
   * @return low (r)
   */
  int low(int r);

  /**
   * get high bdd for root r.
   *
   * @param r - the root bdd
   * @return high (r)
   */
  int high(int r);

  /**
   * get variable of bdd root.
   *
   * @param root - the root bdd
   * @return var (r)
   */
  int var(int root);

  /**
   * get logical false bdd representation.
   *
   * @return false bdd
   */
  int getFalse();

  /**
   * get logical true bdd representation.
   *
   * @return true bdd
   */
  int getTrue();

  IntUniqueTable getNodeTable();

  int getNodeCount();

  int getUniqueTableSize();

  int makeVariableBefore(int variable);

  void cleanUnusedNodes();
}
