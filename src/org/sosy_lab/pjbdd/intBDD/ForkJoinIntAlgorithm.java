// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.intBDD.cache.IntNotCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntOpCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntQuantCache;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * Concurrent {@link IntAlgorithm} implementation. Uses concurrent algorithms for bdd node
 * operations.
 *
 * @author Stephan Holzner
 * @see IntBDDAlgorithm
 * @see IntAlgorithm
 * @since 1.0
 */
public class ForkJoinIntAlgorithm extends IntBDDAlgorithm {

  /** Worker thread pool manager used for concurrent bdd operations. */
  private final ParallelismManager parallelismManager;

  /**
   * Creates a new {@link ForkJoinIntAlgorithm} with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param iteCache - the ite cache
   * @param opCache - the op cache
   * @param notCache - the not cache
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public ForkJoinIntAlgorithm(
      IntOpCache opCache,
      IntOpCache iteCache,
      IntNotCache notCache,
      IntQuantCache quantCache,
      IntOpCache composeCache,
      IntNodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(opCache, iteCache, notCache, quantCache, composeCache, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("resource")
  public int makeIte(int f, int g, int h) {
    int top = topVarForLevels(level(f), level(g), level(h));
    int res = iteCheck(f, g, h);
    if (res != -1) {
      return res;
    }
    if (parallelismManager.canFork(top)) {

      parallelismManager.taskSupplied();
      ForkJoinPool service = parallelismManager.getThreadPool();
      ForkJoinTask<Integer> lowTask =
          service.submit(
              () ->
                  makeIte(
                      restrictIf(f, top, makeFalse()),
                      restrictIf(g, top, makeFalse()),
                      restrictIf(h, top, makeFalse())));
      ForkJoinTask<Integer> highTask =
          service.submit(
              () ->
                  makeIte(
                      restrictIf(f, top, makeTrue()),
                      restrictIf(g, top, makeTrue()),
                      restrictIf(h, top, makeTrue())));
      int high = highTask.join();
      int low = lowTask.join();
      parallelismManager.taskDone();
      res = makeNode(top, low, high);

    } else {
      int low =
          makeIte(
              restrictIf(f, top, makeFalse()),
              restrictIf(g, top, makeFalse()),
              restrictIf(h, top, makeFalse()));
      int high =
          makeIte(
              restrictIf(f, top, makeTrue()),
              restrictIf(g, top, makeTrue()),
              restrictIf(h, top, makeTrue()));
      res = makeNode(low, high, top);
    }
    cacheIte(f, g, h, res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public int makeOp(int f1, int f2, ApplyOp op) {
    int res = applyCheck(f1, f2, op);
    if (res != -1) {
      return res;
    }

    int topVar = topVarForLevels(level(f1), level(f2));
    int lowF1 = (level(f1) <= level(f2)) ? low(f1) : f1;
    int lowF2 = (level(f2) <= level(f1)) ? low(f2) : f2;
    int highF1 = (level(f1) <= level(f2)) ? high(f1) : f1;
    int highF2 = (level(f2) <= level(f1)) ? high(f2) : f2;

    // avoid terminal case fork
    int low = applyCheck(lowF1, lowF2, op);
    int high = applyCheck(highF1, highF2, op);

    if (parallelismManager.canFork(topVar) && low == -1 && high == -1) {
      res = asyncExpand(lowF1, lowF2, highF1, highF2, op, topVar);
    } else {
      if (low == -1) {
        low = makeOp(lowF1, lowF2, op);
      }
      if (high == -1) {
        high = makeOp(highF1, highF2, op);
      }
      res = makeNode(low, high, topVar);
    }
    cacheApply(f1, f2, op, res);
    return res;
  }

  /**
   * async shannon expansion of given operations.
   *
   * @param lF1 the getIf low argument
   * @param lF2 the getThen low argument
   * @param hF1 the getIf low argument
   * @param hF2 the getThen low argument
   * @param op the operation to be applied
   * @param top topVar most variable
   * @return computed bdd
   */
  @SuppressWarnings("resource")
  private int asyncExpand(int lF1, int lF2, int hF1, int hF2, ApplyOp op, int top) {
    parallelismManager.taskSupplied();
    parallelismManager.taskSupplied();
    ForkJoinPool service = parallelismManager.getThreadPool();
    ForkJoinTask<Integer> lowTask = service.submit(() -> makeOp(lF1, lF2, op));
    ForkJoinTask<Integer> highTask = service.submit(() -> makeOp(hF1, hF2, op));
    int high = highTask.join();
    int low = lowTask.join();
    parallelismManager.taskDone();
    parallelismManager.taskDone();
    return makeNode(low, high, top);
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    super.shutDown();
    parallelismManager.shutdown();
  }
}
