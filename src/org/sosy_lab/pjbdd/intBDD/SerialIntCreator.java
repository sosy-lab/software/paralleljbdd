// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.BDDCreatorSimpleStats;
import org.sosy_lab.pjbdd.util.IfThenElseData;
import org.sosy_lab.pjbdd.util.IntArrayUtils;
import org.sosy_lab.pjbdd.util.reference.IntHoldingWeakReference;

/**
 * {@link Creator} implementation with ints as underlying bdd data structure.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public class SerialIntCreator implements Creator {

  /** reference queue used to cleanup reclaimed entries. */
  protected final ReferenceQueue<BDDimpl> queue = new ReferenceQueue<>();

  protected final IntSatAlgorithm satAlgorithm;
  protected final IntAlgorithm algorithm;
  protected final IntNodeManager nodeManager;
  @Nullable protected final Stats stats;

  /** Constructor . */
  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "EI2",
      justification = "intentional design")
  public SerialIntCreator(
      IntAlgorithm algorithm, IntSatAlgorithm satAlgorithm, IntNodeManager nodeManager) {
    this.algorithm = algorithm;
    this.satAlgorithm = satAlgorithm;
    this.nodeManager = nodeManager;
    this.stats = null;
  }

  /** {@inheritDoc} */
  @Override
  public DD restrict(DD bdd, int var, boolean restrictionVar) {
    throw new IllegalArgumentException();
  }

  /** {@inheritDoc} */
  @Override
  public DD anySat(DD f) {
    return makeBDD(satAlgorithm.anySat(((BDDimpl) f).index));
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(DD f) {
    return satAlgorithm.satCount(((BDDimpl) f).index);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeTrue() {
    return makeBDD(IntUniqueTable.ONE);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeFalse() {
    return makeBDD(IntUniqueTable.ZERO);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(DD f1, DD f2, DD f3) {
    return makeBDD(
        algorithm.makeIte(((BDDimpl) f1).index, ((BDDimpl) f2).index, ((BDDimpl) f3).index));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeIte(IfThenElseData iteData) {
    return makeIte(iteData.getIf(), iteData.getThen(), iteData.getElse());
  }

  /** {@inheritDoc} */
  @Override
  public DD makeVariable() {
    return makeBDD(
        nodeManager.makeNode(
            nodeManager.getVarCount(), nodeManager.getFalse(), nodeManager.getTrue()));
  }

  @Override
  public DD makeVariableBefore(DD var) {
    return makeBDD(nodeManager.makeVariableBefore(var.getVariable()));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNot(DD f) {
    return makeBDD(algorithm.makeNot(((BDDimpl) f).index));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeAnd(DD f1, DD f2) {
    return makeBDD(
        algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_AND));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeOr(DD f1, DD f2) {
    return makeBDD(
        algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_OR));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeXor(DD f1, DD f2) {

    return makeBDD(
        algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_XOR));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNor(DD f1, DD f2) {
    return makeBDD(
        algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_NOR));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNand(DD f1, DD f2) {
    return makeBDD(
        algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_NAND));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeExists(DD f1, DD[] f2) {
    int f = ((BDDimpl) f2[0]).index;
    for (DD arg : f2) {
      f = algorithm.makeOp(f, ((BDDimpl) arg).index, IntAlgorithm.ApplyOp.OP_AND);
    }
    Set<Integer> varSet = createHighVarLevelSet(f);
    return makeBDD(algorithm.makeExquant(((BDDimpl) f1).index, IntArrayUtils.toIntArray(varSet)));
  }

  @Override
  public DD makeExists(DD f1, DD f2) {
    Set<Integer> varSet = createHighVarLevelSet(((BDDimpl) f2).index);
    return makeBDD(algorithm.makeExquant(((BDDimpl) f1).index, IntArrayUtils.toIntArray(varSet)));
  }

  @Override
  public DD makeReplace(DD f1, DD oldVar, DD replaceVar) {
    return makeExists(makeAnd(f1, makeXnor(oldVar, replaceVar)), oldVar);
  }

  @Override
  public DD makeCompose(DD f1, int var, DD f2) {
    return makeBDD(algorithm.makeCompose(((BDDimpl) f1).index, var, ((BDDimpl) f2).index));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeXnor(DD f1, DD f2) {
    return makeBDD(
        algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_XNOR));
  }

  /** {@inheritDoc} */
  @Override
  public DD makeEqual(DD f1, DD f2) {
    return makeXnor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeUnequal(DD f1, DD f2) {
    return makeXor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public DD makeImply(DD f1, DD f2) {
    return makeBDD(
        algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_IMP));
  }

  /** {@inheritDoc} */
  @Override
  public IfThenElseData getIfThenElse(DD f) {
    return new IfThenElseData(makeIthVar(f.getVariable()), f.getLow(), f.getHigh());
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNode(DD low, DD high, int var) {
    return makeBDD(nodeManager.makeNode(var, ((BDDimpl) low).index, ((BDDimpl) high).index));
  }

  /** {@inheritDoc} */
  @Override
  public DD getLow(DD top) {
    return makeBDD(low(((BDDimpl) top).index));
  }

  /** {@inheritDoc} */
  @Override
  public DD getHigh(DD top) {
    return makeBDD(high(((BDDimpl) top).index));
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    algorithm.shutDown();
  }

  @Override
  public int getVariableCount() {
    return nodeManager.getVarCount();
  }

  @Override
  public int[] getVariableOrdering() {
    return nodeManager.getCurrentOrdering();
  }

  @Override
  public void setVariableCount(int count) {
    nodeManager.setVarCount(var(count));
  }

  /** {@inheritDoc} */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    nodeManager.setVarOrdering(pOrder.stream().mapToInt(i -> i).toArray());
  }

  /** {@inheritDoc} */
  @Override
  public Stats getCreatorStats() {
    if (stats == null) {
      int uniqueTableNodeCount = nodeManager.getNodeCount();
      int uniqueTableSize = nodeManager.getUniqueTableSize();
      int variableCount = nodeManager.getVarCount();
      int cacheSize = algorithm.getCacheSize();
      int cacheNodeCount = algorithm.getCacheNodeCount();
      return new BDDCreatorSimpleStats(
          uniqueTableSize, uniqueTableNodeCount, cacheSize, cacheNodeCount, variableCount);
    }
    return stats;
  }

  @Override
  public void cleanUnusedNodes() {
    nodeManager.cleanUnusedNodes();
  }

  /**
   * convert internal index representation to BDD object for external use. Creates WeakReference for
   * object to modify reference count on garbage collection reclaim.
   *
   * @param id - bdd's index
   * @return created bdd object
   */
  @SuppressWarnings("unused")
  protected DD makeBDD(int id) {
    BDDimpl bdd = new BDDimpl(id);
    new IntHoldingWeakReference<>(bdd, queue, id);
    deleteReclaimedEntries();
    return bdd;
  }

  /**
   * Takes a bdd and creates a set of all high branch variables.
   *
   * @param f - the bdd argument
   * @return a set of variables
   */
  private Set<Integer> createHighVarLevelSet(int f) {
    Set<Integer> varLevelSet = new TreeSet<>();
    while (nodeManager.getTrue() != f && nodeManager.getFalse() != f) {
      varLevelSet.add(level(f));
      f = high(f);
    }
    return varLevelSet;
  }

  /**
   * add a new reference for given bdd.
   *
   * @param index - of given bdd
   */
  protected void addRef(int index) {
    nodeManager.addRefs(index);
  }

  /**
   * free one reference for given bdd. Cascading delete if there are no more references.
   *
   * @param index - of given bdd
   */
  protected void freeRef(int index) {
    nodeManager.freeRefs(index);
  }

  /**
   * get low child branch for given bdd.
   *
   * @param root - the given bdd
   * @return low branch for given bdd
   */
  protected int low(int root) {
    return nodeManager.low(root);
  }

  /**
   * get high child branch for given bdd.
   *
   * @param root - the given bdd
   * @return high branch for given bdd
   */
  protected int high(int root) {
    return nodeManager.high(root);
  }

  /**
   * get variable for given bdd.
   *
   * @param index - the given bdd
   * @return variable for given bdd
   */
  protected int var(int index) {
    return nodeManager.var(index);
  }

  /**
   * get level for given bdd's variable.
   *
   * @param index - the given bdd
   * @return level of given bdd's variable
   */
  protected int level(int index) {
    return nodeManager.level(var(index));
  }

  /**
   * Int Wrapper {@link DD} implementation. Only holds bdd's unique table index. Uses index and
   * Creator as shadow reference to determine {@link DD} methods.
   *
   * @author Stephan Holzner
   * @see DD
   * @since 1.0
   */
  class BDDimpl implements DD {
    /** the bdd's unique table index. */
    final int index;

    /**
     * Creates a new {@link BDDimpl} instance for a given index.
     *
     * @param index - bdd's index
     */
    BDDimpl(int index) {
      this.index = index;
      addRef(this.index);
    }

    /**
     * Get the negation of this bdd.
     *
     * @return the negation of this bdd
     */
    public DD not() {
      return SerialIntCreator.this.makeNot(this);
    }

    /** {@inheritDoc} */
    @Override
    public int getVariable() {
      return SerialIntCreator.this.var(index);
    }

    /** {@inheritDoc} */
    @Override
    public DD getLow() {
      return makeBDD(SerialIntCreator.this.low(index));
    }

    /** {@inheritDoc} */
    @Override
    public DD getHigh() {
      return makeBDD(SerialIntCreator.this.high(index));
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTrue() {
      return index == IntUniqueTable.ONE;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFalse() {
      return index == IntUniqueTable.ZERO;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
      if (o instanceof BDDimpl) {
        return ((BDDimpl) o).index == index;
      }
      return false;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
      return index;
    }
  }

  protected void deleteReclaimedEntries() {
    Reference<? extends DD> sv = queue.poll();
    while (sv != null) {
      if (sv instanceof IntHoldingWeakReference) {
        freeRef(((IntHoldingWeakReference<?>) sv).intValue());
      }
      sv = queue.poll();
    }
  }
}
