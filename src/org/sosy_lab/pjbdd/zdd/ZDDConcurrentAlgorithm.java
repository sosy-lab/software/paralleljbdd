// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.zdd;

import java.util.Optional;
import java.util.concurrent.ForkJoinTask;
import java.util.function.BiFunction;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link ZDDAlgorithm} implementation which uses {@link ZDDSerialAlgorithm} as base class.
 * Performs multi core zdd operations. For parallel computations the Fork/Join Framework is used.
 *
 * @author Stephan Holzner
 * @see ZDDSerialAlgorithm
 * @see ZDDAlgorithm
 * @since 1.0
 */
public class ZDDConcurrentAlgorithm<V extends DD> extends ZDDSerialAlgorithm<V> {

  /** Worker thread pool manager. */
  private final ParallelismManager parallelismManager;

  /**
   * Creates new {@link ZDDConcurrentAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation.
   */
  public ZDDConcurrentAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /** {@inheritDoc} */
  @Override
  protected V unaryShannon(V zdd, V var, ZddOps op) {
    return operationCheck(zdd, var, op).orElseGet(() -> asyncShannon(zdd, var, op));
  }

  /**
   * Asynchronous unary shannon expansion of operation. Terminal check for current recursion step
   * must be performed before calling this method.
   *
   * @param zdd - ZDD argument
   * @param var - variable argument
   * @param op - op to be applied
   * @return f1 'op' f2
   */
  private V asyncShannon(V zdd, V var, ZddOps op) {
    V res = applyOperation(zdd, var, op);
    cacheBinaryItem(zdd, var, op.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V union(V f1, V f2) {
    return unionCheck(f1, f2).orElseGet(() -> asyncUnion(f1, f2));
  }

  /**
   * Asynchronous union operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'UNION' f2
   */
  private V asyncUnion(V f1, V f2) {
    V res;
    if (level(f1) < level(f2)) {
      res = makeNode(union(getLow(f1), f2), getHigh(f1), f1.getVariable());
    } else if (level(f1) == level(f2)) {
      res = applyOperation(f1, f2, ZddOps.UNION);
    } else {
      res = makeNode(union(f1, getLow(f2)), getHigh(f2), f2.getVariable());
    }
    cacheBinaryItem(f1, f2, ZddOps.UNION.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V difference(V f1, V f2) {
    return differenceCheck(f1, f2).orElseGet(() -> asyncDifference(f1, f2));
  }

  /**
   * Asynchronous difference operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'DIFFERENCE' f2
   */
  private V asyncDifference(V f1, V f2) {
    V res;
    if (level(f1) < level(f2)) {
      // next step check not yet performed
      res = makeNode(difference(getLow(f1), f2), getHigh(f1), f1.getVariable());
    } else if (level(f1) == level(f2)) {
      res = applyOperation(f1, f2, ZddOps.DIFF);
    } else {
      // next step check not yet performed
      res = difference(f1, getLow(f2));
    }
    cacheBinaryItem(f1, f2, ZddOps.DIFF.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V intersection(V f1, V f2) {
    return intersectsCheck(f1, f2).orElseGet(() -> asyncIntersect(f1, f2));
  }

  /**
   * Asynchronous intersection operation. Terminal check for current recursion step must be
   * performed before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'INTERSECTS' f2
   */
  private V asyncIntersect(V f1, V f2) {
    V res;
    if (level(f1) < level(f2)) {
      res = intersection(getLow(f1), f2);
    } else if (level(f1) == level(f2)) {
      res = applyOperation(f1, f2, ZddOps.INTSEC);
    } else {
      res = intersection(f1, getLow(f2));
    }
    cacheBinaryItem(f1, f2, ZddOps.INTSEC.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V product(V f1, V f2) {
    return productCheck(f1, f2).orElseGet(() -> asyncProduct(f1, f2));
  }

  /**
   * Asynchronous product operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'PRODUCT' f2
   */
  private V asyncProduct(V f1, V f2) {
    V res;
    if (level(f1) != level(f2)) {
      res = applyOperation(f1, f2, ZddOps.MUL);
    } else {
      V lowTmp;
      V highTmp;
      V high;
      V low;
      ForkJoinTask<V> lowTask1 = null;
      ForkJoinTask<V> highTask1 = null;
      ForkJoinTask<V> highTask = null;
      ForkJoinTask<V> lowTask = null;

      V lowF1 = getLow(f1);
      V lowF2 = getLow(f2);
      V highF1 = getHigh(f1);
      V highF2 = getHigh(f2);

      Optional<V> optLowTmp = trySerialComputation(highF1, lowF2, ZddOps.MUL);
      if (optLowTmp.isEmpty()) {
        lowTask1 = createTask(this::asyncProduct, highF1, lowF2);
      }

      Optional<V> optHighTmp = trySerialComputation(highF1, highF2, ZddOps.MUL);
      if (optHighTmp.isEmpty()) {
        highTask1 = createTask(this::asyncProduct, highF1, highF2);
      }

      Optional<V> optHighTask = trySerialComputation(lowF1, highF2, ZddOps.MUL);
      if (optHighTask.isEmpty()) {
        highTask = createTask(this::asyncProduct, lowF1, highF2);
      }
      Optional<V> optLowTask = trySerialComputation(lowF1, lowF2, ZddOps.MUL);
      if (optLowTask.isEmpty()) {
        lowTask = createTask(this::asyncProduct, lowF1, lowF2);
      }

      if (optHighTmp.isPresent()) {
        highTmp = optHighTmp.get();
      } else {
        highTmp = extract(highTask1);
      }

      if (optLowTmp.isPresent()) {
        lowTmp = optLowTmp.get();
      } else {
        lowTmp = extract(lowTask1);
      }

      if (optHighTask.isPresent()) {
        high = optHighTask.get();
      } else {
        high = extract(highTask);
      }

      if (optLowTask.isPresent()) {
        low = optLowTask.get();
      } else {
        low = extract(lowTask);
      }

      res = union(highTmp, lowTmp);
      high = union(res, high);
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.MUL.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V division(V f1, V f2) {
    return divisionCheck(f1, f2).orElseGet(() -> asyncDivision(f1, f2));
  }

  /**
   * Asynchronous division operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'DIVIDE' f2
   */
  private V asyncDivision(V f1, V f2) {
    V res;
    if (level(f1) < level(f2)) {
      res = applyOperation(f1, f2, ZddOps.DIV);
    } else {
      res = sequentialDivisionCase(f1, f2);
    }
    cacheBinaryItem(f1, f2, ZddOps.DIV.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V exclude(V f1, V f2) {
    return excludeCheck(f1, f2).orElseGet(() -> asyncExclude(f1, f2));
  }

  /**
   * Asynchronous exclude operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'EXCLUDE' f2
   */
  private V asyncExclude(V f1, V f2) {
    V res;
    if (level(f1) > level(f2)) {
      res = exclude(f1, getLow(f2));
    } else if (level(f1) < level(f2)) {
      res = applyOperation(f1, f2, ZddOps.EXCLUDE);
    } else {
      V low;
      V high;
      if (followLow(getHigh(f2)).equals(base())) {
        high = empty();
      } else {
        V lowF2 = getLow(f2);
        V highF1 = getHigh(f1);
        V highF2 = getHigh(f2);

        Optional<V> lowCheck = excludeCheck(highF1, highF2);
        Optional<V> highCheck = excludeCheck(highF1, lowF2);

        if (lowCheck.isEmpty() && highCheck.isEmpty() && parallelismManager.canFork(level(f1))) {
          ForkJoinTask<V> lowTask = createTask(this::asyncExclude, highF1, highF2);
          ForkJoinTask<V> highTask = createTask(this::asyncExclude, highF1, lowF2);
          high = intersection(extract(highTask), extract(lowTask));
        } else {
          high = highCheck.orElseGet(() -> asyncExclude(highF1, lowF2));
          low = lowCheck.orElseGet(() -> asyncExclude(highF1, highF2));
          high = intersection(high, low);
        }
      }
      low = exclude(getLow(f1), getLow(f2));
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.EXCLUDE.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V restrict(V f1, V f2) {
    return restrictCheck(f1, f2).orElseGet(() -> asyncRestrict(f1, f2));
  }

  /**
   * Asynchronous restrict operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'RESTRICT' f2
   */
  private V asyncRestrict(V f1, V f2) {
    V res;
    if (level(f1) > level(f2)) {
      res = restrict(f1, getLow(f2));
    } else if (level(f1) < level(f2)) {
      res = applyOperation(f1, f2, ZddOps.RESTRICT);
    } else {
      V lowTmp;
      V highTmp;
      V lowRes;
      V highRes;
      ForkJoinTask<V> lowTmpTask = null;
      ForkJoinTask<V> highTmpTask = null;
      ForkJoinTask<V> lowResTask = null;

      V lowF1 = getLow(f1);
      V lowF2 = getLow(f2);
      V highF1 = getHigh(f1);
      V highF2 = getHigh(f2);

      Optional<V> optLowTmp = trySerialComputation(highF1, lowF2, ZddOps.RESTRICT);
      if (optLowTmp.isEmpty()) {
        lowTmpTask = createTask(this::asyncRestrict, highF1, lowF2);
      }
      Optional<V> optHighTmp = trySerialComputation(highF1, highF2, ZddOps.RESTRICT);
      if (optHighTmp.isEmpty()) {
        highTmpTask = createTask(this::asyncRestrict, highF1, highF2);
      }
      Optional<V> optLowRes = trySerialComputation(lowF1, lowF2, ZddOps.RESTRICT);
      if (optLowRes.isEmpty()) {
        lowResTask = createTask(this::asyncRestrict, lowF1, lowF2);
      }

      if (optLowTmp.isPresent()) {
        lowTmp = optLowTmp.get();
      } else {
        lowTmp = extract(lowTmpTask);
      }

      if (optHighTmp.isPresent()) {
        highTmp = optHighTmp.get();
      } else {
        highTmp = extract(highTmpTask);
      }

      Optional<V> optHighRes = trySerialComputation(lowTmp, highTmp, ZddOps.UNION);
      if (optHighRes.isEmpty()) {
        ForkJoinTask<V> highResTask = createTask(this::asyncUnion, highTmp, lowTmp);
        highRes = extract(highResTask);
      } else {
        highRes = optHighRes.get();
      }

      if (optLowRes.isPresent()) {
        lowRes = optLowRes.get();
      } else {
        lowRes = extract(lowResTask);
      }

      res = makeNode(lowRes, highRes, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.RESTRICT.ordinal(), res);
    return res;
  }

  /**
   * Asynchronous applying zdd operation on child nodes.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @param op - the operation to be applied
   * @return f1 'op' f2
   */
  private V applyOperation(V f1, V f2, ZddOps op) {
    V lowF1 = f1;
    V lowF2 = f2;
    V highF1 = f1;
    V highF2 = f2;
    if (level(f1) <= level(f2)) {
      lowF1 = getLow(f1);
      highF1 = getHigh(f1);
    }
    if (level(f2) <= level(f1)) {
      lowF2 = getLow(f2);
      highF2 = getHigh(f2);
    }

    V low;
    V high;
    int var = level(f1) <= level(f2) ? f1.getVariable() : f2.getVariable();
    // avoid terminal case fork
    Optional<V> lowCheck = operationCheck(lowF1, lowF2, op);
    Optional<V> highCheck = operationCheck(highF1, highF2, op);

    if (lowCheck.isEmpty() && highCheck.isEmpty() && parallelismManager.canFork(level(f1))) {
      ForkJoinTask<V> lowTask = createTask((z1, z2) -> apply(z1, z2, op), lowF1, lowF2);
      ForkJoinTask<V> highTask = createTask((z1, z2) -> apply(z1, z2, op), highF1, highF2);
      return makeNode(extract(lowTask), extract(highTask), var);
    } else {
      if (lowCheck.isPresent()) {
        low = lowCheck.get();
      } else {
        low = apply(lowF1, lowF2, op);
      }

      if (highCheck.isPresent()) {
        high = highCheck.get();
      } else {
        high = apply(highF1, highF2, op);
      }
      return makeNode(low, high, var);
    }
  }

  /**
   * Helper method to resolve actual operation to be applied. Necessary because there is no common
   * shannon expansion for zdd operations.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @param op - the operation to be applied
   * @return f1 'op' f2
   */
  private V apply(V f1, V f2, ZddOps op) {
    switch (op) {
      case UNION:
        return asyncUnion(f1, f2);
      case DIFF:
        return asyncDifference(f1, f2);
      case INTSEC:
        return asyncIntersect(f1, f2);
      case MUL:
        return asyncProduct(f1, f2);
      case DIV:
        return asyncDivision(f1, f2);
      case EXCLUDE:
        return asyncExclude(f1, f2);
      case RESTRICT:
        return asyncRestrict(f1, f2);
      case SUB_SET1:
      case CHANGE:
      case SUB_SET0:
        return asyncShannon(f1, f2, op);
      default:
        throw new IllegalArgumentException("Unknown Operator");
    }
  }

  /**
   * Helper function to avoid terminal case forking and reduces some duplicate code. Returns
   * terminal case result, operation result if worker pool is busy or empty if computation can be
   * performed in parallel.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @param op - the operation to be performed
   * @return Optional of computation result
   */
  private Optional<V> trySerialComputation(V f1, V f2, ZddOps op) {
    return operationCheck(f1, f2, op)
        .map(Optional::of)
        .orElseGet(
            () -> {
              if (!parallelismManager.canFork(level(f1))) {
                return Optional.of(apply(f1, f2, op));
              }
              return Optional.empty();
            });
  }

  /**
   * Helper function to create and submit a {@link ForkJoinTask} to {@link
   * ZDDConcurrentAlgorithm#parallelismManager}'s thread pool.
   *
   * @param operation - the operation the new task will perform
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return the task performing the f1 'operation' f2
   */
  @SuppressWarnings("resource")
  private ForkJoinTask<V> createTask(BiFunction<V, V, V> operation, V f1, V f2) {
    parallelismManager.taskSupplied();
    return parallelismManager.getThreadPool().submit(() -> operation.apply(f1, f2));
  }

  /**
   * Helper function to extract asynchronous task result. May block the supplied task.
   *
   * @param task - the computation task
   * @return the tasks computation result
   */
  private V extract(ForkJoinTask<V> task) {
    V res = task.join();
    parallelismManager.taskDone();
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
