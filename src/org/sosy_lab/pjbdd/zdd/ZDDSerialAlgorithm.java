// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.zdd;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.ManipulatingAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;

/**
 * A {@link ZDDAlgorithm} implementation which uses {@link ManipulatingAlgorithm} as base class.
 * Performs single core zdd operations. (But multiple operations can be performed on multiple
 * cores).
 *
 * @author Stephan Holzner
 * @see ZDDAlgorithm
 * @see ManipulatingAlgorithm
 * @since 1.0
 */
public class ZDDSerialAlgorithm<V extends DD> extends ManipulatingAlgorithm<V>
    implements ZDDAlgorithm<V> {

  /**
   * Creates new {@link ZDDSerialAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation.
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   */
  public ZDDSerialAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager<V> nodeManager) {
    super(computedTable, nodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public V subSet1(V zdd, V var) {
    return unaryShannon(zdd, var, ZddOps.SUB_SET1);
  }

  /**
   * Helper function to chain terminal case and cache checks for subset1 operation.
   *
   * @param zdd - the zdd
   * @param var - the variable
   * @return Optional of subset1 terminal check
   */
  private Optional<V> subset1Check(V zdd, V var) {
    if (level(zdd) > level(var)) {
      return Optional.of(empty());
    } else if (level(zdd) == level(var)) {
      return Optional.of(getHigh(zdd));
    }
    return checkBinaryCache(zdd, var, ZddOps.SUB_SET1.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V subSet0(V zdd, V var) {
    return unaryShannon(zdd, var, ZddOps.SUB_SET0);
  }

  /**
   * Helper function to chain terminal case and cache checks for subset0 operation.
   *
   * @param zdd - the zdd
   * @param var - the variable
   * @return Optional of subset0 terminal check
   */
  private Optional<V> subset0Check(V zdd, V var) {
    if (level(zdd) > level(var)) {
      return Optional.of(zdd);
    } else if (level(zdd) == level(var)) {
      return Optional.of(getLow(zdd));
    }
    return checkBinaryCache(zdd, var, ZddOps.SUB_SET0.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V change(V zdd, V var) {
    return unaryShannon(zdd, var, ZddOps.CHANGE);
  }

  /**
   * Helper function to chain terminal case and cache checks for change operation.
   *
   * @param zdd - the zdd
   * @param var - the variable
   * @return Optional of change terminal check
   */
  private Optional<V> changeCheck(V zdd, V var) {
    if (level(zdd) > level(var)) {
      return Optional.of(makeNode(empty(), zdd, var.getVariable()));
    } else if (level(zdd) == level(var)) {
      return Optional.of(
          makeNode(/* low= */ getHigh(zdd), /* high= */ getLow(zdd), var.getVariable()));
    }
    return checkBinaryCache(zdd, var, ZddOps.CHANGE.ordinal());
  }

  /**
   * Helper method for methods with one argument and a variable, where terminal case shannon
   * expansion can be used.
   *
   * @param zdd - the ZDD node
   * @param var - the variable in {@link DD}-representation
   * @param op - the operation code
   * @return the shannon expansion of given operation over zdd and var
   */
  protected V unaryShannon(V zdd, V var, ZddOps op) {
    return operationCheck(zdd, var, op)
        .orElseGet(
            () -> {
              V low = unaryShannon(getLow(zdd), var, op);
              V high = unaryShannon(getHigh(zdd), var, op);
              V res = makeNode(low, high, zdd.getVariable());
              cacheBinaryItem(zdd, var, op.ordinal(), res);
              return res;
            });
  }

  /** {@inheritDoc} */
  @Override
  public V union(V f1, V f2) {
    return unionCheck(f1, f2)
        .orElseGet(
            () -> {
              V res;
              if (level(f1) < level(f2)) {
                res = makeNode(union(getLow(f1), f2), getHigh(f1), f1.getVariable());
              } else if (level(f1) == level(f2)) {
                res =
                    makeNode(
                        union(getLow(f1), getLow(f2)),
                        union(getHigh(f1), getHigh(f2)),
                        f2.getVariable());
              } else {
                res = makeNode(union(f1, getLow(f2)), getHigh(f2), f2.getVariable());
              }
              cacheBinaryItem(f1, f2, ZddOps.UNION.ordinal(), res);
              return res;
            });
  }

  /**
   * Helper function to chain terminal case and cache checks for union operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of union terminal check
   */
  protected Optional<V> unionCheck(V f1, V f2) {
    if (f1.equals(empty())) {
      return Optional.of(f2);
    }
    if (f2.equals(empty())) {
      return Optional.of(f1);
    }
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.UNION.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V difference(V f1, V f2) {
    return differenceCheck(f1, f2)
        .orElseGet(
            () -> {
              V res;
              if (level(f1) < level(f2)) {
                res = makeNode(difference(getLow(f1), f2), getHigh(f1), f1.getVariable());
              } else if (level(f1) == level(f2)) {
                res =
                    makeNode(
                        difference(getLow(f1), getLow(f2)),
                        difference(getHigh(f1), getHigh(f2)),
                        f2.getVariable());
              } else {
                res = difference(f1, getLow(f2));
              }
              cacheBinaryItem(f1, f2, ZddOps.DIFF.ordinal(), res);
              return res;
            });
  }

  /**
   * Helper function to chain terminal case and cache checks for difference operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of difference terminal check
   */
  protected Optional<V> differenceCheck(V f1, V f2) {
    if (f1.equals(empty())) {
      return Optional.of(empty());
    }
    if (f2.equals(empty())) {
      return Optional.of(f1);
    }
    if (f1.equals(f2)) {
      return Optional.of(empty());
    }
    return checkBinaryCache(f1, f2, ZddOps.DIFF.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V intersection(V f1, V f2) {
    return intersectsCheck(f1, f2)
        .orElseGet(
            () -> {
              V res;
              if (level(f1) < level(f2)) {
                res = intersection(getLow(f1), f2);
              } else if (level(f1) == level(f2)) {
                res =
                    makeNode(
                        intersection(getLow(f1), getLow(f2)),
                        intersection(getHigh(f1), getHigh(f2)),
                        f2.getVariable());
              } else {
                res = intersection(f1, getLow(f2));
              }
              cacheBinaryItem(f1, f2, ZddOps.INTSEC.ordinal(), res);
              return res;
            });
  }

  /**
   * Helper function to chain terminal case and cache checks for intersects operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of intersects terminal check
   */
  protected Optional<V> intersectsCheck(V f1, V f2) {
    if (f1.equals(empty())) {
      return Optional.of(empty());
    }
    if (f2.equals(empty())) {
      return Optional.of(empty());
    }
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.INTSEC.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V product(V f1, V f2) {
    Optional<V> check = productCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }
    V res;
    if (level(f1) > level(f2)) {
      res = makeNode(product(f1, getLow(f2)), product(f1, getHigh(f2)), f2.getVariable());
    } else if (level(f1) < level(f2)) {
      res = makeNode(product(getLow(f1), f2), product(getHigh(f1), f2), f1.getVariable());
    } else {
      V high = product(getHigh(f1), getHigh(f2));
      V low = product(getHigh(f1), getLow(f2));
      res = product(high, low);

      high = product(getLow(f1), getHigh(f2));
      high = product(res, high);

      low = product(getLow(f1), getLow(f2));
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.MUL.ordinal(), res);
    return res;
  }

  /**
   * Helper function to chain terminal case and cache checks for product operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of product terminal check
   */
  protected Optional<V> productCheck(V f1, V f2) {
    if (f1.equals(empty()) || f2.equals(empty())) {
      return Optional.of(empty());
    }
    if (f1.equals(base())) {
      return Optional.of(f2);
    }
    if (f2.equals(base())) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.MUL.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V modulo(V zdd1, V zdd2) {
    return checkBinaryCache(zdd1, zdd2, ZddOps.MOD.ordinal())
        .orElseGet(
            () -> {
              V result = difference(zdd1, product(zdd2, difference(zdd1, zdd2)));
              cacheBinaryItem(zdd1, zdd2, ZddOps.MUL.ordinal(), result);
              return result;
            });
  }

  /** {@inheritDoc} */
  @Override
  public V division(V f1, V f2) {
    Optional<V> check = divisionCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }
    V res;
    if (level(f1) < level(f2)) {
      res = makeNode(division(getLow(f1), f2), division(getHigh(f1), f2), f1.getVariable());
    } else {
      res = sequentialDivisionCase(f1, f2);
    }
    cacheBinaryItem(f1, f2, ZddOps.DIV.ordinal(), res);
    return res;
  }

  /**
   * Perform divisions case that can not be performed in parallel.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return (zdd1 { @ literal / } zdd2)
   */
  protected V sequentialDivisionCase(V f1, V f2) {
    V res = division(getHigh(f1), getHigh(f2));
    V tmp = getLow(f2);
    if (!res.isFalse() && !tmp.isFalse()) {
      tmp = division(getLow(f1), tmp);
      res = intersection(tmp, res);
    }
    return res;
  }

  /**
   * Helper function to chain terminal case and cache checks for division operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of division terminal check
   */
  protected Optional<V> divisionCheck(V f1, V f2) {
    if (f1.equals(empty()) || f1.equals(base())) {
      return Optional.of(empty());
    }
    if (f1.equals(f2)) {
      return Optional.of(base());
    }
    if (f2.equals(base())) {
      return Optional.of(f1);
    }
    if (level(f2) < level(f1)) {
      return Optional.of(empty());
    }
    return checkBinaryCache(f1, f2, ZddOps.DIV.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V exclude(V f1, V f2) {
    Optional<V> check = excludeCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }

    V res;
    if (level(f1) > level(f2)) {
      res = exclude(f1, getLow(f2));
    } else if (level(f1) < level(f2)) {
      res = makeNode(exclude(getLow(f1), f2), exclude(getHigh(f1), f2), f1.getVariable());
    } else {
      V low;
      V high;
      if (followLow(getHigh(f2)).equals(base())) {
        high = empty();
      } else {
        high = exclude(getHigh(f1), getLow(f2));
        low = exclude(getHigh(f1), getHigh(f2));
        high = intersection(high, low);
      }
      low = exclude(getLow(f1), getLow(f2));
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.EXCLUDE.ordinal(), res);
    return res;
  }

  /**
   * Helper function to chain terminal case and cache checks for exclude operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of exclude terminal check
   */
  protected Optional<V> excludeCheck(V f1, V f2) {
    if (f1.equals(empty()) || f2.equals(base()) || f1.equals(f2)) {
      return Optional.of(empty());
    }
    if (f1.equals(base()) || f2.equals(empty())) {
      return Optional.of(f1);
    }

    return checkBinaryCache(f1, f2, ZddOps.EXCLUDE.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public V restrict(V f1, V f2) {
    Optional<V> check = restrictCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }

    V res;
    if (level(f1) > level(f2)) {
      res = restrict(f1, getLow(f2));
    } else if (level(f1) < level(f2)) {
      res = makeNode(restrict(getLow(f1), f2), restrict(getHigh(f1), f2), f1.getVariable());
    } else {
      V low = restrict(getHigh(f1), getLow(f2));
      V high = restrict(getHigh(f1), getHigh(f2));
      high = union(high, low);
      low = restrict(getLow(f1), getLow(f2));
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.RESTRICT.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public V empty() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public V base() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    computedTable.clear();
  }

  /**
   * Helper function to chain terminal case and cache checks for restrict operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of restrict terminal check
   */
  protected Optional<V> restrictCheck(V f1, V f2) {
    if (f1.equals(empty()) || f2.equals(empty())) {
      return Optional.of(empty());
    }
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.RESTRICT.ordinal());
  }

  /**
   * Follow low branch till leaf node is reached.
   *
   * @param bdd - the input node
   * @return leaf of low branch path
   */
  protected V followLow(V bdd) {
    V copy = bdd;
    while (!copy.isLeaf()) {
      copy = getLow(copy);
    }
    return copy;
  }

  /**
   * Apply operation check to avoid lambda terminal check references.
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the operation to be applied
   * @return Optional of terminal check f1 'op' f2
   */
  protected Optional<V> operationCheck(V f1, V f2, ZddOps op) {
    switch (op) {
      case UNION:
        return unionCheck(f1, f2);
      case DIFF:
        return differenceCheck(f1, f2);
      case INTSEC:
        return intersectsCheck(f1, f2);
      case MUL:
        return productCheck(f1, f2);
      case EXCLUDE:
        return excludeCheck(f1, f2);
      case RESTRICT:
        return restrictCheck(f1, f2);
      case DIV:
        return divisionCheck(f1, f2);
      case CHANGE:
        return changeCheck(f1, f2);
      case SUB_SET0:
        return subset0Check(f1, f2);
      case SUB_SET1:
        return subset1Check(f1, f2);
      default:
        throw new IllegalArgumentException("Unknown Operation: " + op);
    }
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }
}
