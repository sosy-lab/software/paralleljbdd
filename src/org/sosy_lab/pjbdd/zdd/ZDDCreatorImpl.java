// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.zdd;

import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.sosy_lab.pjbdd.api.AbstractCreator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.api.ZDDCreator;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.node.NodeManager;

/**
 * {@link org.sosy_lab.pjbdd.api.ZDDCreator} implementation which uses {@link AbstractCreator} as a
 * base class.
 *
 * @author Stephan Holzner
 * @see org.sosy_lab.pjbdd.api.ZDDCreator
 * @see AbstractCreator
 * @since 1.0
 */
public class ZDDCreatorImpl extends AbstractCreator implements ZDDCreator {

  /** the backing zdd algorithm implantation. */
  private final ZDDAlgorithm<DD> algorithm;

  private final ReadWriteLock reorderLock = new ReentrantReadWriteLock();

  /**
   * Creates a new zdd creator environment.
   *
   * @param satAlgorithm - the chosen sat algorithm
   * @param nodeManager - the backing node handler
   * @param algorithm - the backing algorithm for zdd implementation
   */
  public ZDDCreatorImpl(
      SatAlgorithm<DD> satAlgorithm, NodeManager<DD> nodeManager, ZDDAlgorithm<DD> algorithm) {
    super(nodeManager, satAlgorithm);
    this.algorithm = algorithm;
  }

  /** {@inheritDoc} */
  @Override
  public DD subSet1(DD zdd, DD var) {
    reorderLock.readLock().lock();
    try {
      return algorithm.subSet1(zdd, var);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD subSet0(DD zdd, DD var) {
    reorderLock.readLock().lock();
    try {
      return algorithm.subSet0(zdd, var);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD change(DD zdd, DD var) {
    reorderLock.readLock().lock();
    try {
      return algorithm.change(zdd, var);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD union(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.union(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD difference(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.difference(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD intersection(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.intersection(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD product(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.product(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD modulo(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.modulo(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD division(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.division(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD exclude(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.exclude(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD restrict(DD zdd1, DD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.restrict(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD makeNode(DD low, DD high, int var) {
    return algorithm.makeNode(low, high, var);
  }

  /** {@inheritDoc} */
  @Override
  public DD empty() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public DD base() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public DD universe() {
    reorderLock.readLock().lock();
    try {
      DD universe = base();
      int[] order = nodeManager.getCurrentOrdering();
      for (int i = order.length; i > 0; ) {
        universe = makeNode(universe, universe, order[--i]);
      }
      return universe;
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public DD cube(int... vars) {
    reorderLock.readLock().lock();
    try {
      DD cube = base();
      int[] order = getVariableOrdering();
      for (int i = vars.length; i > 0; ) {
        if (vars[--i] == 1) {
          cube = makeNode(empty(), cube, order[i]);
        }
        if (vars[i] == -1) {
          cube = makeNode(cube, cube, order[i]);
        }
      }
      return cube;
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(DD bdd) {
    reorderLock.readLock().lock();
    try {

      return satAlgorithm.satCount(bdd);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    algorithm.shutdown();
    nodeManager.shutdown();
  }

  /** {@inheritDoc} */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarOrder(pOrder);
    } finally {
      reorderLock.writeLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setVariableCount(int count) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarCount(count);
    } finally {
      reorderLock.writeLock().unlock();
    }
  }
}
