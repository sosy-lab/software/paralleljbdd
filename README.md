<!--
This file is part of PJBDD,
a framework for decision diagrams:
https://gitlab.com/sosy-lab/software/paralleljbdd

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# PJBDD
PJBDD is a competitive and lightweight multithreated BDD library. 
Several java parallelization technologies are used to implement different versions of the BDD framework with parallel algorithms.
The library supports fully automated resource management and is highly customizable.
PJBDD supports Binary Decision Diagrams (BDD), Zero-suppressed Decision Diagrams (ZDD) and Chained BDDs (CBDD).


## Getting Started
Installation is possible via Ivy.

### Automatic Installation using Ivy

If your build tool supports fetching packages from [Apache Ivy](https://ant.apache.org/ivy/), you can point it to [Sosy-Lab](https://www.sosy-lab.org/) [Ivy repository](https://www.sosy-lab.org/ivy/org.sosy_lab/pjbdd/),
which would automatically fetch `PJBDD` and all of its dependencies.

After the repository URL is configured, you only need to add the following dependency:

```xml
<dependency org="org.sosy_lab" name="pjbdd" rev="v1.0.10-10"/>
```

### Manual Installation

JARs for PJBDD can be downloaded from our [Ivy repository](https://www.sosy-lab.org/ivy/org.sosy_lab/pjbdd/) manually. 
If you choose the manual installation, you also need to resolve the dependencies manually.
The dependencies are listed in the corresponding Ivy file, for example in [ivy-v1.0.10-10.xml](https://www.sosy-lab.org/ivy/org.sosy_lab/pjbdd/ivy-v1.0.10-10.xml).

### Initialization:
Below is a small example showing how to initialize the library using the entry point `Builders`:

```Java
//instantiate a new bdd creator with 4 worker
     Creator creator =
            Builders.bddBuilder()
              .setVarCount(5)
              .setThreads(4)
              .build();
              
// Create bdd "f1 OR f2" with two integer variables
DD f1 = creator.makeVariable();
DD f2 = creator.makeVariable();
DD orBDD = creator.makeOr(f1,f2);

//Get number of all satisfying true assingments
BigInteger satCount = creator.satCount(orBDD);

// print dot representation of "f1 OR f2"
System.out.println(new DotExporter().bddToString(orBDD));
```

### Tutorial
There is a short [tutorial](https://gitlab.com/sosy-lab/software/paralleljbdd/-/tree/master/Tutorial.md)
demonstrating the most basic features of PJBDD.

## Examples
There is an small [example package](https://gitlab.com/sosy-lab/software/paralleljbdd/-/tree/master/src/org/sosy_lab/pjbdd/examples) containing some applications using PJBDD.

## Contribution
For contribution please read our [developers documentation](https://gitlab.com/sosy-lab/software/paralleljbdd/-/blob/master/doc/developers.md)
